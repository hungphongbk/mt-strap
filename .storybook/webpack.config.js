module.exports = async ({ config, mode }) => {
    config.module.rules.push(
        ...[
            {
                test: /\.scss$/,
                oneOf: [
                    {
                        test: /\.module\.scss$/,
                        loaders: [
                            "style-loader",
                            {
                                loader: "css-loader",
                                options: {
                                    modules: true,
                                    localIdentName:
                                        "[folder]--[local]-[hash:base64:5]"
                                }
                            },
                            "resolve-url-loader",
                            {
                                loader: "sass-loader",
                                options: {
                                    sourceMap: true,
                                    sourceMapContents: false
                                }
                            }
                        ]
                    },
                    {
                        loaders: [
                            "style-loader",
                            "css-loader",
                            "resolve-url-loader",
                            {
                                loader: "sass-loader",
                                options: {
                                    sourceMap: true,
                                    sourceMapContents: false
                                }
                            }
                        ]
                    }
                ]
            }
        ]
    );
    return config;
};

import { create } from "@storybook/theming";

export default create({
    base: "light",
    fontBase: '"Roboto", sans-serif',

    brandTitle: "mytour.vn"
});

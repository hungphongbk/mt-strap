import "./fw.scss";
import { configure, addParameters } from "@storybook/react";
import theme from "./theme";

addParameters({
    options: {
        theme,
        /**
         * Turn off config panel
         */
        showPanel: false,
        sortStoriesByKind: true
    }
});

// automatically import all files ending in *.stories.js
const req = require.context("../src/components", true, /.stories.js$/);
function loadStories() {
    req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);

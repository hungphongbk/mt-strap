import React from "react";
import { StaticQuery, graphql } from "gatsby";
import styles from "./ComponentAPI.module.scss";
import classnames from "classnames";
import Prism from "../core/prismjs";

const ComponentApi = ({ name, parent = name }) => (
    <StaticQuery
        query={graphql`
            query {
                allComponentApi {
                    nodes {
                        name
                        specs {
                            name
                            type
                            default
                            description
                            required
                        }
                    }
                }
            }
        `}
    >
        {data => {
            const component = data.allComponentApi.nodes.find(
                c => c.name === name
            );
            return (
                <div>
                    <h3 className={"mb-1"}>{name}</h3>
                    <pre className="language-javascript">
                        <code
                            className="language-javascript"
                            dangerouslySetInnerHTML={{
                                __html: Prism.highlight(
                                    `import ${name} from \"mt-strap/lib/es/${parent}/${name}\"`,
                                    Prism.languages.javascript,
                                    "javascript"
                                )
                            }}
                        />
                    </pre>
                    <table className={styles.Table}>
                        <thead>
                            <th>Name</th>
                            <th style={{ width: "160px" }}>Type</th>
                            <th>Default</th>
                            <th>Description</th>
                        </thead>
                        <tbody>
                            {component.specs.map((spec, index) => (
                                <tr
                                    key={index}
                                    className={classnames(
                                        spec.required ? styles.Required : null
                                    )}
                                >
                                    <td>{spec.name}</td>
                                    <td className={styles.Type}>{spec.type}</td>
                                    <td className={styles.Default}>
                                        {spec.default}
                                    </td>
                                    <td>{spec.description}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            );
        }}
    </StaticQuery>
);

export default ComponentApi;

import React from "react";
import { StaticQuery, graphql, Link } from "gatsby";
import Expandable from "../Expandable/Expandable";
import styles from "../Layout/Layout.module.scss";
import classnames from "classnames";
import sortBy from "lodash/sortBy";
import { createObserver } from "propserver";

const query = graphql`
        query {
            list: allMdx(sort: { fields: frontmatter___title, order: ASC }) {
                group(field: frontmatter___section) {
                    fieldValue
                    nodes {
                        id
                        fields {
                            slug
                        }
                        frontmatter {
                            title
                        }
                    }
                }
            }
        }
    `,
    sortGuides = [/^Gett/, /^Conte/, /^Comp/, /^Contr/];

const SidebarItem = ({ node, frontmatter }) => (
    <li
        key={node.id}
        className={classnames(
            styles.SidebarItem,
            node.frontmatter.title === frontmatter.title
                ? styles.SidebarItemActive
                : null
        )}
    >
        <Link key={node.id} to={node.fields.slug}>
            {node.frontmatter.title.replace(/^\d+\./, "")}
        </Link>
    </li>
);

const Sidebar = ({ frontmatter, className }) => {
    return (
        <div className={className}>
            <StaticQuery query={query}>
                {({ list }) =>
                    list.group &&
                    sortBy(list.group, ({ fieldValue }) =>
                        sortGuides.findIndex(v => v.test(fieldValue))
                    ).map(({ fieldValue, nodes }) => (
                        <Expandable key={fieldValue} panelTag={"ul"}>
                            <h3 key={"__id"}>{fieldValue}</h3>
                            {nodes.map(node => (
                                <SidebarItem
                                    key={node.id}
                                    node={node}
                                    frontmatter={frontmatter}
                                />
                            ))}
                        </Expandable>
                    ))
                }
            </StaticQuery>
        </div>
    );
};

export default Sidebar;

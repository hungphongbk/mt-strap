// TODO: #info prevent Prismjs re-highlight again (it caused an unexpected highlight friction)
if (typeof window !== "undefined") {
    if (typeof window.Prism !== "undefined") window.Prism.manual = true;
    else
        window.Prism = {
            manual: true
        };
}
const Prism = require("prismjs");
export default Prism;

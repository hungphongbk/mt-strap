import React from "react";
import styles from "./Layout.module.scss";
import "./Layout.scss";
import "../../sass/helpers/_spacing.scss";
import Sidebar from "../Sidebar/Sidebar";
import { Helmet } from "react-helmet";

import LinearIconWoff2 from "../../theme/fonts/font-linear/linear-icon.woff2";

const Layout = ({ children, pageContext: { frontmatter } }) => (
    <div className={styles.App}>
        <Helmet>
            <title>
                {frontmatter.title.replace(/^\d+\./, "")} -{" "}
                {frontmatter.section}
            </title>
            <link
                rel={"preload"}
                as={"font"}
                href={LinearIconWoff2}
                type={"font/woff2"}
                crossOrigin={"anonymous"}
            />
        </Helmet>
        <Sidebar className={styles.Sidebar} frontmatter={frontmatter} />
        <main className={styles.Content}>{children}</main>
    </div>
);

export default Layout;

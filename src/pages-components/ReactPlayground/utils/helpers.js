import React from "react";

export const addLineNumbers = (code = []) => {
    const numberOfLines = code.length === 0 ? 0 : code.split(`\n`).length;

    // Generate the container for the line numbers.
    // Relevant code in the Prism Line Numbers plugin can be found here:
    // https://github.com/PrismJS/prism/blob/f356dfe71bf126e6dc060c03f3e042de28a9bec4/plugins/line-numbers/prism-line-numbers.js#L99-L115
    return (
        <span
            aria-hidden="true"
            className="line-numbers-rows"
            style={{
                whiteSpace: "auto",
                left: 0,
                paddingTop: "1em",
                paddingBottom: "1em"
            }}
        >
            {Array(numberOfLines)
                .fill(0)
                .map((_, index) => (
                    <span key={index} />
                ))}
        </span>
    );
};

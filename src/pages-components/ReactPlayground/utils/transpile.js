import evalCode from "./evalCode";
import transform from "./transform";
import React from "react";

export const renderElementAsync = ({
    code = "",
    scope = {},
    transformed = false
}) =>
    new Promise(resolve => {
        evalCode(
            transformed
                ? code + ";render(__GATSBY_DEMO_RENDERED__.default);"
                : transform(code),
            {
                ...scope,
                render: Element => {
                    resolve(() =>
                        typeof Element === "function" ? <Element /> : Element
                    );
                }
            }
        );
    });

export const generateElement = ({ code = "", scope = {} }) => {
    const codeTrimmed = code.trim().replace(/;$/, "");
    const transformed = transform(`(${codeTrimmed})`).trim();
    return () => evalCode(`return ${transformed}`, scope);
};

import React from "react";
import { _poly } from "./transform";
import * as PropTypes from "prop-types";
import classNames from "classnames";

const evalCode = (code, scope) => {
    const newScope = Object.assign({}, scope, {
        PropTypes,
        classNames
    });

    const scopeKeys = Object.keys(newScope);
    const scopeValues = scopeKeys.map(key => newScope[key]);
    // eslint-disable-next-line no-new-func
    const res = new Function("_poly", "React", ...scopeKeys, code);
    return res(_poly, React, ...scopeValues);
};

export default evalCode;

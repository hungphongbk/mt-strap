import React from "react";
import prettier from "prettier/standalone";
import parserHTML from "prettier/parser-html";
import { addLineNumbers } from "./utils/helpers";
import Prism from "../core/prismjs";

const ReactPlaygroundHtml = React.memo(function({ html, printWidth, active }) {
    const [_html, _setHtml] = React.useState({
        html: "",
        lineNumbers: null
    });
    React.useEffect(
        function() {
            if (active) {
                const newHtml = prettier.format(html, {
                    parser: "html",
                    plugins: [parserHTML],
                    printWidth
                });
                _setHtml({
                    html: Prism.highlight(
                        newHtml,
                        Prism.languages.html,
                        "html"
                    ),
                    lineNumbers: addLineNumbers(newHtml)
                });
            }
        },
        [html, active]
    );

    return (
        <pre className={`line-numbers language-html`}>
            <code
                className={`language-html`}
                dangerouslySetInnerHTML={{
                    __html: _html.html
                }}
            />
            {_html.lineNumbers}
        </pre>
    );
});

export default ReactPlaygroundHtml;

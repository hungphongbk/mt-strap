import React from "react";
import reactElemToJSX from "react-element-to-jsx-string";
import Prism from "../core/prismjs";
import "prismjs/components/prism-jsx.js";
import * as PropTypes from "prop-types";
import prettier from "prettier/standalone";
import parserBabylon from "prettier/parser-babylon";
import parserHTML from "prettier/parser-html";
import styles from "./ReactPlayground.module.scss";
import { generateElement, renderElementAsync } from "./utils/transpile";
import * as components from "./components";
import Tabs from "../../components/Tabs/Tabs";
import Tab from "../../components/Tab/Tab";
import { useStaticQuery, graphql } from "gatsby";
import { MemoryRouter, withRouter } from "react-router-dom";
import { addLineNumbers } from "./utils/helpers";
import ReactPlaygroundHtml from "./ReactPlaygroundHtml";
// import MutationObserver from "mutation-observer";

const printWidth = 90,
    withJsxDemo = Component =>
        function(props) {
            if (!props.demo) return <Component {...props} />;
            const data = useStaticQuery(graphql`
                    {
                        demo: allComponentDemo {
                            nodes {
                                name
                                source
                                parsed
                            }
                        }
                    }
                `),
                demo = data.demo.nodes.find(node => node.name === props.demo);
            const newProps = { ...props, demo };
            return <Component {...newProps} />;
        },
    MemoryRouterPlayground = withRouter(
        ({ match, location, history, children }) => (
            <div>
                <div className={styles.PlayerNavigationBar}>
                    <div className={styles.PlayerNavigationBarAddress}>
                        {location.pathname}
                    </div>
                </div>
                {children}
            </div>
        )
    );

@withJsxDemo
class ReactPlayground extends React.PureComponent {
    state = {
        initialized: false,
        Rendered: "div",
        html: "",
        currentTab: "",
        jsxCode: "",
        jsxCodeHtml: ""
    };
    /**
     *
     * @type {MutationObserver}
     */
    observer = null;
    /**
     *
     * @type {HTMLElement}
     */
    el = null;
    constructor(props) {
        super(props);
        if (typeof window !== "undefined")
            this.observer = new MutationObserver(
                this.observerCallback.bind(this)
            );
    }
    componentWillMount() {
        this.renderChildren();
    }

    getJsxCode = () => {
        // IN CASE: code was imported from demo directory
        if (typeof this.props.demo === "object" && this.props.demo !== null) {
            const jsxCode = this.props.demo,
                _jsxCode = jsxCode.source,
                _jsxCodeParsed = jsxCode.parsed,
                numLinesNumbers = addLineNumbers(_jsxCode);

            return {
                jsxCode: _jsxCode,
                jsxCodeParsed: _jsxCodeParsed,
                numLinesNumbers
            };
        }

        let jsxCode = this.props.children;

        if (typeof jsxCode !== "string")
            jsxCode = reactElemToJSX(<>{children}</>, {
                maxInlineAttributesLineLength: 4,
                showDefaultProps: false
            }).replace(/<\/?>/g, "");
        jsxCode = prettier.format(jsxCode, {
            parser: "babel",
            plugins: [parserBabylon],
            printWidth,
            jsxBracketSameLine: true
        });

        const numLinesNumbers = addLineNumbers(jsxCode);
        return { jsxCode, numLinesNumbers };
    };

    doObserve = el => {
        if (!el) return;
        this.el = el;
        this.observerCallback();
        // eslint-disable-next-line no-unused-expressions
        this.observer?.observe(el, {
            attributes: true,
            childList: true,
            subtree: true
        });
    };
    observerCallback = () => {
        this.setState({
            initialized: true,
            html: this.el.innerHTML
        });
    };

    renderChildren = () => {
        const { children, inline, language } = this.props;
        if (typeof children !== "undefined" && typeof children !== "string")
            this.setState({
                Rendered: () => children,
                html: ""
            });
        else {
            const {
                jsxCode,
                jsxCodeParsed,
                numLinesNumbers
            } = this.getJsxCode();
            const update = Rendered => {
                const htmlifiedJsxCode = () => {
                    this.setState({
                        jsxCodeHtml: Prism.highlight(
                            jsxCode,
                            Prism.languages[language],
                            language
                        )
                    });
                };
                this.setState(
                    {
                        initialized: true,
                        Rendered,
                        jsxCode,
                        numLinesNumbers
                    },
                    () => requestAnimationFrame(htmlifiedJsxCode)
                );
            };
            if (!inline) {
                renderElementAsync({
                    code: jsxCodeParsed || jsxCode,
                    scope: { ...components },
                    transformed: typeof jsxCodeParsed === "string"
                }).then(update);
            } else {
                const Rendered = generateElement({
                    code: jsxCode,
                    scope: { ...components }
                });
                update(Rendered);
            }
        }
    };
    renderPlayground = () => {
        const { Rendered } = this.state,
            childFn = () => (
                <div className={styles.Player} ref={this.doObserve}>
                    <Rendered />
                </div>
            );
        if (!this.props.withRouter) return childFn();
        return (
            <MemoryRouter>
                <MemoryRouterPlayground>{childFn()}</MemoryRouterPlayground>
            </MemoryRouter>
        );
    };
    render() {
        if (!this.state.initialized) return <div />;
        const { language } = this.props,
            { jsxCode, numLinesNumbers } = this.state;
        // console.log();
        return (
            <div className={styles.ReactPlayground}>
                <Tabs onChange={currentTab => this.setState({ currentTab })}>
                    <Tab tabKey={"preview"} title={"Preview"}>
                        {this.renderPlayground()}
                    </Tab>
                    <Tab tabKey={"jsx"} title={"JSX"}>
                        <div className={styles.Preview}>
                            <pre
                                className={`line-numbers language-${language}`}
                            >
                                <code
                                    className={`language-${language}`}
                                    dangerouslySetInnerHTML={{
                                        __html: this.state.jsxCodeHtml
                                    }}
                                />
                                {numLinesNumbers}
                            </pre>
                        </div>
                    </Tab>
                    <Tab tabKey={"html"} title={"HTML"}>
                        <div className={styles.Preview}>
                            <ReactPlaygroundHtml
                                html={this.state.html}
                                printWidth={printWidth}
                                active={this.state.currentTab === "html"}
                            />
                        </div>
                    </Tab>
                </Tabs>
            </div>
        );
    }
}
ReactPlayground.propTypes = {
    language: PropTypes.string,
    inline: PropTypes.bool,
    demo: PropTypes.string,
    withRouter: PropTypes.bool
};
ReactPlayground.defaultProps = {
    language: "jsx",
    inline: false,
    demo: null,
    withRouter: false
};

export default ReactPlayground;

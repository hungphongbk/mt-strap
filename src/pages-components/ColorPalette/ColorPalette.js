import React from "react";
import styles from "./ColorPalette.module.scss";

const ColorPalette = ({ title, theme, description, palettes }) => (
    <div className={styles.Box}>
        <div
            className={`${styles.BoxHeader} ${
                theme !== "white" ? `mt-text-${theme}` : ""
            }`}
        >
            <h3 className={styles.BoxHeaderTitle}>
                {title}
                <small>{description}</small>
            </h3>
        </div>
        <div className={styles.BoxBody}>
            {palettes.map(([codeName, color, displayTextColor = ""]) => (
                <div
                    key={codeName}
                    className={`clearfix ${
                        styles.ColorBlock
                    } mt-bg-${codeName}`}
                >
                    <span
                        className={`pull-left ${
                            styles.ColorName
                        } ${displayTextColor}`}
                    >
                        .mt-bg-{codeName}
                        <br />${codeName}
                    </span>
                    <span
                        className={`pull-right ${
                            styles.ColorHex
                        } ${displayTextColor}`}
                    >
                        {color.toUpperCase()}
                    </span>
                </div>
            ))}
        </div>
    </div>
);

export default ColorPalette;

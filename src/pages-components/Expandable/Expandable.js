import React, { PureComponent } from "react";
import styles from "./Expandable.module.scss";
import * as PropTypes from "prop-types";

class Expandable extends PureComponent {
    static propTypes = {
        panelTag: PropTypes.string
    };
    static defaultProps = {
        panelTag: "div"
    };
    state = {
        isOpen: true,
        $panel: null
    };
    toggleOpen = () =>
        this.setState(state => ({
            isOpen: !state.isOpen
        }));
    render() {
        const [section, ...panelChildren] = React.Children.toArray(
                this.props.children
            ),
            panelStyle = (() => {
                if (!this.state.$panel) return {};
                return {
                    maxHeight: this.state.isOpen
                        ? this.state.$panel.scrollHeight + "px"
                        : 0
                };
            })(),
            PanelTag = this.props.panelTag;
        return (
            <div className={styles.Expandable}>
                <div
                    className={styles.ExpandableHeader}
                    // onClick={this.toggleOpen}
                >
                    {section}
                </div>
                <PanelTag
                    className={styles.Panel}
                    ref={$panel => $panel && this.setState({ $panel })}
                    // style={panelStyle}
                >
                    {panelChildren}
                </PanelTag>
            </div>
        );
    }
}

export default Expandable;

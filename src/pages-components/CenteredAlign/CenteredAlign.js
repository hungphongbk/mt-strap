import styled from "styled-components";
import * as PropTypes from "prop-types";

const CenteredAlign = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: ${props => props.direction};
`;
CenteredAlign.propTypes = {
    direction: PropTypes.string
};
CenteredAlign.defaultProps = {
    direction: "row"
};

export default CenteredAlign;

import React from "react";
import Button from "../components/Button/Button";
import Modal from "../components/Modal/Modal";
import ModalHeader from "../components/ModalHeader/ModalHeader";
import Typography from "../components/Typography/Typography";
import TextInput from "../components/TextInput/TextInput";

const Demo = () => {
    const [open, setOpen] = React.useState(false),
        [mail, setMail] = React.useState("");

    return (
        <div className={"mt-p-4"}>
            <Button variant={"blue"} onClick={() => setOpen(true)}>
                Open modal
            </Button>
            <Modal isOpen={open} toggle={setOpen} animation>
                <ModalHeader>
                    <Typography variant={"h4"}>Subscribe</Typography>
                </ModalHeader>
                <div className="mt-modal-body">
                    <Typography color={"blueberry-light-10"}>
                        To subscribe to this website, please enter your email
                        address here. We will send updates occasionally.
                    </Typography>
                    <TextInput
                        className={"mt-mt-3"}
                        onChange={setMail}
                        value={mail}
                    />
                </div>
                <div className="mt-modal-footer">
                    <Button
                        variant={"text-gray"}
                        onClick={() => setOpen(false)}
                    >
                        Cancel
                    </Button>
                    <Button variant={"blue"}>Subscribe</Button>
                </div>
            </Modal>
        </div>
    );
};

export default <Demo />;

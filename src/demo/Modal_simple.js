import React from "react";
import Modal from "../components/Modal/Modal";
import Typography from "../components/Typography/Typography";

const text =
    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa optio illo, exercitationem aperiam accusamus natus non eum enim incidunt dolore accusantium in magnam nesciunt officiis maiores distinctio ullam dignissimos. Dolor. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Totam tenetur, odio quam ratione ducimus perspiciatis cumque explicabo aliquam! Aperiam facilis officiis rerum quasi numquam veritatis corporis dolorem omnis ipsum? At?";

export default (
    <>
        <Modal isOpen={true} disablePortal>
            <div className="mt-modal-header">
                <Typography variant={"h4"}>Normal modal</Typography>
            </div>
            <div className="mt-modal-body">
                <p>{text}</p>
            </div>
        </Modal>
        <Modal isOpen={true} disablePortal size={"lg"} className={"mt-mt-4"}>
            <div className="mt-modal-header">
                <Typography variant={"h4"}>Large modal</Typography>
            </div>
            <div className="mt-modal-body">
                <p>{text}</p>
            </div>
        </Modal>
    </>
);

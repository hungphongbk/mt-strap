import React from "react";
import { cloneDeep } from "lodash";
import TextInput from "../components/TextInput/TextInput";
import Typography from "../components/Typography/Typography";

class InputSimpleDemo extends React.Component {
    state = {
        values: {}
    };

    handleChange = name => value =>
        this.setState(state => {
            const newState = cloneDeep(state);
            newState.values[name] = value;
            return newState;
        });

    render() {
        const { values } = this.state;
        return (
            <div className="mt-row">
                <div className="mt-col-sm-12">
                    <Typography
                        variant={"h4"}
                        color={"blueberry-light-20"}
                        className={"mt-pt-1 mt-pb-1"}
                    >
                        INPUT SIZE DEFAULT
                    </Typography>
                </div>
                <div className="mt-col-sm-4">
                    <Typography
                        variant={"h6"}
                        color={"blueberry-light-20"}
                        className={"mt-pt-1 mt-pb-1"}
                    >
                        TEXT INPUTS
                    </Typography>
                    <TextInput
                        label={"Normal text input"}
                        className={"mt-mb-1"}
                        value={values["text1"]}
                        onChange={this.handleChange("text1")}
                        placeholder={"Text input"}
                    />
                    <TextInput
                        className={"mt-mb-1"}
                        onChange={this.handleChange("text1")}
                        value={values["text1"]}
                        placeholder={"Without label"}
                    />
                    <TextInput
                        label={"Normal text input"}
                        helperText={"And this is a helper text."}
                        className={"mt-mb-1"}
                        onChange={this.handleChange("text2")}
                        value={values["text2"]}
                        placeholder={"Text input with helper"}
                    />
                    <TextInput
                        label={"Error text input"}
                        className={"mt-mb-1"}
                        onChange={this.handleChange("text3")}
                        value={values["text3"]}
                        placeholder={"Text input with helper"}
                        error={"Something went wrong!"}
                    />
                    <TextInput
                        label={"Success text input"}
                        className={"mt-mb-1"}
                        onChange={this.handleChange("text4")}
                        value={values["text4"]}
                        placeholder={"Text input with helper"}
                        success={"You're okay :)"}
                    />
                    <TextInput
                        disabled
                        label={"Disabled text input"}
                        className={"mt-mb-1"}
                        onChange={this.handleChange("text4")}
                        value={values["text4"]}
                        placeholder={"This is a disabled text input"}
                    />
                </div>
                <div className="mt-col-sm-4">
                    <Typography
                        variant={"h6"}
                        color={"blueberry-light-20"}
                        className={"mt-pt-1 mt-pb-1"}
                    >
                        WITH ICONS
                    </Typography>
                    <TextInput
                        label={"Normal text input"}
                        className={"mt-mb-1"}
                        onChange={this.handleChange("text1")}
                        value={values["text1"]}
                        placeholder={"Text input"}
                        icon={<i className={"li-calendar-empty"} />}
                    />
                    <TextInput
                        className={"mt-mb-1"}
                        onChange={this.handleChange("text1")}
                        value={values["text1"]}
                        placeholder={"Without label"}
                        icon={<i className={"li-calendar-empty"} />}
                    />
                    <TextInput
                        label={"Normal text input"}
                        helperText={"And this is a helper text."}
                        className={"mt-mb-1"}
                        onChange={values["text2"]}
                        value={this.handleChange("text2")}
                        placeholder={"Text input with helper"}
                        icon={<i className={"li-user"} />}
                    />
                    <TextInput
                        label={"Error text input"}
                        className={"mt-mb-1"}
                        onChange={values["text3"]}
                        value={this.handleChange("text3")}
                        placeholder={"Text input with helper"}
                        error={"Something went wrong!"}
                        icon={<i className={"li-calendar-empty"} />}
                    />
                    <TextInput
                        label={"Success text input"}
                        className={"mt-mb-1"}
                        onChange={values["text4"]}
                        value={this.handleChange("text4")}
                        placeholder={"Text input with helper"}
                        success={"You're okay :)"}
                        icon={<i className={"li-user"} />}
                    />
                    <TextInput
                        disabled
                        label={"Disabled text input"}
                        className={"mt-mb-1"}
                        onChange={values["text4"]}
                        value={this.handleChange("text4")}
                        placeholder={"This is a disabled text input"}
                        icon={<i className={"li-user"} />}
                    />
                </div>
                <div className="mt-col-sm-4">
                    <Typography
                        variant={"h6"}
                        color={"blueberry-light-20"}
                        className={"mt-pt-1 mt-pb-1"}
                    >
                        WITH ICONS (RIGHT SIDE)
                    </Typography>
                    <TextInput
                        label={"Normal text input"}
                        className={"mt-mb-1"}
                        onChange={values["text1"]}
                        value={this.handleChange("text1")}
                        placeholder={"Text input"}
                        icon={<i className={"li-calendar-empty"} />}
                        iconPosition={"right"}
                    />
                    <TextInput
                        className={"mt-mb-1"}
                        onChange={values["text1"]}
                        value={this.handleChange("text1")}
                        placeholder={"Without label"}
                        icon={<i className={"li-calendar-empty"} />}
                        iconPosition={"right"}
                    />
                    <TextInput
                        label={"Normal text input"}
                        helperText={"And this is a helper text."}
                        className={"mt-mb-1"}
                        onChange={values["text2"]}
                        value={this.handleChange("text2")}
                        placeholder={"Text input with helper"}
                        icon={<i className={"li-user"} />}
                        iconPosition={"right"}
                    />
                    <TextInput
                        label={"Error text input"}
                        className={"mt-mb-1"}
                        onChange={values["text3"]}
                        value={this.handleChange("text3")}
                        placeholder={"Text input with helper"}
                        error={"Something went wrong!"}
                        icon={<i className={"li-calendar-empty"} />}
                        iconPosition={"right"}
                    />
                    <TextInput
                        label={"Success text input"}
                        className={"mt-mb-1"}
                        onChange={values["text4"]}
                        value={this.handleChange("text4")}
                        placeholder={"Text input with helper"}
                        success={"You're okay :)"}
                        icon={<i className={"li-user"} />}
                        iconPosition={"right"}
                    />
                    <TextInput
                        disabled
                        label={"Disabled text input"}
                        className={"mt-mb-1"}
                        onChange={values["text4"]}
                        value={this.handleChange("text4")}
                        placeholder={"This is a disabled text input"}
                        iconPosition={"right"}
                    />
                </div>
            </div>
        );
    }
}

export default <InputSimpleDemo />;

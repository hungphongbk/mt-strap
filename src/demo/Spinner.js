import React from "react";
import Spinner from "../components/Spinner/Spinner";

const SpinnerDemo = () => {
    const [value, setValue] = React.useState(10);

    return (
        <div style={{ width: "200px" }}>
            <Spinner value={value} onChange={setValue} />
        </div>
    );
};

export default <SpinnerDemo />;

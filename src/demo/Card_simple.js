import React from "react";
import Card from "../components/Card/Card";
import CardBody from "../components/CardBody/CardBody";
import Typography from "../components/Typography/Typography";
import Row from "../components/Row/Row";
import Col from "../components/Col/Col";
import CardImg from "../components/CardImg/CardImg";

const Demo = () => (
    <Row>
        <Col xs={6} md={4}>
            <Typography
                variant={"h5"}
                color={"blueberry-light-20"}
                className={"mt-mb-3"}
            >
                Title and text
            </Typography>
            <Card style={{ width: "100%" }}>
                <CardBody>
                    <Typography variant={"h5"}>Starting from $522</Typography>
                    <p>
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                    </p>
                </CardBody>
            </Card>
        </Col>
        <Col xs={6} md={4}>
            <Typography
                variant={"h5"}
                color={"blueberry-light-20"}
                className={"mt-mb-3"}
            >
                Title and text and image
            </Typography>
            <Card style={{ width: "100%" }}>
                <CardImg
                    src={"https://i.imgur.com/z2hbjyL.jpg"}
                    alt={"card with image 1"}
                    ratio={"4-3"}
                />
                <CardBody>
                    <Typography variant={"h5"}>Starting from $522</Typography>
                    <p>
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                    </p>
                </CardBody>
            </Card>
        </Col>
        <Col xs={6} md={4}>
            <Typography
                variant={"h5"}
                color={"blueberry-light-20"}
                className={"mt-mb-3"}
            >
                Image and text inside
            </Typography>
            <Card style={{ width: "100%" }}>
                <CardImg
                    src={"https://i.imgur.com/z2hbjyL.jpg"}
                    alt={"card with image 1"}
                    ratio={"4-3"}
                >
                    <CardBody>
                        <Typography variant={"h5"} color={"white"}>
                            Starting from $522
                        </Typography>
                        <p className={"mt-text-white"}>
                            Some quick example text to build on the card title
                            and make up the bulk of the card's content.
                        </p>
                    </CardBody>
                </CardImg>
            </Card>
        </Col>
    </Row>
);

export default <Demo />;

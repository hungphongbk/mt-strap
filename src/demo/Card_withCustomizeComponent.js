import React from "react";
import { Link, Route, withRouter } from "react-router-dom";
import Card from "../components/Card/Card";
import Row from "../components/Row/Row";
import Col from "../components/Col/Col";
import CardBody from "../components/CardBody/CardBody";
import Typography from "../components/Typography/Typography";

const titleMapping = {
    "standard-room": "standard room",
    "deluxe-room": "deluxe room",
    "luxury-room": "luxury room"
};

const DemoContent = withRouter(props => (
        <div className="mt-mb-4">
            <Typography variant={"h4"} color={"blue-dark-20"}>
                This is a {titleMapping[props.match.params.room]}
            </Typography>
            <p className={"mt-text-green-dark-20"}>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Delectus deleniti id illo inventore ipsam nisi, porro quidem
                reiciendis totam voluptates.
            </p>
        </div>
    )),
    Demo = () => (
        <>
            <Route path={"/:room"} component={DemoContent} />
            <Row>
                <Col sm={4}>
                    <Card
                        component={Link}
                        to={"/standard-room"}
                        style={{ width: "100%" }}
                    >
                        <CardBody>
                            <Typography variant={"h5"} color={"blueberry"}>
                                Standard Room
                            </Typography>
                        </CardBody>
                    </Card>
                </Col>
                <Col sm={4}>
                    <Card
                        component={Link}
                        to={"/deluxe-room"}
                        style={{ width: "100%" }}
                    >
                        <CardBody>
                            <Typography variant={"h5"} color={"blueberry"}>
                                Deluxe Room
                            </Typography>
                        </CardBody>
                    </Card>
                </Col>
                <Col sm={4}>
                    <Card
                        component={Link}
                        to={"/luxury-room"}
                        style={{ width: "100%" }}
                    >
                        <CardBody>
                            <Typography variant={"h5"} color={"blueberry"}>
                                Luxury Room
                            </Typography>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </>
    );

export default <Demo />;

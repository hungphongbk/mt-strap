import React from "react";
import Checkbox from "../components/Checkbox/Checkbox";
import Container from "../components/Container/Container";
import Row from "../components/Row/Row";
import Col from "../components/Col/Col";
import Typography from "../components/Typography/Typography";

const noop = () => {},
    Demo = () => (
        <Container>
            <Row>
                <Col sm={6}>
                    <Typography variant={"h4"}>Default size</Typography>
                    <Checkbox checked={false} onChange={noop}>
                        Default checkbox (hover mouse to see UI changes)
                    </Checkbox>
                    <Checkbox checked onChange={noop}>
                        Checked checkbox
                    </Checkbox>
                    <Checkbox checked onChange={noop} disabled={true}>
                        Checked checkbox (disabled)
                    </Checkbox>
                    <Checkbox checked={false} onChange={noop} disabled={true}>
                        Unchecked checkbox (disabled)
                    </Checkbox>
                </Col>
                <Col sm={6}>
                    <Typography variant={"h4"}>Large size</Typography>
                    <Checkbox checked={false} onChange={noop} size={"lg"}>
                        Default checkbox (hover mouse to see UI changes)
                    </Checkbox>
                    <Checkbox checked onChange={noop} size={"lg"}>
                        Checked checkbox
                    </Checkbox>
                    <Checkbox
                        checked
                        onChange={noop}
                        disabled={true}
                        size={"lg"}
                    >
                        Checked checkbox (disabled)
                    </Checkbox>
                    <Checkbox
                        checked={false}
                        onChange={noop}
                        disabled={true}
                        size={"lg"}
                    >
                        Unchecked checkbox (disabled)
                    </Checkbox>
                </Col>
            </Row>
        </Container>
    );

export default <Demo />;

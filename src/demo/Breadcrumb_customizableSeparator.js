import React from "react";
import Breadcrumb from "../components/Breadcrumb/Breadcrumb";
import { Link } from "react-router-dom";

export default (
    <Breadcrumb separator={<span>/</span>}>
        <Link to={"/"} isActive={() => false}>
            Home
        </Link>
        <Link to={"/foo"} isActive={() => false}>
            Foo
        </Link>
        <Link to={"/bar"} isActive={() => true}>
            Bar
        </Link>
    </Breadcrumb>
);

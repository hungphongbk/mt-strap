import React from "react";
import Pagination from "../components/Pagination/Pagination";

export default (
    <div
        style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            margin: "2.5rem auto"
        }}
    >
        <Pagination pageCount={10} initialPage={1} />
    </div>
);

import React from "react";
import Card from "../components/Card/Card";
import CardBody from "../components/CardBody/CardBody";
import Typography from "../components/Typography/Typography";
import Collapse from "../components/Collapse/Collapse";
import CardImg from "../components/CardImg/CardImg";

const text =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";

const Demo = () => {
    const [open, setOpen] = React.useState(false);
    return (
        <div style={{ minHeight: "80vh" }}>
            <Card style={{ width: "400px" }}>
                <CardImg
                    src={"https://i.imgur.com/z2hbjyL.jpg"}
                    alt={"card with image 1"}
                    ratio={"5-3"}
                />
                <CardBody>
                    <Typography
                        color={"blueberry"}
                        variant={"h5"}
                        className={"mt-mb-2"}
                    >
                        Collapsable card
                    </Typography>
                    <Typography>
                        <a href={"javascript:;"} onClick={() => setOpen(!open)}>
                            {open ? "Collapse" : "See something"}
                        </a>
                    </Typography>
                    <Collapse isOpen={open}>
                        {<p className={"mt-text-blueberry"}>{text}</p>}
                    </Collapse>
                </CardBody>
            </Card>
        </div>
    );
};

export default <Demo />;

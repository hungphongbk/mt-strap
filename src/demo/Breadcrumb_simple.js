import React from "react";
import Breadcrumb from "../components/Breadcrumb/Breadcrumb";
import { Link, Route, Switch, withRouter } from "react-router-dom";

const withBreadcrumb = Component =>
    withRouter(props => {
        return (
            <>
                <Breadcrumb>
                    <Link to={"/"} isActive={match => match.isExact}>
                        Home
                    </Link>
                    {/posts/.test(props.location.pathname) && (
                        <Link to={"/posts"} isActive={match => match.isExact}>
                            Post List
                        </Link>
                    )}
                    {/posts\//.test(props.location.pathname) && (
                        <Link
                            to={props.location.pathname}
                            isActive={match => match.isExact}
                        >
                            A post
                        </Link>
                    )}
                </Breadcrumb>
                <Component {...props} />
            </>
        );
    });

const Home = withBreadcrumb(() => (
    <div>
        <h3>Home</h3>
        <hr />
        <Link to={"/posts"}>Go to post list</Link>
    </div>
));

const Posts = withBreadcrumb(({ match: { url } }) => (
    <div>
        <h3>Post List</h3>
        <hr />
        <ul>
            <li>
                <Link to={`${url}/red`}>Red post</Link>
            </li>
            <li>
                <Link to={`${url}/green`}>Green post</Link>
            </li>
            <li>
                <Link to={`${url}/blue`}>Blue post</Link>
            </li>
            <li>
                <Link to={`/`}>
                    <i>Return to home</i>
                </Link>
            </li>
        </ul>
    </div>
));

const Post = withBreadcrumb(({ match: { params } }) => (
    <div>
        <h3 style={{ color: params.color }}>
            Hi there, this is a/an {params.color} post!
        </h3>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Exercitationem, iusto?
        </p>
    </div>
));

const BreadCrumbExample = () => (
    <Switch>
        <Route exact path={"/"} component={Home} />
        <Route exact path={"/posts"} component={Posts} />
        <Route path={"/posts/:color"} component={Post} />
    </Switch>
);

export default <BreadCrumbExample />;

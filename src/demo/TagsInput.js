import React from "react";
import TagsInput from "../components/TagsInput/TagsInput";

const Demo = () => {
    const [tags, setTags] = React.useState(["Item #1", "Item #2"]);
    return (
        <div className={"mt-row"}>
            <div className="mt-col-xsml-7">
                <TagsInput onChange={setTags} value={tags} />
            </div>
        </div>
    );
};

export default <Demo />;

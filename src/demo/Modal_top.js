import React from "react";
import Button from "../components/Button/Button";
import Modal from "../components/Modal/Modal";
import Typography from "../components/Typography/Typography";
import ModalHeader from "../components/ModalHeader/ModalHeader";
import Radio from "../components/Radio/Radio";

function Demo() {
    const [open, setOpen] = React.useState(false),
        [position, setPosition] = React.useState("center");

    return (
        <div className={"mt-p-4"}>
            <p>
                <span className="mt-mr-4">Select dialog position</span>
                <Radio
                    name={"position"}
                    inline
                    value={"center"}
                    checked={position === "center"}
                    onChange={setPosition}
                >
                    Center
                </Radio>
                <Radio
                    name={"position"}
                    inline
                    value={"top"}
                    checked={position === "top"}
                    onChange={setPosition}
                >
                    Top
                </Radio>
            </p>
            <Button variant={"blue"} onClick={() => setOpen(true)}>
                Open {position} modal
            </Button>
            <Modal isOpen={open} toggle={setOpen} position={position}>
                <ModalHeader>
                    <Typography variant={"h4"}>Normal modal</Typography>
                </ModalHeader>
                <div className="mt-modal-body">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit. Inventore ipsum maxime mollitia possimus similique
                        ut!
                    </p>
                </div>
            </Modal>
        </div>
    );
}

export default <Demo />;

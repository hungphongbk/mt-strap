import React from "react";
import Table from "../components/Table/Table";

export default (
    <Table>
        <thead>
            <tr>
                <th>Name</th>
                <th>Subject</th>
                <th>Price</th>
                <th>Paid by</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Minie Ferguson</td>
                <td>Easy One Page Dashboard</td>
                <td>$150</td>
                <td>3 Days Ago</td>
            </tr>
            <tr>
                <td>Minie Ferguson</td>
                <td>Easy One Page Dashboard</td>
                <td>$150</td>
                <td>3 Days Ago</td>
            </tr>
            <tr>
                <td>Minie Ferguson</td>
                <td>Easy One Page Dashboard</td>
                <td>$150</td>
                <td>3 Days Ago</td>
            </tr>
        </tbody>
    </Table>
);

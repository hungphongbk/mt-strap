import React from "react";
import classnames from "classnames";
import * as PropTypes from "prop-types";
import Table from "../components/Table/Table";
import TableSortLabel from "../components/TableSortLabel/TableSortLabel";
import { orderBy } from "lodash";

const createLine = (
        project,
        author,
        deadline,
        deadlineStatus,
        leader,
        team,
        budget,
        budgetStatus,
        status
    ) => ({
        project,
        author,
        deadline,
        deadlineStatus,
        leader,
        team,
        budget,
        budgetStatus,
        status
    }),
    data = [
        createLine(
            "New Dashboard",
            "Google",
            "17th Oct, 2015",
            "Overdue",
            "Norman Hammond",
            "UK Design Team",
            4760,
            "Paid",
            "Running Smoothly"
        ),
        createLine(
            "Creative Popups",
            "Badoo",
            "9th Feb, 2016",
            "Overdue",
            "Carol Brentson",
            "Design Lead",
            950,
            "Paid",
            "Running Smoothly"
        ),
        createLine(
            "Spam with 10% Open Rate",
            "Nigeria",
            "15th May, 17",
            "65 Days Remaining",
            "Jake Ward",
            "President",
            3900,
            "Not-Paid",
            "Pending Invoice"
        )
    ];

class SortableTable extends React.Component {
    static propTypes = {
        data: PropTypes.array.isRequired
    };
    state = {
        sortState: {}
    };

    setSort = field => value =>
        this.setState(state => {
            const newState = { ...state };
            newState.sortState[field] = value;
            return newState;
        });

    sortedData = () => {
        const { sortState } = this.state,
            keys = [],
            orders = [];
        for (const field in sortState)
            if (sortState.hasOwnProperty(field)) {
                if (sortState[field] === "asc" || sortState[field] === "desc") {
                    keys.push(field);
                    orders.push(sortState[field]);
                }
            }
        return orderBy(this.props.data, keys, orders);
    };

    renderHead = () => (
        <thead>
            <tr>
                <th>Project</th>
                <th>Deadline</th>
                <th>Leader + Team</th>
                <th>
                    Budget
                    <TableSortLabel
                        sortValue={this.state.sortState.budget}
                        onChange={this.setSort("budget")}
                    />
                </th>
                <td>Status</td>
            </tr>
        </thead>
    );

    renderItem = (item, index) => (
        <tr key={index}>
            <td>
                <strong>{item.project}</strong>
                <p className="mt-text-desc mt-text-muted mt-mb-0">
                    {item.author}
                </p>
            </td>
            <td>
                <strong>{item.deadline}</strong>
                <p
                    className={classnames(
                        "mt-text-desc mt-mb-0",
                        /overdue/.test(item.deadlineStatus.toLowerCase())
                            ? "mt-text-red"
                            : "mt-text-muted"
                    )}
                >
                    {item.deadlineStatus}
                </p>
            </td>
            <td>
                <strong>{item.leader}</strong>
                <p className="mt-text-desc mt-mb-0 mt-text-muted">
                    {item.team}
                </p>
            </td>
            <td>
                <strong>${item.budget}</strong>
                <p
                    className={classnames(
                        "mt-text-desc mt-mb-0",
                        /not/.test(item.budgetStatus.toLowerCase())
                            ? "mt-text-red"
                            : "mt-text-muted"
                    )}
                >
                    {item.budgetStatus}
                </p>
            </td>
            <td />
        </tr>
    );

    render() {
        return (
            <Table>
                {this.renderHead()}
                {this.sortedData().map(this.renderItem.bind(this))}
            </Table>
        );
    }
}

export default <SortableTable data={data} />;

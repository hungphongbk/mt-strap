import React from "react";
import Switch from "../components/Switch/Switch";

const Demo = () => {
    const [on, setOn] = React.useState(false);
    return (
        <Switch value={on} onChange={setOn}>
            {on ? "on" : "off"}
        </Switch>
    );
};

export default <Demo />;

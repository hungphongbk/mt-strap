import React from "react";
import ValueSlider from "../components/ValueSlider/ValueSlider";

class DoubleSliderDemo extends React.Component {
    state = {
        value: { min: 100000, max: 2000000 }
    };

    render() {
        return (
            <div style={{ padding: "2rem 25%" }}>
                <ValueSlider
                    type={"range"}
                    min={0}
                    max={10000000}
                    step={1000}
                    value={this.state.value}
                    rippleEffect={this.props.rippleEffect}
                    onChange={value => this.setState({ value })}
                />
                <p>
                    Min value: <strong>{this.state.value.min}</strong> | Max
                    value: <strong>{this.state.value.max}</strong>
                </p>
            </div>
        );
    }
}
export default <DoubleSliderDemo rippleEffect={false} />;

import React from "react";
import Button from "../components/Button/Button";
import Modal from "../components/Modal/Modal";
import Typography from "../components/Typography/Typography";
import ModalHeader from "../components/ModalHeader/ModalHeader";
import Radio from "../components/Radio/Radio";

function Demo() {
    const [open, setOpen] = React.useState(false);
    return (
        <div className={"mt-p-4"}>
            <Button variant={"blue"} onClick={() => setOpen(true)}>
                Open modal
            </Button>
            <Modal isOpen={open} toggle={setOpen} disableBackdropClick={true}>
                <ModalHeader>
                    <Typography variant={"h4"}>Normal modal</Typography>
                </ModalHeader>
                <div className="mt-modal-body">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit. Inventore ipsum maxime mollitia possimus similique
                        ut!
                    </p>
                </div>
            </Modal>
        </div>
    );
}

export default <Demo />;

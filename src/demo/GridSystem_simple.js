import React from "react";
import injectSheet from "react-jss";
import Container from "../components/Container/Container";
import Col from "../components/Col/Col";
import Row from "../components/Row/Row";
import _ from "lodash";

const classNames = {
    card: {
        backgroundColor: "#0093cb",
        border: "1px solid #006a94",
        color: "white",
        fontSize: ".9em"
    },
    content: {
        padding: ".3em .5rem",
        width: "100%",
        whiteSpace: "pre-wrap"
    }
};

const Demo = injectSheet(classNames)(({ classes }) => {
    const Cell = props => (
        <Col {...props} className={classes.card}>
            <div className={classes.content}>
                {Object.entries(props)
                    .map(([prop, value]) => `.mt-col-${prop}-${value}`)
                    .join("\n")}
            </div>
        </Col>
    );

    return (
        <Container>
            <Row>
                {_.range(0, 12).map(i => (
                    <Cell key={i} sm={2} md={1} />
                ))}
                <Cell key={13} md={8} />
                <Cell key={14} md={4} />
            </Row>
        </Container>
    );
});

export default <Demo />;

import React from "react";
import Table from "../components/Table/Table";
import Avatar from "../components/Avatar/Avatar";
import Switch from "../components/Switch/Switch";

export default (
    <div>
        <Table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Subject</th>
                    <th>Price</th>
                    <th>Paid by</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <Avatar
                            src="https://i.pinimg.com/564x/cd/11/f8/cd11f85b631d9244b647e179b898c014.jpg"
                            alt=""
                        />
                        <span>Minie Ferguson</span>
                    </td>
                    <td>Easy One Page Dashboard</td>
                    <td>$150</td>
                    <td>3 Days Ago</td>
                </tr>
                <tr>
                    <td>
                        <Avatar
                            src="https://i.pinimg.com/564x/b6/0e/12/b60e127c5c370adfebf714972d12669d.jpg"
                            alt=""
                        />
                        <span>Caleb Castillo</span>
                    </td>
                    <td>Easy One Page Dashboard</td>
                    <td>$150</td>
                    <td>3 Days Ago</td>
                </tr>
                <tr>
                    <td>
                        <Avatar
                            src="https://i.pinimg.com/564x/d7/53/19/d753198a8d19472b85a9d23b0fdfa08c.jpg"
                            alt=""
                        />
                        <span>Ann Hunter</span>
                    </td>
                    <td>Easy One Page Dashboard</td>
                    <td>$150</td>
                    <td>3 Days Ago</td>
                </tr>
            </tbody>
        </Table>
        <br />
        <Table>
            <thead>
                <tr>
                    <th>Status</th>
                    <th>Mission Name</th>
                    <th>Activated</th>
                    <th>People Involved</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <Switch value={true} />
                    </td>
                    <td>
                        <strong>From Russia with Love (1963)</strong>
                    </td>
                    <td>
                        <span className="mt-text-muted">3 Days Ago</span>
                    </td>
                    <td>
                        <strong>548</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <Switch value={false} />
                    </td>
                    <td>
                        <strong>Gold Finger (1964)</strong>
                    </td>
                    <td>
                        <span className="mt-text-muted">4 Days Ago</span>
                    </td>
                    <td>
                        <strong>881</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <Switch value={false} />
                    </td>
                    <td>
                        <strong>The perks of being a wallflower (2010)</strong>
                    </td>
                    <td>
                        <span className="mt-text-muted">7 Days Ago</span>
                    </td>
                    <td>
                        <strong>874</strong>
                    </td>
                </tr>
            </tbody>
        </Table>
    </div>
);

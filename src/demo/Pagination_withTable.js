import React from "react";
import Pagination from "../components/Pagination/Pagination";
import Table from "../components/Table/Table";

const data = [
    ["Kiara B. Slater", "4382 Egestas, St.", "(0131) 883 8419", 3],
    ["Beau G. Ortiz", "P.O. Box 672, 3321 Lacus. Av.", "0800 1111", 4],
    ["Xander B. Jordan", "821-6979 Eleifend. Road", "(01408) 93736", 7],
    ["Felix B. Marshall", "P.O. Box 584, 3455 Elementum Ave", "0845 46 45", 5],
    ["Hammett X. Keith", "Ap #113-6974 Elementum Avenue", "055 7915 3511", 4],
    ["Wing S. Kidd", "7870 Placerat. Avenue", "(01049) 04500", 3],
    ["Leandra A. Doyle", "Ap #800-1767 Nisl Ave", "070 5634 2469", 9],
    ["Barclay S. Chambers", "P.O. Box 117, 3871 Nisi Ave", "(01002) 687719", 1],
    ["Andrew O. Duke", "672 Donec St.", "055 8109 3586", 2],
    ["Simon D. Marsh", "9990 In Avenue", "0800 495154", 9],
    ["Danielle T. Vazquez", "336-9574 Dictum St.", "0800 299 6420", 2],
    ["Kimberley E. Hampton", "695-5246 Proin Street", "0800 1111", 10],
    ["Galena G. Ballard", "8292 Sed, Rd.", "(01435) 756220", 2],
    ["Sloane K. Clark", "P.O. Box 883, 5680 A, Avenue", "0800 482 8298", 4],
    ["Blossom P. Leonard", "Ap #652-8920 Cras St.", "070 0942 2535", 8],
    ["Clarke A. Walton", "Ap #926-1160 Tortor. Road", "(016977) 5083", 7],
    ["Kato I. Ayers", "Ap #507-299 Nunc Street", "0924 973 9095", 9],
    ["Alexander M. Wheeler", "8204 Sit Ave", "076 2331 0053", 6],
    ["Shay E. Herring", "3858 Id, St.", "(016977) 5254", 6],
    ["Jescie Q. Haney", "P.O. Box 826, 3093 Mi, Av.", "055 7099 8894", 5],
    ["Jessamine P. English", "Ap #238-7512 Aliquam Avenue", "0800 1111", 7],
    ["Whitney A. Long", "4621 At St.", "076 7382 1524", 5],
    ["Hop S. Herring", "P.O. Box 588, 8505 Eu, Ave", "(0141) 239 6146", 3],
    ["Christine M. Lester", "Ap #987-8412 Feugiat Rd.", "(012043) 26020", 9],
    ["Ronan M. Case", "404-233 Bibendum St.", "076 4618 9447", 7],
    ["Mufutau W. Harris", "946-815 Nullam Road", "(022) 5323 5473", 7],
    ["Damian C. Mccarty", "1518 Tempus, Rd.", "0361 325 0583", 8],
    ["Olympia Z. Ramsey", "P.O. Box 832, 1174 Risus. St.", "070 6523 9131", 5],
    ["Ray T. Hicks", "2780 Blandit Rd.", "0938 692 2111", 7],
    ["Hector V. Hunt", "Ap #590-1971 Aliquam Road", "07206 377514", 2],
    ["Mufutau O. Calderon", "5462 Sem, Avenue", "(016977) 5864", 7],
    ["Briar Z. Arnold", "Ap #605-6927 Rhoncus St.", "(01791) 038373", 2],
    ["Akeem W. Jacobson", "144-6090 Id Rd.", "(029) 6790 8002", 9],
    ["Axel P. Moran", "6402 Magna. Rd.", "0500 472825", 6],
    ["Deacon Q. Good", "654-4722 Urna. Road", "(01387) 064177", 3],
    ["Willow T. Wood", "573 Ac Rd.", "0500 146208", 6],
    ["Aquila K. Stuart", "6581 Nam Avenue", "(0171) 182 2793", 3],
    ["Isabelle Z. Osborn", "P.O. Box 606, 8665 Nisi. Avenue", "0845 46 42", 2],
    ["Ian A. Pennington", "P.O. Box 872, 6078 Fusce St.", "0341 006 9882", 5],
    ["Rajah D. Rocha", "435-5742 Magna. St.", "(01250) 45416", 7],
    ["Odysseus V. Reyes", "308-8059 Neque. Road", "07624 777096", 2],
    ["Summer M. Miles", "586-2356 Sed, Av.", "(0171) 433 6513", 8],
    ["Hamilton I. Bullock", "4042 Pede Street", "055 3067 1897", 4],
    ["Richard C. Rutledge", "2209 Mus. St.", "07902 975979", 8]
];

class PaginationWithTable extends React.Component {
    state = {
        data: data,
        currentPage: 0,
        perPage: 5,
        totalPage: Math.floor(data.length / 5)
    };
    getTableList = () => {
        const offset = Math.ceil(this.state.currentPage * this.state.perPage);

        return this.state.data.slice(offset, offset + this.state.perPage);
    };
    render() {
        return (
            <div style={{ padding: "2rem" }}>
                <Table>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Phone number</th>
                            <th>KPI</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.getTableList().map((row, index) => (
                            <tr key={index}>
                                <td>{row[0]}</td>
                                <td>{row[1]}</td>
                                <td>{row[2]}</td>
                                <td>{row[3]}</td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
                <br />
                <Pagination
                    pageCount={this.state.totalPage}
                    initialPage={this.state.currentPage}
                    onPageChange={e =>
                        this.setState({
                            currentPage: e.selected
                        })
                    }
                />
            </div>
        );
    }
}

export default <PaginationWithTable />;

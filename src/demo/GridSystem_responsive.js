import React from "react";
import Row from "../components/Row/Row";
import injectSheet from "react-jss";
import Col from "../components/Col/Col";

const classNames = {
    card: {
        backgroundColor: "#0093cb",
        border: "1px solid #006a94",
        color: "white",
        fontSize: ".9em"
    },
    content: {
        padding: ".3em .5rem",
        width: "100%",
        whiteSpace: "pre-wrap"
    }
};

const Demo = injectSheet(classNames)(({ classes }) => {
    const Cell = props => (
        <Col {...props} className={classes.card}>
            <div className={classes.content}>
                {Object.entries(props)
                    .map(([prop, value]) => `.mt-col-${prop}-${value}`)
                    .join("\n")}
            </div>
        </Col>
    );

    return (
        <div>
            <Row>
                <Cell xsml={12} md={8} />
                <Cell xsml={6} md={4} />
            </Row>
            <Row>
                <Cell xsml={6} sm={4} />
                <Cell xsml={6} sm={4} />
                <Cell xsml={6} sm={4} />
            </Row>
            <Row>
                <Cell xsml={6} />
                <Cell xsml={6} />
            </Row>
        </div>
    );
});

export default <Demo />;

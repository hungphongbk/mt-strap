import React from "react";
import DropdownEffect from "../components/DropdownEffect/DropdownEffect";
import styles from "./DropdownEffect.module.scss";
import classnames from "classnames";
import injectSheet from "react-jss";

const list = ["foo foo foo", "bar bar bar", "fuck fuck fuck", "baz baz"];

const classDefines = {
    ul: {
        listStyleType: "none",
        marginBlockStart: 0,
        marginBlockEnd: 0,
        paddingInlineStart: 0
    }
};

const Demo = injectSheet(classDefines)(({ classes }) => {
    const [index, setIndex] = React.useState(0),
        percentage = index / list.length;

    return (
        <DropdownEffect
            index={percentage}
            className={styles.dropdown}
            animationDuration={300}
        >
            {({ itemRef, toggle }) => (
                <ul className={classes.ul}>
                    {list.map((item, i) => (
                        <li
                            className={classnames({
                                [styles.item]: true,
                                [styles.selected]: i === index
                            })}
                            key={item}
                            ref={itemRef}
                            onClick={() => {
                                setIndex(i);
                                toggle();
                            }}
                        >
                            <p className={classes.innerText}>{item}</p>
                        </li>
                    ))}
                </ul>
            )}
        </DropdownEffect>
    );
});

export default <Demo />;

import React from "react";
import FormGroup from "../components/FormGroup/FormGroup";
import Radio from "../components/Radio/Radio";
import Typography from "../components/Typography/Typography";
import Checkbox from "../components/Checkbox/Checkbox";

class Demo extends React.Component {
    state = {
        gender: "male",
        abilities: {}
    };
    onGenderChange = val => this.setState({ gender: val });
    onAbilityChange = val =>
        this.setState(({ abilities }) => {
            const rs = { ...abilities };
            rs[val] = !rs[val];
            console.log(rs);
            return { abilities: rs };
        });
    getAbility = () =>
        " " +
        Object.entries(this.state.abilities)
            .filter(ab => ab[1] === true)
            .map(ab => ab[0])
            .join(", ");
    render() {
        return (
            <form>
                <FormGroup label={"Select your gender"}>
                    <div className={"mt-mt-2"}>
                        {["male", "female", "LGBT"].map(gender => (
                            <Radio
                                key={gender}
                                name={"gender"}
                                value={gender}
                                checked={this.state.gender === gender}
                                inline
                                onChange={this.onGenderChange}
                            >
                                {gender}
                            </Radio>
                        ))}
                    </div>
                </FormGroup>
                <FormGroup label={"Select your ability"}>
                    <div className="mt-mt-2">
                        {["rich", "handsome", "talented"].map(ability => (
                            <Checkbox
                                key={ability}
                                name={"ability"}
                                value={ability}
                                checked={this.state.abilities[ability]}
                                onChange={this.onAbilityChange}
                            >
                                {ability}
                            </Checkbox>
                        ))}
                    </div>
                </FormGroup>
                <Typography color={"green-light-10"}>
                    Your gender is {this.state.gender}, and you are
                    {this.getAbility()}
                </Typography>
            </form>
        );
    }
}

export default <Demo />;

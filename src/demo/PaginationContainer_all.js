import React from "react";
import PaginationContainer from "../components/PaginationContainer/PaginationContainer";

export default (
    <div>
        <div style={{ margin: "2rem" }}>
            <PaginationContainer pageCount={5} initialPage={1}>
                {paginationProps => (
                    <table style={{ width: "auto" }}>
                        <tr>
                            <td>{paginationProps.renderPreviousThumb}</td>
                            <td style={{ width: "200px" }}>ahihi</td>
                            <td>{paginationProps.renderNextThumb}</td>
                        </tr>
                    </table>
                )}
            </PaginationContainer>
        </div>
    </div>
);

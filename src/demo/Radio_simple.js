import React from "react";
import Container from "../components/Container/Container";
import Row from "../components/Row/Row";
import Col from "../components/Col/Col";
import Typography from "../components/Typography/Typography";
import Radio from "../components/Radio/Radio";

const noop = () => {},
    Demo = () => (
        <Container>
            <Row>
                <Col sm={6}>
                    <Typography variant={"h4"}>Default size</Typography>
                    <Radio checked={false} onChange={noop}>
                        Default radio (hover mouse to see UI changes)
                    </Radio>
                    <Radio checked onChange={noop}>
                        Checked radio
                    </Radio>
                    <Radio checked onChange={noop} disabled={true}>
                        Checked radio (disabled)
                    </Radio>
                    <Radio checked={false} onChange={noop} disabled={true}>
                        Unchecked radio (disabled)
                    </Radio>
                </Col>
                <Col sm={6}>
                    <Typography variant={"h4"}>Large size</Typography>
                    <Radio checked={false} onChange={noop} size={"lg"}>
                        Default radio (hover mouse to see UI changes)
                    </Radio>
                    <Radio checked onChange={noop} size={"lg"}>
                        Checked radio
                    </Radio>
                    <Radio checked onChange={noop} disabled={true} size={"lg"}>
                        Checked radio (disabled)
                    </Radio>
                    <Radio
                        checked={false}
                        onChange={noop}
                        disabled={true}
                        size={"lg"}
                    >
                        Unchecked radio (disabled)
                    </Radio>
                </Col>
            </Row>
        </Container>
    );

export default <Demo />;

import React from "react";
import { StaticQuery, graphql } from "gatsby";
import styled from "styled-components";
import styles from "./index/index.module.scss";
import StorybookLogo from "../media/storybook.svg";
import ReactIcon from "../media/React-icon.svg";

const Background = styled.div`
    background-image: linear-gradient(
            0deg,
            ${props => props.color},
            ${props => props.color}
        ),
        url(${props => props.src});
    background-size: cover;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
`;

export default () => (
    <StaticQuery
        query={graphql`
            {
                background: file(relativePath: { eq: "index.jpg" }) {
                    childImageSharp {
                        fluid(maxWidth: 1920, quality: 90) {
                            src
                        }
                    }
                }
                logo: file(relativePath: { eq: "mytour-logo-coupon.png" }) {
                    childImageSharp {
                        fluid(maxWidth: 400, quality: 80) {
                            src
                        }
                    }
                }
            }
        `}
        render={({ background, logo }) => (
            <Background
                className={styles.Logo}
                src={background.childImageSharp.fluid.src}
                color={"rgba(0,66,92,0.7)"}
            >
                <div>
                    <img
                        src={logo.childImageSharp.fluid.src}
                        alt="mytour logo"
                    />
                    <h3>Development Guide</h3>
                </div>
                <div style={{ display: "flex" }}>
                    <a href="/storybook/" className={styles.Link}>
                        <StorybookLogo />
                    </a>
                    <a href="/react/getting-started/" className={styles.Link}>
                        <ReactIcon />
                    </a>
                </div>
            </Background>
        )}
    />
);

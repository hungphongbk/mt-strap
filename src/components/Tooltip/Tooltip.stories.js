import React from "react";
import Tooltip from "./Tooltip";
import { storiesOf } from "@storybook/react";
import Button from "../Button/Button";
import injectSheet from "react-jss";
import HorizontalItem from "../HorizontalItem/HorizontalItem";

const styles = {
        content: {
            width: props => `${props.width}px`
        }
    },
    CustomizableDemo = injectSheet(styles)(({ classes, content, children }) => (
        <div>
            <Tooltip contentClassName={classes.content} content={content}>
                {children}
            </Tooltip>
        </div>
    ));
CustomizableDemo.defaultProps = {
    width: 500
};

storiesOf("Tooltip", module)
    .add("default", () => (
        <div style={{ padding: "8em" }}>
            <Tooltip content={"This is a fucking tooltip top ahihi!"}>
                <Button variant={"white"}>Top</Button>
            </Tooltip>
            &nbsp;
            <Tooltip
                content={"This is a fucking tooltip bottom ahihi!"}
                position={"bottom"}
            >
                <Button variant={"white"}>Bottom</Button>
            </Tooltip>
            &nbsp;
            <Tooltip
                content={"This is a fucking tooltip left ahihi!"}
                position={"left"}
            >
                <Button variant={"white"}>Left</Button>
            </Tooltip>
            &nbsp;
            <Tooltip
                content={"This is a fucking tooltip right ahihi!"}
                position={"right"}
            >
                <Button variant={"white"}>Right</Button>
            </Tooltip>
            <br />
            <br />
            <Tooltip
                content={"This is a fucking tooltip top ahihi!"}
                variant={"dark"}
            >
                <Button variant={"blue"}>Top dark</Button>
            </Tooltip>
            &nbsp;
            <Tooltip
                variant={"dark"}
                content={"This is a fucking tooltip bottom ahihi!"}
                position={"bottom"}
            >
                <Button variant={"blue"}>Bottom dark</Button>
            </Tooltip>
            &nbsp;
            <Tooltip
                variant={"dark"}
                content={"This is a fucking tooltip left ahihi!"}
                position={"left"}
            >
                <Button variant={"blue"}>Left dark</Button>
            </Tooltip>
            &nbsp;
            <Tooltip
                variant={"dark"}
                content={"This is a fucking tooltip right ahihi!"}
                position={"right"}
            >
                <Button variant={"blue"}>Right dark</Button>
            </Tooltip>
        </div>
    ))
    .add("customizable content style", () => (
        <div style={{ paddingTop: "5rem" }}>
            <CustomizableDemo
                content={
                    "Oh hey this is a 500px-width tooltip. You're welcome ;)"
                }
            >
                <Button variant={"white"}>
                    Hover me to see 500px width tooltip :)
                </Button>
            </CustomizableDemo>
        </div>
    ))
    .add("with disabled elements", () => (
        <div style={{ paddingTop: "5rem" }}>
            <Tooltip content={"Disabled"}>
                <Button variant={"blue"} disabled>
                    Disabled
                </Button>
            </Tooltip>
        </div>
    ))
    .add("with JSX content", () => (
        <div style={{ padding: "10rem" }}>
            <CustomizableDemo
                content={
                    <HorizontalItem
                        thumbnailSrc={
                            "https://pmcvariety.files.wordpress.com/2018/03/trump.jpg?w=1000&h=562&crop=1"
                        }
                    >
                        <p
                            style={{
                                marginTop: 0,
                                fontSize: "0.95em",
                                textAlign: "left"
                            }}
                        >
                            Kiểu tóc của Trump đã được nghiên cứu rộng khắp và
                            thường là tiêu điểm cho các bình luận hài hước
                            (Wikipedia) =))
                        </p>
                    </HorizontalItem>
                }
                width={400}
            >
                <Button variant={"blue"} content={""}>
                    Wanna see Donald Trump? Hover here!
                </Button>
            </CustomizableDemo>
        </div>
    ));

import React from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import "../_helpers/_bootstrap.scss";
import "./Tooltip.scss";

class Tooltip extends React.PureComponent {
    state = {
        isMouseEnter: false
    };
    handleMouseEnter = () => {
        !this.state.isMouseEnter &&
            this.setState({
                isMouseEnter: true
            });
    };
    handleMouseOut = () => {
        this.setState({
            isMouseEnter: false
        });
    };
    render() {
        const {
            variant,
            position,
            content,
            children,
            className,
            contentClassName
        } = this.props;
        // console.count(`mouseenter = ${this.state.isMouseEnter}`);
        return (
            <div
                className={classnames(
                    "mt-tooltip",
                    "mt-tooltip-default",
                    this.state.isMouseEnter && "mt-tooltip-hover",
                    variant === "light"
                        ? "mt-tooltip-style"
                        : "mt-tooltip-dark",
                    `mt-tooltip-${position}`,
                    className
                )}
                onMouseEnter={this.handleMouseEnter}
                onMouseLeave={this.handleMouseOut}
            >
                {children}
                <div
                    className={classnames(
                        "mt-tooltip-content",
                        contentClassName
                    )}
                >
                    {content}
                </div>
            </div>
        );
    }
}

Tooltip.propTypes = {
    /**
     * Specific tooltip style (light or dark)
     *
     * @type ('light'|'dark')
     * @default 'light'
     */
    variant: PropTypes.oneOf(["light", "dark"]),
    /**
     * Sets the direction the Tooltip is positioned towards.
     *
     * @type ('top'|'bottom'|'right'|'left')
     * @default 'top'
     */
    position: PropTypes.oneOf(["top", "bottom", "right", "left"]),
    /**
     * Tooltip content
     *
     * @type (string|Element)
     */
    content: PropTypes.oneOfType([PropTypes.string, PropTypes.element])
        .isRequired,
    children: PropTypes.element,
    className: PropTypes.any,
    /**
     * @type string
     */
    contentClassName: PropTypes.any
};
Tooltip.defaultProps = {
    variant: "light",
    position: "top",
    contentClassName: null
};

export default Tooltip;

import React from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import "../_helpers/_bootstrap.scss";
import Image from "../Image/Image";

const CardImg = props => {
    const { children, className, src, alt = "", ratio, ...others } = props,
        classes = classnames("mt-card-img", className);
    if (children !== null)
        return (
            <div className={classes} {...others}>
                <Image src={src} alt={alt} ratio={ratio} />
                <div className="mt-card-img-overlay" />
                {children}
            </div>
        );
    else return <Image className={classes} src={src} alt={alt} ratio={ratio} />;
};
CardImg.propTypes = {
    src: PropTypes.string.isRequired,
    alt: PropTypes.string,
    children: PropTypes.oneOf([
        PropTypes.element,
        PropTypes.arrayOf(PropTypes.element)
    ]),
    className: PropTypes.any,
    ratio: PropTypes.string
};
CardImg.defaultProps = {
    alt: "",
    children: null,
    ratio: null
};
export default CardImg;

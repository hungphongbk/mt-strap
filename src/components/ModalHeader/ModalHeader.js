import React from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import "../_helpers/_bootstrap.scss";

const ModalHeader = props => {
    const { className, children, ...other } = props;

    return (
        <div className={classnames("mt-modal-header", className)} {...other}>
            {children}
        </div>
    );
};
ModalHeader.propTypes = {};
ModalHeader.defaultProps = {};
export default ModalHeader;

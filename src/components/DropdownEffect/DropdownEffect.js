import React from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import "../_helpers/_bootstrap.scss";
import styles from "./DropdownEffect.module.scss";
import withProps from "../_helpers/withProps";
// import TWEEN from "@tweenjs/tween.js";
import fastdom from "../_helpers/fastDom";
import uniqueId from "../_helpers/uniqueId";

const frameRate = 1000 / 60,
    clamp = (value, min, max) => {
        return Math.max(min, Math.min(max, value));
    },
    ease = (v, pow = 4) => {
        const _v = clamp(v, 0, 1);
        return 1 - Math.pow(1 - _v, pow);
    };

@withProps(props => ({
    nFrames: props.animationDuration / frameRate,
    translateY: -props.index * 100
}))
class DropdownEffect extends React.PureComponent {
    /**
     * @type {HTMLElement}
     */
    menu = null;
    /**
     * @type {HTMLElement}
     */
    menuContent = null;
    /**
     * @type {HTMLElement}
     */
    menuTitle = null;

    state = {
        id: uniqueId(5),
        isOpen: true,
        animate: false,
        collapsed: {
            x: 0,
            y: 0
        }
    };

    componentDidMount() {
        (async () => {
            await this.calculateScales();
            this.createEaseAnimation();

            await this.collapse();
            await this.activate();
        })();
    }

    assignElement = name => el => {
        if (typeof el !== "undefined" && el !== null) this[name] = el;
    };

    activate = async () => {
        await fastdom.mutate(() => {
            this.menu.classList.add(styles.MenuActive);
        });
        this.setState({ animate: true });
    };

    expand = async () => {
        if (this.state.isOpen) return;
        this.setState({ isOpen: true });

        if (this.state.animate) {
            this.createEaseAnimation();
            await this.applyAnimation({ expand: true });
        }
        await fastdom.mutate(() => {
            this.menu.style.transform = `scale(1,1) translateY(${
                this.props.translateY
            }%)`;
            this.menuContent.style.transform = `scale(1,1) translateY(0%)`;
        });
    };

    collapse = async () => {
        if (!this.state.isOpen) return;
        this.setState({ isOpen: false });
        // console.log("collapsed " + this.state.isOpen);

        const { x, y } = this.state.collapsed;
        const invX = 1 / x;
        const invY = 1 / y;

        if (this.state.animate) {
            this.createEaseAnimation();
            await this.applyAnimation({ expand: false });
        }

        await fastdom.mutate(() => {
            this.menu.style.transform = `scale(${x}, ${y}) translateY(0%)`;
            this.menuContent.style.transform = `scale(${invX}, ${invY}) translateY(${
                this.props.translateY
            }%)`;
        });
    };
    toggle = () =>
        new Promise(resolve => {
            if (this.state.isOpen) {
                this.collapse().then(resolve);
            } else this.expand().then(resolve);
        });

    createEaseAnimation = () => {
        let menuEase = document.querySelector(styles.MenuEase + this.state.id);
        if (!menuEase) {
            menuEase = document.createElement("style");
            menuEase.classList.add(styles.MenuEase + this.state.id);
        } else return;

        const duration = this.props.animationDuration;
        const menuExpandAnimation = [];
        const menuExpandContentsAnimation = [];
        const menuCollapseAnimation = [];
        const menuCollapseContentsAnimation = [];
        const percentIncrement = 100 / this.props.nFrames;

        for (let i = 0; i <= this.props.nFrames; i++) {
            const step = ease(i / this.props.nFrames).toFixed(5);
            console.log(step);
            const percentage = (i * percentIncrement).toFixed(5);
            const startX = this.state.collapsed.x;
            const startY = this.state.collapsed.y;
            const endX = 1;
            const endY = 1;
            const startTranslateY = 0;
            const endTranslateY = this.props.translateY;

            // expand animation
            this.append({
                percentage,
                step,
                startX,
                startY,
                endX,
                endY,
                startTranslateY,
                endTranslateY,
                outerAnimation: menuExpandAnimation,
                innerAnimation: menuExpandContentsAnimation
            });

            // Collapse animation.
            this.append({
                percentage,
                step,
                startX: 1,
                startY: 1,
                endX: this.state.collapsed.x,
                endY: this.state.collapsed.y,
                startTranslateY: this.props.translateY,
                endTranslateY: 0,
                outerAnimation: menuCollapseAnimation,
                innerAnimation: menuCollapseContentsAnimation
            });
        }
        menuEase.textContent = [
            `@keyframes ${
                styles.menuExpandAnimation
            } {${menuExpandAnimation.join("")}}`,
            `@keyframes ${
                styles.menuExpandContentsAnimation
            } {${menuExpandContentsAnimation.join("")}}`,
            `@keyframes ${
                styles.menuCollapseAnimation
            } {${menuCollapseAnimation.join("")}}`,
            `@keyframes ${
                styles.menuCollapseContentsAnimation
            } {${menuCollapseContentsAnimation.join("")}}`,
            `.${styles.Menu}{animation-duration:${duration}ms}`,
            `.${styles.MenuContents}{animation-duration:${duration}ms}`
        ].join("");
        console.log(menuEase.textContent);
        document.head.appendChild(menuEase);
        return menuEase;
    };
    append = ({
        percentage,
        step,
        startX,
        startY,
        endX,
        endY,
        startTranslateY,
        endTranslateY,
        outerAnimation,
        innerAnimation
    }) => {
        const f = (start, end) => (start + (end - start) * step).toFixed(5);
        const xScale = f(startX, endX);
        const yScale = f(startY, endY);
        const yTranslate = f(startTranslateY, endTranslateY);

        const invScaleX = (1 / xScale).toFixed(5);
        const invScaleY = (1 / yScale).toFixed(5);
        const invTranslateY = f(endTranslateY, startTranslateY);
        console.log(yTranslate, invTranslateY);
        outerAnimation.push(
            `${percentage}% {transform: scale(${xScale}, ${yScale}) translateY(${yTranslate}%);}`
        );
        innerAnimation.push(
            `${percentage}% {transform: scale(${invScaleX}, ${invScaleY}) translateY(${invTranslateY}%);}`
        );
    };
    applyAnimation = async ({ expand }) => {
        await fastdom.mutate(() => {
            this.menu.classList.remove(styles.Expanded);
            this.menu.classList.remove(styles.Collapsed);
            // eslint-disable-next-line no-unused-expressions
            window.getComputedStyle(this.menu).transform;
        });
        if (expand) {
            await fastdom.mutate(() => {
                this.menu.classList.add(styles.Expanded);
            });
        } else {
            await fastdom.mutate(() => {
                this.menu.classList.add(styles.Collapsed);
            });
        }
    };
    calculateScales = async () => {
        let collapsed, expanded;
        await fastdom.measure(() => {
            collapsed = this.menuTitle.getBoundingClientRect();
            expanded = this.menu.getBoundingClientRect();
        });

        this.setState({
            collapsed: {
                x: collapsed.width / expanded.width,
                y: collapsed.height / expanded.height
            }
        });
    };

    render() {
        return (
            <div className={classnames(styles.Container, this.props.className)}>
                <div className={styles.Menu} ref={this.assignElement("menu")}>
                    <div
                        className={styles.MenuContents}
                        ref={this.assignElement("menuContent")}
                    >
                        {this.props.children({
                            itemRef: this.assignElement("menuTitle"),
                            toggle: this.toggle
                        })}
                    </div>
                </div>
            </div>
        );
    }
}

DropdownEffect.propTypes = {
    dropdownHeight: PropTypes.any,
    animationDuration: PropTypes.number,
    index: PropTypes.number.isRequired,
    // computed
    nFrames: PropTypes.number,
    translateY: PropTypes.number,
    // default
    className: PropTypes.any,
    children: PropTypes.func.isRequired
};
DropdownEffect.defaultProps = {
    animationDuration: 300
};

export default DropdownEffect;

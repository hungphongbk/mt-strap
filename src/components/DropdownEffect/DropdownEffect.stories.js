import { storiesOf } from "@storybook/react";
import React from "react";
import demo from "../../demo/DropdownEffect";

storiesOf("Utilities").add("DropdownEffect", () => demo);

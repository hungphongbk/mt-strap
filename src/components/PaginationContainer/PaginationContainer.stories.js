import React from "react";
import PaginationContainer from "./PaginationContainer";
import { storiesOf } from "@storybook/react";
import allDemo from "../../demo/PaginationContainer_all";

storiesOf("PaginationContainer", module).add("all demo", () => allDemo);

import React from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import "../_helpers/_bootstrap.scss";
import styles from "./PaginationContainer.module.scss";
import hoistNonReactStatics from "hoist-non-react-statics";
import { setDisplayName } from "../_helpers/setDisplayName";

export const withPaginationContainer = options => Component => {
    class PaginationContainerBase extends React.Component {
        getOptions = () => {
            if (typeof options === "function") return options(this.props);
            else return options;
        };
        getPreviousThumbClasses = () => {
            return classnames(styles.PaginationThumb, {
                [styles.Rounded]: this.getOptions().previousLabel.length === 0
            });
        };
        getNextThumbClasses = () => {
            return classnames(styles.PaginationThumb, {
                [styles.Rounded]: this.getOptions().nextLabel.length === 0
            });
        };
        renderPreviousThumb = () => (
            <a className={this.getPreviousThumbClasses()} role={"button"}>
                {this.getOptions().arrow && <i className="li li-arrow-left" />}
                {this.getOptions().previousLabel}
            </a>
        );
        renderNextThumb = () => (
            <a className={this.getNextThumbClasses()} role={"button"}>
                {this.getOptions().arrow && <i className="li li-arrow-right" />}
                {this.getOptions().nextLabel}
            </a>
        );
        render() {
            const paginationProps = {
                renderNextThumb: this.renderNextThumb(),
                renderPreviousThumb: this.renderPreviousThumb()
            };
            return (
                <Component {...this.props} paginationProps={paginationProps} />
            );
        }
    }
    hoistNonReactStatics(PaginationContainerBase, Component);
    setDisplayName(
        PaginationContainerBase,
        "withPaginationContainer",
        Component
    );
    return PaginationContainerBase;
};

const PaginationContainer = withPaginationContainer(props => props)(props =>
    props.children(props.paginationProps)
);
export default PaginationContainer;

PaginationContainer.propTypes = {
    /**
     * The total number of pages.
     * @required
     * @type number
     */
    pageCount: PropTypes.number.isRequired,
    /**
     * The method to call when a page is clicked. Exposes the current page object as an argument.
     * @type function
     */
    onPageChange: PropTypes.func,
    /**
     * The initial page selected.
     *
     * @type number
     * @default 0
     */
    initialPage: PropTypes.number,
    /**
     * @type string
     * @default ''
     */
    previousLabel: PropTypes.string,
    /**
     * @type string
     * @default ''
     */
    nextLabel: PropTypes.string,
    /**
     * @type boolean
     * @default true
     */
    arrow: PropTypes.bool,
    /**
     * @type function
     * @required
     */
    children: PropTypes.func.isRequired
};
PaginationContainer.defaultProps = {
    previousLabel: "",
    nextLabel: "",
    arrow: true,
    onPageChange: () => {}
};

import React, { Component } from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import "../_helpers/_bootstrap.scss";
import styles from "./Breadcrumb.module.scss";
import { NavLink } from "react-router-dom";

class Breadcrumb extends React.PureComponent {
    manipulateChildren = () => {
        const newArr = [];
        React.Children.forEach(this.props.children, child => {
            if (typeof child !== "undefined" && child !== null)
                newArr.push(
                    <li>{this.renderBreadCrumbItem(child)}</li>,
                    <li>
                        <span className={styles.Separator}>
                            {this.props.separator}
                        </span>
                    </li>
                );
        });
        newArr.pop();
        return (
            <nav aria-label="Breadcrumb">
                <ol
                    className={classnames(
                        styles.BreadCrumbOl,
                        this.props.className
                    )}
                >
                    {newArr}
                </ol>
            </nav>
        );
    };

    renderBreadCrumbItem = child => {
        if (typeof child === "undefined" || child === null) return null;
        const { className, ...props } = child.props;
        return React.createElement(NavLink, {
            className: classnames(styles.Item, className),
            ...props,
            activeClassName: styles.ItemActive
        });
    };
    render() {
        return this.manipulateChildren();
    }
}
Breadcrumb.propTypes = {
    className: PropTypes.any,
    separator: PropTypes.element
};
Breadcrumb.defaultProps = {
    separator: <i className="li li-chevron-right" />
};
export default Breadcrumb;

import React from "react";
import Breadcrumb from "./Breadcrumb";
import { storiesOf } from "@storybook/react";
import StoryRouter from "storybook-react-router";

import BreadCrumbSimple from "../../demo/Breadcrumb_simple";
import BreadCrumb2 from "../../demo/Breadcrumb_customizableSeparator";

storiesOf("Breadcrumb", module)
    .addDecorator(StoryRouter())
    .add("Simple", () => BreadCrumbSimple)
    .add("customizable separator", () => BreadCrumb2);

import React, { Component } from "react";
import "../_helpers/_bootstrap.scss";
import "../../sass/components/_tables.scss";

class Table extends Component {
    render() {
        return (
            <table className="mt-table mt-table-style mt-table-size">
                {this.props.children}
            </table>
        );
    }
}
Table.propTypes = {};
Table.defaultProps = {};

export default Table;

import React from "react";
import { storiesOf } from "@storybook/react";
import Table from "./Table";
import Table_simple from "../../demo/Table_simple";
import Table_externalComponents from "../../demo/Table_externalComponents";
import Table_sortable from "../../demo/Table_sortable";

storiesOf("Table", module)
    .add("Default", () => Table_simple)
    .add("with simple components", () => Table_externalComponents)
    .add("sortable", () => Table_sortable);

import React from "react";
import { storiesOf } from "@storybook/react";
import DemoSimple from "../../demo/Checkbox_simple";

storiesOf("Input/Checkbox", module).add("Simple", () => DemoSimple);

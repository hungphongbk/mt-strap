import React from "react";
import classnames from "classnames";
import * as PropTypes from "prop-types";

const Container = ({ className, children, component: Component }) => (
    <Component className={classnames("mt-container", className)}>
        {children}
    </Component>
);

Container.propTypes = {
    component: PropTypes.oneOf([PropTypes.string, PropTypes.elementType]),
    className: PropTypes.any
};
Container.defaultProps = {
    component: "div"
};

export default Container;

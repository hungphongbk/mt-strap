import React, { Component } from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import "../_helpers/_bootstrap.scss";
import "../../sass/components/_cards.scss";

const CardBody = props => {
    const { className, children, ...other } = props;
    return (
        <div className={classnames("mt-card-body", className)} {...other}>
            {children}
        </div>
    );
};
CardBody.propTypes = {};
CardBody.defaultProps = {};
export default CardBody;

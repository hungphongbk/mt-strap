import React from "react";
import Card from "./Card";
import { storiesOf } from "@storybook/react";

import simple from "../../demo/Card_simple";
import customize from "../../demo/Card_withCustomizeComponent";
import withCollapse from "../../demo/Card_withCollapse";
import StoryRouter from "storybook-react-router";

storiesOf("Card & Accordion/Card", module)
    // .addDecorator(StoryRouter())
    .add("all simple", () => simple)
    .add("customizable component", () => customize)
    .add("with collapse", () => withCollapse);

import React from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import "../_helpers/_bootstrap.scss";
import "../../sass/components/_cards.scss";

const Card = props => {
    const { component: Component, className, children, ...other } = props;
    return (
        <Component
            className={classnames(
                "mt-card mt-card-style mt-card-default",
                className
            )}
            {...other}
        >
            {children}
        </Component>
    );
};
Card.propTypes = {
    component: PropTypes.elementType
};
Card.defaultProps = {
    component: "div"
};
export default Card;

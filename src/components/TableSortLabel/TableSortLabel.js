import React from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import "../_helpers/_bootstrap.scss";
import styles from "./TableSortLabel.module.scss";

const nextValue = { none: "asc", asc: "desc", desc: "none" };

class TableSortLabel extends React.PureComponent {
    /**
     * @type {HTMLElement}
     */
    th = null;
    onChange = () => this.props.onChange(nextValue[this.props.sortValue]);
    attachEventHandler = el => {
        if (!el) return;
        const th = el.parentElement;
        th.addEventListener("click", this.onChange);
        if (!th.classList.contains(styles.SortLabelTh)) {
            th.classList.add(styles.SortLabelTh);
        }
        this.th = th;
    };
    componentWillUnmount() {
        const th = this.th;
        th.removeEventListener("click", this.onChange);
        if (th.classList.contains(styles.SortLabelTh))
            th.classList.remove(styles.SortLabelTh);
    }

    render() {
        const props = this.props;
        if (!props.sortValue) {
            props.onChange("none");
            return null;
        }
        return (
            <span
                ref={this.attachEventHandler}
                className={classnames(
                    styles.SortLabel,
                    `sort-label-${props.sortValue}`
                )}
            >
                <i className="li li-chevrons-expand-vertical" />
            </span>
        );
    }
}

TableSortLabel.propTypes = {
    sortValue: PropTypes.oneOf(["none", "asc", "desc"]).isRequired,
    onChange: PropTypes.func.isRequired
};
export default TableSortLabel;

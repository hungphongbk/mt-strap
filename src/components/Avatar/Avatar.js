import React from "react";
import "../_helpers/_bootstrap.scss";
import Image from "../Image/Image";
import * as PropTypes from "prop-types";

const Avatar = ({ src, alt, size }) => (
    <Image
        src={src}
        alt={alt}
        ratio={Image.FIXED_RATIO_1_1}
        borderStyle={"circle"}
        className={"mt-avatar"}
        style={{ width: `${size}px` }}
    />
);
Avatar.propTypes = {
    size: PropTypes.number
};
Avatar.defaultProps = {
    size: 30
};

export default Avatar;

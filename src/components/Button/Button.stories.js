import React from "react";
import Button from "./Button";
import { storiesOf } from "@storybook/react";
import { withKnobs, text, boolean, number } from "@storybook/addon-knobs";

const variants = ["blue", "green", "white", "pink", "red", "orange"],
    printButtons = (props = {}) =>
        variants.map(variant => [
            <Button
                key={variant}
                variant={variant}
                {...props}
                disabled={boolean("Disabled?", false)}
            >
                mt-btn-{variant}
            </Button>,
            " "
        ]);

storiesOf("Button/Normal", module)
    .addDecorator(withKnobs)
    .add("Buttons color", () => [
        <div>{printButtons()}</div>,
        <div style={{ marginTop: ".3rem" }}>
            {printButtons({ shape: "outlined" })}
        </div>,
        <div style={{ marginTop: ".3rem" }}>
            {printButtons({ shape: "round" })}
        </div>,
        <div style={{ marginTop: ".3rem" }}>
            {printButtons({ shape: "round-outlined" })}
        </div>
    ])
    .add("Buttons sizing", () => (
        <div>
            {["lg", "md", "sm", "xs"].map(size => [
                <Button key={size} size={size} variant={"blue"}>
                    Button {size}
                </Button>,
                " "
            ])}
        </div>
    ))
    .add("Buttons loading animations", () => (
        <div>
            {[
                ["blue", "lg"],
                ["green", "md"],
                ["red", "sm"],
                ["orange", "xs"]
            ].map(([variant, size]) => [
                <Button
                    key={variant}
                    size={size}
                    variant={variant}
                    isLoading={boolean("Is Loading?", true)}
                >
                    Button {variant}-{size}
                </Button>,
                " "
            ])}
        </div>
    ));

storiesOf("Button/Grouped & dropdown", module).add("Button group", () => (
    <div />
));

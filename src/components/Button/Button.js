import React, { Component } from "react";
import * as PropTypes from "prop-types";
import "../_helpers/_bootstrap.scss";
import "../../sass/components/_buttons.scss";
import classnames from "classnames";
import { ContextModal } from "../_helpers/utils";

const buttonClassname = (props, inModal) => {
    const classes = ["mt-btn", `mt-size-${props.size}`];
    if (/outlined/.test(props.shape) && props.variant !== "white")
        classes.push(`mt-btn-outline-${props.variant}`);
    else classes.push(`mt-btn-${props.variant}`);

    // if is rounded
    /round/.test(props.shape) && classes.push("mt-btn-rounded");

    // if is disabled
    props.disabled && classes.push("disabled");

    inModal && classes.push("mt-modal-btn-style");

    classes.push(props.className || null);
    return classnames(classes);
};
const Button = props => {
    const children = props.isLoading ? (
        <div className="lds-spinner">
            {new Array(12).fill(0).map((_, i) => (
                <div key={i} />
            ))}
        </div>
    ) : (
        props.children
    );

    const { inModal } = React.useContext(ContextModal);

    const eventProps = {};
    for (const prop in props)
        if (props.hasOwnProperty(prop) && /^on/.test(prop))
            eventProps[prop] = props[prop];

    return (
        <button className={buttonClassname(props, inModal)} {...eventProps}>
            {children}
        </button>
    );
};

Button.propTypes = {
    /**
     * Specify button border style
     *
     * @type ('normal'|'outlined'|'round'|'round-outlined')
     * @default 'normal'
     */
    shape: PropTypes.oneOf(["normal", "outlined", "round", "round-outlined"]),
    /**
     * One or more button variant combinations
     * @type ('blue'|'green'|'white'|'pink'|'red'|'orange')
     * @required
     */
    variant: PropTypes.oneOf([
        "blue",
        "green",
        "white",
        "pink",
        "red",
        "orange",
        "text-gray"
    ]).isRequired,
    /**
     * Specifies a large or smaller or smallest button.
     *
     * @type ('lg'|'md'|'sm'|'xs')
     * @default 'md'
     */
    size: PropTypes.oneOf(["lg", "md", "sm"]),
    /**
     * Disables the Button, preventing mouse events, even if the underlying component is an <a> element
     * @type boolean
     * @default false
     */
    disabled: PropTypes.bool,
    /**
     * Toggle the loading spinner indicator
     * @type boolean
     * @default false
     */
    isLoading: PropTypes.bool
};
Button.defaultProps = {
    shape: "normal",
    size: "md",
    disabled: false,
    isLoading: false
};

export default Button;

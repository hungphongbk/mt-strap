import React, { Component } from "react";
import "../_helpers/_bootstrap.scss";
import "../../sass/components/_alert.scss";
import * as PropTypes from "prop-types";
import classnames from "classnames";
// import { connect } from "react-redux";
// import { bindActionCreators } from "redux";
// import * as AlertActions from "../../store/Alert/actions";

const Alert = props => {
    const classes = ["mt-alert", `mt-alert-${props.variant}`];
    props.withIcon && classes.push("mt-alert-with-icon");
    const htmlRendered = (
        <div className={classnames(classes)}>
            {props.withIcon && <i className={`li li-${props.withIcon}`} />}
            {props.children}
            {props.dismissible && (
                <i
                    className="mt-alert-close li-cross"
                    onClick={() => props.onClose()}
                />
            )}
        </div>
    );
    return props.dismissible ? props.show && htmlRendered : htmlRendered;
};
Alert.propTypes = {
    /**
     * The Alert visual variant
     *
     * @type ('success'|'danger'|'warning'|'info')
     * @required
     */
    variant: PropTypes.oneOf(["success", "info", "warning", "danger"])
        .isRequired,
    withIcon: PropTypes.string,
    dismissible: PropTypes.bool,
    show: PropTypes.bool,
    onClose: PropTypes.func
};
Alert.defaultProps = {
    withIcon: null
};

export default Alert;

import React from "react";
import Alert from "./Alert";
import { storiesOf } from "@storybook/react";
import { withKnobs, text, boolean, number } from "@storybook/addon-knobs";
import Button from "../Button/Button";

class DismissibleDemo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dismissible: false,
            show: true
        };
        this.reveal = this.reveal.bind(this);
    }
    reveal() {
        this.setState(state => ({
            show: true,
            dismissible: !state.dismissible
        }));
    }
    render() {
        return (
            <>
                <div className="mb-1">
                    <Button variant={"blue"} onClick={this.reveal}>
                        Dismissible ={" "}
                        {this.state.dismissible ? "true" : "false"}
                    </Button>
                </div>
                <Alert
                    variant="success"
                    dismissible={this.state.dismissible}
                    show={this.state.show}
                    onClose={() => this.setState({ show: false })}
                >
                    Whenever you need to, be sure to use margin utilities to
                    keep things nice and tidy.
                </Alert>
            </>
        );
    }
}

storiesOf("Alert", module)
    .addDecorator(withKnobs)
    .add("Alerts", () => (
        <div>
            {["success", "info", "warning", "danger"].map((variant, index) => (
                <div key={variant}>
                    <Alert variant={variant}>
                        <strong>{variant.toUpperCase()}!</strong> This is{" "}
                        {index < 2 ? "positive" : "negative"} notification.
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit. Repellendus vel quod, commodi, tempore possimus
                        asperiores aliquid vitae iste ratione nobis totam
                        officia, fugit incidunt rerum. Aliquam iste quisquam
                        vitae sunt!
                    </Alert>
                </div>
            ))}
        </div>
    ))
    .add("with icon", () => (
        <div>
            <div>
                <Alert variant={"success"} withIcon={"youtube"}>
                    <strong>Wassup man?!!</strong>&nbsp;Nghe nhạc hông?
                </Alert>
            </div>
            <div>
                <Alert variant={"info"} withIcon={"glass-cocktail"}>
                    <strong>Hey?!!</strong>&nbsp;Chán quá thì làm gì?
                </Alert>
            </div>
        </div>
    ))
    .add("with dismissable", () => <DismissibleDemo />);

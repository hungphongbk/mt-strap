import React from "react";
import Image from "./Image";
import { storiesOf } from "@storybook/react";
import styled from "styled-components";

const Wrapper = styled.div`
        display: inline-block;
        width: 30%;
        margin-right: 0.3rem;
    `,
    Inner = styled.div`
        width: 100%;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        > h1 {
            color: white;
            text-shadow: 0 0 6px #0000007a;
            font-size: 2.25rem;
        }
    `;

storiesOf("Image", module).add("with fixed ratio", () => (
    <div>
        {[
            ["https://i.imgur.com/9ncYySC.jpg", "4-3"],
            ["https://i.imgur.com/C87vMI3.jpg", "16-9"]
        ].map(([url, ratio], index) => (
            <Wrapper key={index}>
                <Image
                    src={url}
                    alt={"storybook demo"}
                    ratio={ratio}
                    borderStyle={"rounded"}
                >
                    <Inner>
                        <h1>{ratio}</h1>
                    </Inner>
                </Image>
            </Wrapper>
        ))}
    </div>
));

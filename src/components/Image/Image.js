import React from "react";
import * as PropTypes from "prop-types";
import "../_helpers/_bootstrap.scss";
import styles from "./Image.module.scss";
import FixedRatio from "../FixedRatio/FixedRatio";
import classnames from "classnames";
// import "intersection-observer";
import ResizeObserver from "react-resize-observer";

const symbolShow = Symbol("show"),
    observer =
        typeof IntersectionObserver !== "undefined" &&
        new IntersectionObserver(function(entries) {
            entries.forEach(entry => {
                if (entry.intersectionRatio) {
                    const $img = entry.target.children[0];
                    $img.hasOwnProperty(symbolShow) && $img[symbolShow]();
                }
            });
        }),
    // TODO: #info SSR support
    observe = observer ? observer.observe.bind(observer) : () => {};

const Image = ({
    src,
    alt,
    children,
    borderStyle,
    ratio,
    className,
    style
}) => {
    if (ratio && typeof ratio === "string") {
        /**
         *
         * @type {React.MutableRefObject<HTMLImageElement>}
         */
        // eslint-disable-next-line react-hooks/rules-of-hooks
        const $img = React.useRef(null);
        let ratioDiff = 0;
        const [width, height] = ratio.split("-"),
            setHeight = () => {
                const img = $img.current;
                if (!img) return;
                img.style.height = img.parentElement.clientHeight + "px";
                img.style.width = "auto";
                // console.log("set height", img?.parentElement.clientHeight);
            },
            setWidth = () => {
                const img = $img.current;
                if (!img) return;
                img.style.height = "auto";
                img.style.width = img.parentElement.clientWidth + "px";
                // console.log("set width");
            },
            onLoad = () => {
                const { naturalWidth, naturalHeight } = $img.current;
                ratioDiff =
                    (naturalWidth * 1.0) /
                    naturalHeight /
                    ((width * 1.0) / height);
                Object.defineProperty($img.current, symbolShow, {
                    get: () => () => {
                        // console.log("what?");
                        if (ratioDiff > 1.0) setHeight();
                        else setWidth();
                    }
                });
                $img.current[symbolShow]();
            },
            onResize = () => {
                if (Math.abs(ratio) < 0.01) return;
                $img.current.hasOwnProperty(symbolShow) &&
                    $img.current[symbolShow]();
            };
        return (
            <FixedRatio
                className={classnames(
                    styles.Image,
                    {
                        rect: null,
                        rounded: styles.Rounded,
                        circle: styles.Circle
                    }[borderStyle],
                    className
                )}
                height={height}
                width={width}
                style={style}
                ref={el => {
                    el && observe(el);
                }}
            >
                <img ref={$img} src={src} alt={alt} onLoad={onLoad} />
                {children && <div className={styles.Children}>{children}</div>}
                <ResizeObserver onResize={onResize} />
            </FixedRatio>
        );
    } else {
    }
};

Image.FIXED_RATIO_5_3 = "5-3";
Image.FIXED_RATIO_4_3 = "4-3";
Image.FIXED_RATIO_1_1 = "1-1";

Image.propTypes = {
    /**
     * Image src
     *
     * @type string
     * @required
     */
    src: PropTypes.string.isRequired,
    /**
     * Image alt
     *
     * @type string
     * @default ''
     */
    alt: PropTypes.string,
    /**
     * Set image border shape as rectangular, rounded or circle
     *
     * @type ('rect'|'rounded'|'circle')
     * @default 'rect'
     */
    borderStyle: PropTypes.oneOf(["rect", "rounded", "circle"]),
    /**
     * Enable this option to maintain aspect ratio with new image sizes
     *
     * @type (string|null)
     * @default null
     */
    ratio: PropTypes.string,
    className: PropTypes.string,
    style: PropTypes.object
};
Image.defaultProps = {
    borderStyle: "rect",
    ratio: null,
    children: null,
    className: "",
    style: {}
};

export default Image;

import React from "react";
import FixedRatio from "./FixedRatio";
import { storiesOf } from "@storybook/react";
import styled from "styled-components";

const Wrapper = styled.div`
        display: inline-block;
        width: 22%;
        margin-right: 0.3rem;
    `,
    Inner = styled.div`
        width: 100%;
        height: 100%;
        display: flex;
        justify-content: center;
        background-color: #e7e9eb;
        align-items: center;
    `;

storiesOf("FixedRatio", module).add("default", () => (
    <div>
        {[[4, 3], [5, 3], [16, 9], [1, 1]].map(([width, height]) => (
            <Wrapper key={`${width}-${height}`}>
                <FixedRatio height={height} width={width}>
                    <Inner>
                        {width}:{height}
                    </Inner>
                </FixedRatio>
            </Wrapper>
        ))}
    </div>
));

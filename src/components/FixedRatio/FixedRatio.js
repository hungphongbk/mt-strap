import React, { Component } from "react";
import classnames from "classnames";
import * as PropTypes from "prop-types";
import "../_helpers/_bootstrap.scss";
// import './FixedRatio.scss'

const FixedRatio = React.forwardRef(
    ({ width, height, className = "", style = {}, children, ...rest }, ref) => (
        <div
            className={classnames("FixedRatio__root", className)}
            style={style}
        >
            <div className="FixedRatio__content" ref={ref}>
                {children}
            </div>
            <style jsx>{`
                .FixedRatio__root {
                    position: relative;
                }
                .FixedRatio__root:before {
                    content: "";
                    display: block;
                    padding-top: ${(height * 100) / width}%;
                }
                .FixedRatio__content {
                    position: absolute;
                    top: 0;
                    left: 0;
                    bottom: 0;
                    right: 0;
                }
            `}</style>
        </div>
    )
);
FixedRatio.propTypes = {
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    className: PropTypes.any,
    style: PropTypes.object
};

export default FixedRatio;

import React from "react";
import Switch from "./Switch";
import { storiesOf } from "@storybook/react";

import simpleDemo from "../../demo/Switch_simple";

const noop = () => {};

storiesOf("Input/Switch", module)
    .add("default", () => (
        <div>
            <Switch value={true} className="mr-2" onChange={noop}>
                Checked
            </Switch>
            <Switch value={false} className="mr-2" onChange={noop}>
                Unchecked
            </Switch>
            <Switch value={true} className="mr-2" disabled onChange={noop}>
                Checked & disabled
            </Switch>
            <Switch value={false} className="mr-2" disabled onChange={noop}>
                Disabled only
            </Switch>
        </div>
    ))
    .add("simple", () => simpleDemo);

import React from "react";
import "../_helpers/_bootstrap.scss";
import "./Switch.scss";
import * as PropTypes from "prop-types";
import * as classnames from "classnames";

const Switch = ({
    value,
    disabled,
    children,
    className,
    onChange,
    ...other
}) => (
    <label className={classnames("mt-switch", className)} {...other}>
        <input type="checkbox" checked={value} disabled={disabled} />
        <span className="mt-switch-slider" onClick={() => onChange(!value)}>
            <span className="mt-switch-slider-thumb" />
        </span>
        <span className="mt-switch-title">{children}</span>
    </label>
);

Switch.propTypes = {
    /**
     * If true, the component is checked.
     *
     * @type bool
     * @required
     */
    value: PropTypes.bool.isRequired,
    onChange: PropTypes.func.isRequired,
    /**
     * If true, the switch will be disabled.
     *
     * @type bool
     * @default false
     */
    disabled: PropTypes.bool,
    children: PropTypes.any,
    className: PropTypes.any
};
Switch.defaultProps = {
    disabled: false
};

export default Switch;

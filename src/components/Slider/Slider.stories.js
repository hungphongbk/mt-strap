import React from "react";
import Slider from "./Slider";
import { storiesOf } from "@storybook/react";

storiesOf("Slider", module).add("foo", () => (
    <Slider>
        <div>
            <img
                src={
                    "https://content.screencast.com/users/tutnm/folders/Jing/media/7bd28c90-e44d-4651-b0ec-71a660dcd31c/2019-03-28_1739.png"
                }
            />
            <a href="#">ahihi</a>
        </div>
        <div>
            <img
                src={
                    "https://content.screencast.com/users/tutnm/folders/Jing/media/8f4009e1-ad3a-4453-bbc2-af7d761a0e89/2019-03-28_1741.png"
                }
            />
            <a href="#">ahehe</a>
        </div>
    </Slider>
));

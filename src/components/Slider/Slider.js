import React, { Component } from "react";
import classnames from "classnames";
import * as PropTypes from "prop-types";
import styles from "./Slider.module.scss";
import "../_helpers/_bootstrap.scss";
import domTraverse from "../_helpers/domTraverse";
import imagesLoaded from "imagesloaded";

export default class Slider extends Component {
    static propTypes = {
        children: PropTypes.element.isRequired,
        slideDuration: PropTypes.number,
        onInitialized: PropTypes.func
    };
    static defaultProps = {
        slideDuration: 0.5,
        onInitialized: () => {}
    };
    state = {
        idx: 0,
        sliding: false,
        initialized: false,
        style: {}
    };
    firstSlide = null;

    constructor(props) {
        super(props);
    }
    componentWillMount() {
        this.getImageUrl(0);
    }

    initialize = () =>
        imagesLoaded(this.firstSlide, () =>
            this.setState({
                initialized: true,
                style: {
                    width: this.firstSlide.clientWidth + "px",
                    height: this.firstSlide.clientHeight + "px"
                }
            })
        );
    getImageUrl = idx =>
        new Promise(resolve => {
            React.Children.forEach(
                this.props.children,
                (child, i) =>
                    i === idx &&
                    domTraverse(
                        child,
                        node => node.type === "img" && console.log(node)
                    )
            );
        });
    getSlideWrapperTransition = () => {
        const styles = w => ({
            transition: `transform ${this.props.slideDuration}s ease-in-out`,
            transform: `translateX(-${w}px)`
        });

        if (!this.state.initialized) return styles(0);
        const w = 1 * this.state.style.width.replace(/px/, "");
        return styles(w * this.state.idx);
    };

    render() {
        return (
            <div
                className={classnames(
                    styles.Container,
                    this.state.initialized && styles.Initialized
                )}
                style={this.state.style}
            >
                <div
                    className={styles.Wrapper}
                    style={this.getSlideWrapperTransition()}
                >
                    {React.Children.map(this.props.children, (child, i) =>
                        React.cloneElement(child, {
                            key: i,
                            ref: el => {
                                if (el && !this.firstSlide) {
                                    this.firstSlide = el;
                                    this.initialize();
                                }
                            },
                            style: this.state.style
                        })
                    )}
                </div>
                <div className={styles.NextButton} />
                <div className={styles.PrevButton} />
            </div>
        );
    }
}

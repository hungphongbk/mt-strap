import React from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import "../_helpers/_bootstrap.scss";
import "../../sass/components/_checkboxes-radios.scss";

const checkboxClassnames = props => {
    const classes = ["mt-checkbox-radio", `mt-checkbox-radio-${props.size}`];

    props.disabled && classes.push("disabled");
    props.inline && classes.push("mt-checkbox-radio-inline");

    classes.push(props.className || null);
    return classnames(classes);
};
const CheckboxRadioBase = props => {
    const { type, size, className, children, onChange, ...other } = props;
    return (
        <label className={checkboxClassnames(props)} disabled={props.disabled}>
            <input
                type={type}
                {...other}
                onChange={e => onChange(e.target.value)}
            />
            <span className={`li-checkmark li-checkmark-${type}`} />
            {children}
        </label>
    );
};
CheckboxRadioBase.propTypes = {
    type: PropTypes.oneOf(["checkbox", "radio"]).isRequired,
    /**
     * Specifies a large or default checkbox.
     *
     * @type ('default'|'lg')
     * @default 'default'
     */
    size: PropTypes.oneOf(["default", "lg"]),
    /**
     * Disables the checkbox, preventing mouse events, even if the underlying component is an <a> element
     * @type boolean
     * @default false
     */
    disabled: PropTypes.bool,
    checked: PropTypes.bool.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    name: PropTypes.string,
    inline: PropTypes.bool
};
CheckboxRadioBase.defaultProps = {
    size: "default",
    disabled: false,
    inline: false
};
export default CheckboxRadioBase;

/* eslint no-unused-expressions: 0 */
import React from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import "../_helpers/_bootstrap.scss";
import styles from "./Collapse.module.scss";
import withForwardRef from "../_helpers/withForwardRef";
import {
    duration,
    getAutoHeightDuration,
    getTransitionProps
} from "../_helpers/styles/transitions";
import { Transition } from "react-transition-group";

let Collapse = function(props) {
    const {
        children,
        className,
        collapsedHeight = "0px",
        component: Component = "div",
        isOpen: inProp,
        onEnter,
        onEntered,
        onEntering,
        onExit,
        onExiting,
        style,
        theme,
        timeout = duration.standard,
        ref,
        ...other
    } = props;

    const timer = React.useRef(null),
        /**
         *
         * @type {React.MutableRefObject<HTMLElement>}
         */
        wrapperRef = React.useRef(null),
        autoTransitionDuration = React.useRef();

    React.useEffect(() => {
        return () => {
            clearTimeout(timer.current);
        };
    }, []);

    const handleEnter = node => {
        node.style.height = collapsedHeight;

        onEnter?.(node);
    };

    const handleEntering = node => {
        const wrapperHeight = wrapperRef.current?.clientHeight || 0;

        const { duration: transitionDuration } = getTransitionProps(
            { style, timeout },
            {
                mode: "enter"
            }
        );

        if (timeout === "auto") {
            const duration2 = getAutoHeightDuration(wrapperHeight);
            node.style.transitionDuration = `${duration2}ms`;
            autoTransitionDuration.current = duration2;
        } else {
            node.style.transitionDuration =
                typeof transitionDuration === "string"
                    ? transitionDuration
                    : `${transitionDuration}ms`;
        }

        node.style.height = `${wrapperHeight}px`;

        onEntering?.(node);
    };

    const handleEntered = node => {
        node.style.height = "auto";

        onEntered?.(node);
    };

    const handleExit = node => {
        const wrapperHeight = wrapperRef.current?.clientHeight || 0;
        node.style.height = `${wrapperHeight}px`;

        onExit?.(node);
    };

    const handleExiting = node => {
        const wrapperHeight = wrapperRef.current?.clientHeight || 0;

        const { duration: transitionDuration } = getTransitionProps(
            { style, timeout },
            {
                mode: "exit"
            }
        );

        if (timeout === "auto") {
            const duration2 = getAutoHeightDuration(wrapperHeight);
            node.style.transitionDuration = `${duration2}ms`;
            autoTransitionDuration.current = duration2;
        } else {
            node.style.transitionDuration =
                typeof transitionDuration === "string"
                    ? transitionDuration
                    : `${transitionDuration}ms`;
        }

        node.style.height = collapsedHeight;

        console.log(collapsedHeight);

        onExiting?.(node);
    };

    const addEndListener = (_, next) => {
        if (timeout === "auto") {
            timer.current = setTimeout(
                next,
                autoTransitionDuration.current || 0
            );
        }
    };

    return (
        <Transition
            in={inProp}
            onEnter={handleEnter}
            onEntered={handleEntered}
            onEntering={handleEntering}
            onExit={handleExit}
            onExiting={handleExiting}
            addEndListener={addEndListener}
            timeout={timeout === "auto" ? null : timeout}
            {...other}
        >
            {(state, childProps) => (
                <Component
                    className={classnames(
                        styles.Container,
                        {
                            [styles.Entered]: state === "entered",
                            [styles.Hidden]:
                                state === "exited" &&
                                !inProp &&
                                collapsedHeight === "0px"
                        },
                        className
                    )}
                    style={{
                        minHeight: collapsedHeight,
                        ...style
                    }}
                    ref={ref}
                >
                    <div className={styles.Wrapper} ref={wrapperRef}>
                        <div className={styles.WrapperInner}>{children}</div>
                    </div>
                </Component>
            )}
        </Transition>
    );
};
Collapse = withForwardRef(Collapse);
Collapse.propTypes = {
    /**
     * The height of the container when collapsed.
     */
    collapsedHeight: PropTypes.string,
    /**
     * If `true`, the component will transition in.
     */
    isOpen: PropTypes.bool,
    /**
     * @ignore
     */
    onEnter: PropTypes.func,
    /**
     * @ignore
     */
    onEntered: PropTypes.func,
    /**
     * @ignore
     */
    onEntering: PropTypes.func,
    /**
     * @ignore
     */
    onExit: PropTypes.func,
    /**
     * @ignore
     */
    onExiting: PropTypes.func,
    /**
     * The duration for the transition, in milliseconds.
     * You may specify a single timeout for all transitions, or individually with an object.
     *
     * Set to 'auto' to automatically calculate transition time based on height.
     */
    timeout: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.shape({ enter: PropTypes.number, exit: PropTypes.number }),
        PropTypes.oneOf(["auto"])
    ]),
    /**
     * @ignore
     */
    innerRef: PropTypes.any
};
Collapse.defaultProps = {};
export default Collapse;

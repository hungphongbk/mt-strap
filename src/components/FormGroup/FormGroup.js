import React, { Component } from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import "../_helpers/_bootstrap.scss";
import "../../sass/components/_inputs.scss";
import uniqueId from "../_helpers/uniqueId";
import { addClassName } from "../_helpers/utils";

const FormGroup = props => {
    const { children, error, success, className, label, ...other } = props;

    const classes = ["mt-form-group"],
        renderAfters = [],
        addAfter = after => {
            if (typeof after === "string")
                renderAfters.push(
                    <span className={"mt-input-validated-text mt-text-desc"}>
                        {after}
                    </span>
                );
        },
        labelProps = {},
        { current: componentId } = React.useRef(uniqueId(5, "form-group-"));

    let newChildren = children;

    /**
     * Add some classes when TextInput has label
     */
    if (label) {
        classes.push("mt-form-group-label");
        Object.assign(labelProps, {
            for: componentId
        });
        newChildren = React.Children.map(newChildren, (child, index) => {
            if (index > 0) return child;
            return React.cloneElement(child, {
                id: componentId
            });
        });
    }

    if (error) {
        classes.push("mt-validated-error");
        addAfter(error);
    }
    if (success) {
        classes.push("mt-validated-success");
        addAfter(success);
    }
    return (
        <div className={classnames(classes, className)} {...other}>
            {label && (
                <label className={"mt-label-default"} {...labelProps}>
                    {label}
                </label>
            )}
            {newChildren}
            {renderAfters}
        </div>
    );
};
FormGroup.propTypes = {
    className: PropTypes.any,
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    error: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    success: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
};
FormGroup.defaultProps = {
    error: false,
    success: false
};
export default FormGroup;

import React from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import "../_helpers/_bootstrap.scss";
import styles from "./HorizontalItem.module.scss";
import Image from "../Image/Image";
import { wordLefts } from "../_helpers/stringManipulate";

class HorizontalItem extends React.PureComponent {
    state = {
        contentEl: null
    };
    bindContentEl = contentEl =>
        contentEl &&
        this.setState({ contentEl: wordLefts(contentEl.innerText) });
    getThumbnailAlt = () => {
        const thumbnailAlt = this.props.thumbnailAlt,
            contentEl = this.state.contentEl;
        if (thumbnailAlt && thumbnailAlt.length > 0) return thumbnailAlt;
        if (contentEl) return contentEl;
        return "";
    };
    render() {
        const props = this.props;
        return (
            <div className={classnames(styles.HorizontalItem, props.className)}>
                <div
                    className={styles.Thumbnail}
                    style={{ width: props.thumbnailWidth }}
                >
                    <Image
                        src={props.thumbnailSrc}
                        ratio={props.thumbnailRatio}
                        alt={this.getThumbnailAlt()}
                    />
                </div>
                <div className={styles.Content} ref={this.bindContentEl}>
                    {props.children}
                </div>
            </div>
        );
    }
}
HorizontalItem.propTypes = {
    thumbnailSrc: PropTypes.string.isRequired,
    thumbnailAlt: PropTypes.string,
    thumbnailRatio: PropTypes.string,
    thumbnailWidth: PropTypes.string,
    className: PropTypes.any,
    children: PropTypes.any
};
HorizontalItem.defaultProps = {
    thumbnailRatio: "1-1",
    thumbnailWidth: "25%"
};
export default HorizontalItem;

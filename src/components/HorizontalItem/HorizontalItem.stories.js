import React from "react";
import HorizontalItem from "./HorizontalItem";
import { storiesOf } from "@storybook/react";
import styled from "styled-components";

const Wrapper = styled.div`
    width: 75%;
`;

storiesOf("HorizontalItem", module).add("default", () => (
    <Wrapper>
        <HorizontalItem
            thumbnailSrc={
                "https://pmcvariety.files.wordpress.com/2018/03/trump.jpg?w=1000&h=562&crop=1"
            }
        >
            <h3>Donald J.Trump</h3>
            <p>
                Kiểu tóc của Trump đã được nghiên cứu rộng khắp và thường là
                tiêu điểm cho các bình luận hài hước (Wikipedia)
            </p>
        </HorizontalItem>
    </Wrapper>
));

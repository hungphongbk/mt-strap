import React from "react";
import { storiesOf } from "@storybook/react";
import SpinnerDemo from "../../demo/Spinner";

storiesOf("Input/Spinner", module).add("default", () => SpinnerDemo);

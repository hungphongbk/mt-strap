import React, { Component } from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import "../_helpers/_bootstrap.scss";
import styles from "./Spinner.module.scss";
// import Holdable from "react-touch/lib/Holdable.react";
// import defineHold from "react-touch/lib/defineHold";

const HOLD_THRESHOLD = 700,
    HOLD_INTERVAL = 300,
    contextualClass = bool =>
        bool ? "mt-text-blue" : "mt-text-blueberry-light-20",
    toBool = num => num !== 0;

class Spinner extends React.PureComponent {
    state = {
        holdDown: 0,
        holdUp: 0
    };
    handlers = {
        holdDown: 0,
        holdUp: 0
    };
    timeOuts = {
        holdUp: 0,
        holdDown: 0
    };
    getUpDownAvailable = () => {
        /**
         * if `min` props is null, the component becomes has no lower boundary
         * and vice-versa to `max` props
         */
        const { min, max, value } = this.props,
            canDown = min === null || value > min,
            canUp = max === null || value < max;
        return { canUp, canDown };
    };

    componentDidMount() {}
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (toBool(this.state.holdDown) !== toBool(prevState.holdDown))
            this.handleHoldWithKey("holdDown", -1);
        if (toBool(this.state.holdUp) !== toBool(prevState.holdUp))
            this.handleHoldWithKey("holdUp", 1);
    }

    handleHold = (key, callback) => {
        if (this.state[key]) {
            this.timeOuts[key] = setTimeout(() => {
                this.handlers[key] = setInterval(callback, HOLD_INTERVAL);
            }, HOLD_THRESHOLD);
        } else {
            clearTimeout(this.timeOuts[key]);
            clearInterval(this.handlers[key]);
        }
    };

    handleHoldWithKey = (key, multiplier) =>
        this.handleHold(key, () =>
            this.enter(key)(() => {
                const value = Math.floor(
                    this.props.value +
                        (1 + Math.pow((this.state[key] * 1.0) / 5, 2)) *
                            multiplier
                );
                this.props.onChange(value);
            })
        );

    enter = key => (callback = () => {}) =>
        this.setState(
            state => ({
                [key]: state[key] + 1
            }),
            callback
        );
    leave = key => (callback = () => {}) =>
        this.setState({ [key]: 0 }, callback);

    holdEventHandlers = key => {
        return {
            onMouseDown: () => this.enter(key)(),
            onMouseUp: () => this.leave(key)(),
            onMouseLeave: () => this.leave(key)()
        };
    };

    render() {
        const { props } = this,
            { canUp, canDown } = this.getUpDownAvailable();
        return (
            <div className={classnames(styles.Spinner, props.className)}>
                <div>
                    <span {...this.holdEventHandlers("holdDown")}>
                        <i
                            onMouseDown={() => props.onChange(props.value - 1)}
                            className={classnames(
                                "li li-circle-minus mt-pr-2",
                                contextualClass(canDown)
                            )}
                        />
                    </span>
                    <input
                        className={styles.InputArea}
                        type="text"
                        value={props.value}
                    />
                    <span {...this.holdEventHandlers("holdUp")}>
                        <i
                            onMouseDown={() => props.onChange(props.value + 1)}
                            className={classnames(
                                "li li-plus-circle mt-pl-2",
                                contextualClass(canUp)
                            )}
                        />
                    </span>
                </div>
            </div>
        );
    }
}
Spinner.propTypes = {
    /**
     * The value of the spinner
     *
     * @type number
     * @required
     */
    value: PropTypes.number.isRequired,
    /**
     * Callback function that is fired when the spinner's value changed.
     *
     * @type function
     * @required
     */
    onChange: PropTypes.func.isRequired,
    /**
     * The minimum allowed value of the spinner. Should not be equal to max.
     *
     * @type number
     * @default null
     */
    min: PropTypes.number,
    /**
     * The maximum allowed value of the spinner. Should not be equal to min.
     * @type number
     * @default null
     */
    max: PropTypes.number,
    className: PropTypes.any
};
Spinner.defaultProps = {
    min: null,
    max: null
};
export default Spinner;

import React, { Component } from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import "../_helpers/_bootstrap.scss";
import styles from "./Accordion.module.scss";

const Accordion = props => {
    return <div className={"mt-accordion"}>{props.children}</div>;
};
Accordion.propTypes = {};
Accordion.defaultProps = {};
export default Accordion;

import React, { Component } from "react";
// import './Tab.scss'
import "../_helpers/_bootstrap.scss";
import * as PropTypes from "prop-types";
import classnames from "classnames";

const Tab = ({ isActive, children, ...props }) => {
    return (
        <div
            className={classnames("mt-tab-pane", isActive && "active")}
            {...props}
        >
            {children}
        </div>
    );
};
Tab.propTypes = {
    /**
     * Let Tabs component identifies particular tabs
     *
     * @type string
     * @required
     */
    tabKey: PropTypes.string.isRequired,
    /**
     *
     *
     * @type string
     * @required
     */
    title: PropTypes.oneOfType([PropTypes.string]).isRequired,
    isActive: PropTypes.bool
};

export default Tab;

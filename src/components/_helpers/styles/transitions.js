export const duration = {
    shortest: 150,
    shorter: 200,
    short: 250,
    // most basic recommended timing
    standard: 300,
    // this is to be used in complex animations
    complex: 375,
    // recommended when something is entering screen
    enteringScreen: 225,
    // recommended when something is leaving screen
    leavingScreen: 195
};

export function getAutoHeightDuration(height) {
    if (!height) {
        return 0;
    }

    const constant = height / 36;

    // https://www.wolframalpha.com/input/?i=(4+%2B+15+*+(x+%2F+36+)+**+0.25+%2B+(x+%2F+36)+%2F+5)+*+10
    return Math.round((4 + 15 * constant ** 0.25 + constant / 5) * 10);
}

export function getTransitionProps(props, options) {
    const { timeout, style = {} } = props;

    return {
        duration:
            style.transitionDuration || typeof timeout === "number"
                ? timeout
                : timeout[options.mode],
        delay: style.transitionDelay
    };
}

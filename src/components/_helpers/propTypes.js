import * as PropTypes from "prop-types";

export const variantPropType = PropTypes.oneOf([
    "blue",
    "green",
    "white",
    "pink",
    "red",
    "orange"
]);

export const sizingPropType = PropTypes.oneOf(["lg", "md", "sm"]);

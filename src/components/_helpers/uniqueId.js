const uniqueId = (length = 10, prefix = "") =>
    prefix +
    [...Array(length).fill(" ")]
        .map(i => (~~(Math.random() * 36)).toString(36))
        .join("");

export default uniqueId;

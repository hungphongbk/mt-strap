export const capture = callback => e => {
    e.stopPropagation();
    callback();
};

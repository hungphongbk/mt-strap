import _fastdom from "fastdom";
import fastdomPromised from "fastdom/extensions/fastdom-promised";
// if (process.env.NODE_ENV !== "production") require("fastdom/fastdom-strict");

const fastdom = _fastdom.extend(fastdomPromised);

export default fastdom;

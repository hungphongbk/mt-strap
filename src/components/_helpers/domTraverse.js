import React from "react";

function domTraverse(node, visitor) {
    return _traverse(node, visitor, { level: 0, parent: null });
}

function _traverse(node, visitor, state) {
    visitor(node, state);

    if (!node.props) return;
    const children = React.Children.toArray(node.props.children);

    children.forEach(child =>
        _traverse(child, visitor, {
            level: state.level + 1,
            parent: node
        })
    );
}

export default domTraverse;

import React from "react";
import classnames from "classnames";

export const addClassName = (obj, ...classes) => {
    obj.className = classnames(obj.className, ...classes);
};

export const ContextModal = React.createContext({
    inModal: true
});

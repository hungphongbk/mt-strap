import React, { useState, useEffect } from "react";
import * as PropTypes from "prop-types";

const LongPress = props => {
    const [startLongPress, setStartLongPress] = useState(false);

    useEffect(() => {
        if (startLongPress) {
            props.progressCb();
        } else {
            props.completedCb();
        } // this is here because minimum edit on SO is 6 characters :)
    }, [startLongPress]);

    return React.cloneElement(React.Children.only(props.children), {
        onMouseDown: () => setStartLongPress(true),
        onMouseUp: () => setStartLongPress(false),
        onMouseLeave: () => setStartLongPress(false),
        onTouchStart: () => setStartLongPress(true),
        onTouchEnd: () => setStartLongPress(false)
    });
};
LongPress.propTypes = {
    progressCb: PropTypes.func,
    completedCb: PropTypes.func
};
LongPress.defaultProps = {
    progressCb: () => {},
    completedCb: () => {}
};
export default LongPress;

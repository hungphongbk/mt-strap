import React from "react";
import hoistNonReactStatics from "hoist-non-react-statics/src";

const withForwardRef = Component => {
    const WithForwardRef = React.forwardRef((props, ref) => (
        <Component {...props} innerRef={ref} />
    ));
    hoistNonReactStatics(WithForwardRef, Component);
    return WithForwardRef;
};
export default withForwardRef;

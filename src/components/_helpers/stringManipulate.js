/**
 * Extract first N words from sentences (default 10 words)
 *
 * @author hungphongbk
 * @param {string} str
 * @param {number} count
 * @returns {string}
 */
export const wordLefts = (str, count = 10) => {
    let newStr = str
        .split(" ")
        .slice(0, count)
        .join(" ");
    if (newStr.length < str.length) newStr += "...";
    return newStr;
};

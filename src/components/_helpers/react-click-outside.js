import { setDisplayName } from "./setDisplayName";

const hoistNonReactStatic = require("hoist-non-react-statics");
const React = require("react");
const ReactDOM = require("react-dom");

function withClickOutside(Component) {
    class EnhancedComponent extends React.Component {
        constructor(props) {
            super(props);
            this.handleClickOutside = this.handleClickOutside.bind(this);
        }

        componentDidMount() {
            document.addEventListener("click", this.handleClickOutside, true);
        }

        componentWillUnmount() {
            document.removeEventListener(
                "click",
                this.handleClickOutside,
                true
            );
        }

        handleClickOutside(e) {
            const domNode = this.__domNode;
            const wrappedInstance = this.__wrappedInstance;
            if (
                (!domNode || !domNode.contains(e.target)) &&
                wrappedInstance &&
                typeof wrappedInstance.handleClickOutside === "function"
            ) {
                wrappedInstance.handleClickOutside(e);
            }
        }

        render() {
            const { wrappedRef, ...rest } = this.props;

            return (
                <Component
                    {...rest}
                    ref={c => {
                        this.__wrappedInstance = c;
                        // eslint-disable-next-line react/no-find-dom-node
                        this.__domNode = ReactDOM.findDOMNode(c);
                        wrappedRef && wrappedRef(c);
                    }}
                />
            );
        }
    }
    setDisplayName(EnhancedComponent, "withClickOutside", Component);

    return hoistNonReactStatic(EnhancedComponent, Component);
}

function useClickOutside(onClickOutside = () => {}) {
    const [domNode, setDomNode] = React.useState(null);

    React.useEffect(() => {
        const onClick = e => {
            // console.log(domNode);
            // console.log(e.target);
            console.log(domNode);
            if ((!domNode || !domNode.contains(e.target)) && onClickOutside)
                onClickOutside(e);
        };

        document.addEventListener("click", onClick, true);
        return () => {
            document.removeEventListener("click", onClick, true);
        };
    }, [domNode, onClickOutside]);

    return React.useCallback(setDomNode, [onClickOutside]);
}
export default withClickOutside;
export { useClickOutside };

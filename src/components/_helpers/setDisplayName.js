export function setDisplayName(WrappedComponent, prefix, SourceComponent) {
    const sourceName =
        SourceComponent.displayName ||
        SourceComponent.name ||
        "AnonymousComponent";
    WrappedComponent.displayName = `${prefix}(${sourceName})`;
}

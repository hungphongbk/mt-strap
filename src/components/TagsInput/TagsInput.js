import React, { Component } from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import ReactTagsInput from "react-tagsinput";
import "../_helpers/_bootstrap.scss";
import styles from "./TagsInput.module.scss";

const TagsInput = ({ value, onChange, className, ...rest }) => {
    return (
        <div className={classnames(styles.TagsInput, className)}>
            <ReactTagsInput value={value} onChange={onChange} {...rest} />
        </div>
    );
};
TagsInput.propTypes = {
    /**
     * The value of the TagsInput. Must be a valid array
     *
     * @type Array
     * @required
     */
    value: PropTypes.arrayOf(PropTypes.any).isRequired,
    /**
     * Callback function that is fired when the TagsInput's value changed.
     *
     * @type function
     * @required
     */
    onChange: PropTypes.func.isRequired
};
TagsInput.defaultProps = {};
export default TagsInput;

import React from "react";
import Modal from "./Modal";
import { storiesOf } from "@storybook/react";

import DemoSimple from "../../demo/Modal_simple";
import interactDemo from "../../demo/Modal_interact";
import topDemo from "../../demo/Modal_top";
import formDemo from "../../demo/Modal_form";
import animationDemo from "../../demo/Modal_animation";

storiesOf("Modal", module)
    .add("simple", () => DemoSimple)
    .add("interactive", () => interactDemo)
    .add("top", () => topDemo)
    .add("form", () => formDemo)
    .add("animation", () => animationDemo);

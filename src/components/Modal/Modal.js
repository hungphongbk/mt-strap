import React from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import "../_helpers/_bootstrap.scss";
import "../../sass/components/_modals.scss";
import Portal from "react-portal/lib/PortalCompat";
import uniqueId from "../_helpers/uniqueId";
import { useClickOutside } from "../_helpers/react-click-outside";
import ModalHeader from "../ModalHeader/ModalHeader";
import { ContextModal } from "../_helpers/utils";
import { Transition } from "react-transition-group";
import { duration } from "../_helpers/styles/transitions";
import styles from "./Modal.module.scss";

function createElement(id) {
    const node = document.createElement("div");
    [["id", id], ["role", "presentation"]].forEach(([attr, val]) => {
        node.setAttribute(attr, val);
    });
    return node;
}

function Modal({
    isOpen,
    toggle,
    size,
    position,
    disablePortal,
    disableBackdropClick,
    closeButton,
    className,
    children,
    disableCloseOnEsc,
    animation,
    ...others
}) {
    const id = React.useRef(uniqueId(5, "modal-")),
        portalDom = React.useRef(createElement(id.current + "-bg")),
        refClickOutside = useClickOutside(
            React.useCallback(() => {
                if (!disableBackdropClick)
                    // eslint-disable-next-line no-unused-expressions
                    toggle?.(false);
            }, [isOpen])
        );

    React.useEffect(() => {
        if (!disablePortal) document.body.appendChild(portalDom.current);

        return () => {
            if (!disablePortal)
                // eslint-disable-next-line react-hooks/exhaustive-deps
                portalDom.current.remove();
        };
    }, []);

    React.useEffect(() => {
        if (!animation) {
            if (isOpen) portalDom.current.classList.add("mt-modal-bg");
            else portalDom.current.classList.remove("mt-modal-bg");
        }
    }, [isOpen, animation]);

    const newChildren = React.Children.map(children, child => {
        if (child.type === ModalHeader) {
            const newProps = {
                _modalId: id.current + "-title"
            };
            if (closeButton)
                newProps.children = [
                    child.props.children,
                    <i
                        key={"close-button"}
                        className="li-cross2 mt-modal-close-icon"
                        onClick={() => toggle(false)}
                    />
                ];
            return React.cloneElement(child, newProps);
        }
        return child;
    });

    const modal = (
        <div
            tabIndex={1}
            role={"dialog"}
            id={id.current}
            aria-modal={true}
            className={classnames(
                `mt-modal mt-modal-${size} mt-modal-position-${position}`,
                className
            )}
            ref={refClickOutside}
            onKeyDown={e =>
                e.keyCode === 27 && !disableCloseOnEsc && toggle(false)
            }
            {...others}
        >
            <ContextModal.Provider value={{ inModal: true }}>
                {newChildren}
            </ContextModal.Provider>
        </div>
    );

    if (disablePortal) return isOpen ? modal : null;
    if (!animation)
        return (
            <Portal node={portalDom.current}>
                {isOpen ? (
                    <div
                        className={"mt-modal-bg-overlay"}
                        ref={el => el?.firstChild.focus()}
                    >
                        {modal}
                    </div>
                ) : null}
            </Portal>
        );

    return (
        <Portal node={portalDom.current}>
            <Transition
                in={isOpen}
                timeout={duration.complex}
                mountOnEnter
                unmountOnExit
            >
                {state => (
                    <div
                        className={classnames(
                            "mt-modal-bg",
                            styles.Overlay,
                            styles[state]
                        )}
                    >
                        <div
                            className={classnames("mt-modal-bg-overlay")}
                            ref={el => el?.firstChild.focus()}
                        >
                            <div className={styles.Modal}>{modal}</div>
                        </div>
                    </div>
                )}
            </Transition>
        </Portal>
    );
}

Modal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    toggle: PropTypes.func,
    size: PropTypes.oneOf(["md", "lg"]),
    position: PropTypes.oneOf(["center", "top"]),
    disablePortal: PropTypes.bool,
    disableBackdropClick: PropTypes.bool,
    disableCloseOnEsc: PropTypes.bool,
    closeButton: PropTypes.bool,
    animation: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.number,
        PropTypes.shape({ enter: PropTypes.number, exit: PropTypes.number })
    ])
};
Modal.defaultProps = {
    size: "md",
    position: "center",
    disablePortal: false,
    disableBackdropClick: false,
    disableCloseOnEsc: false,
    closeButton: true,
    animation: false
};

export default Modal;

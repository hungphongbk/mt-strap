import React from "react";
import styles from "./ValueSlider.module.scss";
import * as classnames from "classnames";
import * as PropTypes from "prop-types";
import ValueSliderTrack from "./ValueSliderTrack";
import ValueSliderThumb from "./ValueSliderThumb";
import {
    getPositionFromEvent,
    getPositionsFromValues,
    getStepValueFromValue,
    getValueFromPosition,
    length,
    withInteraction
} from "./utils";

@withInteraction
class ValueSliderRange extends React.PureComponent {
    /**
     *
     * @type {?Component}
     */
    track = null;

    state = {
        isSliderDragging: null
    };

    constructor(props) {
        super(props);
        this.props.setInteractionEndListener(this.handleInteractionEnd);
    }
    getValuePercentage = () => {
        const { value, min, max } = this.props,
            abs = Math.abs;

        const percentageMin = (abs(value.min - min) * 1.0) / abs(max - min),
            percentageMax = (abs(value.max - min) * 1.0) / abs(max - min);
        return {
            min: percentageMin,
            max: percentageMax
        };
    };
    getTrackClientRect = () => this.track.getClientRect();
    //region Event handlers
    handleThumbDrag = (e, key) => {
        if (this.props.disabled) return;
        const position = getPositionFromEvent(e, this.getTrackClientRect());
        this.setState({ isSliderDragging: key }, () =>
            requestAnimationFrame(() => this.updatePosition(key, position))
        );
    };
    handleInteractionEnd = () => {
        if (this.state.isSliderDragging) {
            this.setState({
                isSliderDragging: null
            });
        }
    };
    //endregion
    updatePosition = (key, position) => {
        const positions = getPositionsFromValues(
            this.props.value,
            this.props.min,
            this.props.max,
            this.getTrackClientRect()
        );
        positions[key] = position;
        this.updatePositions(positions);
    };
    updatePositions = positions => {
        const values = {
            min: getValueFromPosition(
                positions.min,
                this.props.min,
                this.props.max,
                this.getTrackClientRect()
            ),
            max: getValueFromPosition(
                positions.max,
                this.props.min,
                this.props.max,
                this.getTrackClientRect()
            )
        };

        const transformedValues = {
            min: getStepValueFromValue(values.min, this.props.step),
            max: getStepValueFromValue(values.max, this.props.step)
        };
        // console.log(transformedValues);
        this.updateValues(transformedValues);
    };
    updateValues = values => {
        if (!this.shouldUpdate(values)) {
            return;
        }

        this.props.onChange(values);
    };
    shouldUpdate = values => {
        return this.isWithinRange(values) && this.hasStepDifference(values);
    };
    isWithinRange = values => {
        return (
            values.min >= this.props.min &&
            values.max <= this.props.max &&
            /*this.props.allowSameValues
            ? values.min <= values.max
            :*/ values.min <
                values.max
        );
    };
    hasStepDifference = values => {
        const currentValues = this.props.value;
        return (
            length(values.min, currentValues.min) >= this.props.step ||
            length(values.max, currentValues.max) >= this.props.step
        );
    };
    render() {
        const { props } = this,
            percentage = this.getValuePercentage();
        return (
            <div
                className={classnames(
                    styles.Container,
                    styles.MultipleRange,
                    props.rippleEffect && styles.WithRippleEffect,
                    props.className
                )}
                ref={this.props.bindElement}
                onMouseDown={this.props.onMouseDown}
                onTouchStart={this.props.onTouchStart}
            >
                <ValueSliderTrack
                    value={percentage}
                    ref={$track => {
                        this.track = $track;
                    }}
                >
                    <ValueSliderThumb
                        className={classnames(
                            this.state.isSliderDragging === "min" &&
                                styles.Dragging
                        )}
                        value={percentage.min}
                        onDrag={e => this.handleThumbDrag(e, "min")}
                    />
                    <ValueSliderThumb
                        className={classnames(
                            this.state.isSliderDragging === "max" &&
                                styles.Dragging
                        )}
                        value={percentage.max}
                        onDrag={e => this.handleThumbDrag(e, "max")}
                    />
                </ValueSliderTrack>
            </div>
        );
    }
}
ValueSliderRange.propTypes = {
    min: PropTypes.number.isRequired,
    max: PropTypes.number.isRequired,
    step: PropTypes.number,
    value: PropTypes.shape({
        min: PropTypes.number,
        max: PropTypes.number
    }).isRequired,
    onChange: PropTypes.func.isRequired,
    onChangeCompleted: PropTypes.func,
    disabled: PropTypes.bool,
    rippleEffect: PropTypes.bool,
    className: PropTypes.any
};
ValueSliderRange.defaultProps = {
    onChangeCompleted: () => {},
    step: 1,
    disabled: false,
    rippleEffect: false
};

export default ValueSliderRange;

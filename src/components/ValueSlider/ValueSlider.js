import React from "react";
import "../_helpers/_bootstrap.scss";
import range from "./ValueSliderRange";
import * as PropTypes from "prop-types";

const types = { range };

const ValueSlider = ({ type, ...rest }) => {
    const Component = types[type];
    return <Component {...rest} />;
};
ValueSlider.propTypes = {
    /**
     * The minimum allowed value of the slider. Should not be equal to max.
     *
     * @type number
     * @required
     */
    min: PropTypes.number.isRequired,
    /**
     * The maximum allowed value of the slider. Should not be equal to min.
     *
     * @type number
     * @required
     */
    max: PropTypes.number.isRequired,
    /**
     * The granularity the slider can step through values.
     *
     * @type number
     * @default 1
     */
    step: PropTypes.number,
    /**
     * The value of the slider.
     */
    value: PropTypes.shape({
        min: PropTypes.number,
        max: PropTypes.number
    }).isRequired,
    /**
     * Callback function that is fired when the slider's value changed.
     *
     * @type function
     * @required
     */
    onChange: PropTypes.func.isRequired,
    /**
     * Callback function that is fired when the slider's value changed & slider thumb was released.
     *
     * @type function
     */
    onChangeCompleted: PropTypes.func,
    /**
     * If `true`, the slider will be disabled.
     *
     * @type boolean
     * @default false
     */
    disabled: PropTypes.bool,
    /**
     * @type boolean
     * @default false
     */
    rippleEffect: PropTypes.bool,
    /**
     * Direction of the slider (horizontal by default or vertical)
     *
     * @type ('horizontal'|'vertical')
     * @default 'horizontal'
     */
    direction: PropTypes.oneOf(["horizontal", "vertical"]),
    /**
     * @type any
     */
    className: PropTypes.any
};
ValueSlider.defaultProps = {
    direction: "horizontal"
};
export default ValueSlider;

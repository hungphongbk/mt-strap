import React from "react";
import * as PropTypes from "prop-types";
import styles from "./ValueSlider.module.scss";
import classnames from "classnames";

export default class ValueSliderTrack extends React.Component {
    /**
     *
     * @type {?HTMLElement}
     */
    node = null;
    static propTypes = {
        value: PropTypes.shape({
            min: PropTypes.number,
            max: PropTypes.number
        })
    };
    getActiveTrackStyle = () => {
        const { value } = this.props;
        const width = `${(value.max - value.min) * 100}%`;
        const left = `${value.min * 100}%`;

        return { left, width };
    };
    getClientRect() {
        return this.node.getBoundingClientRect();
    }
    render() {
        return (
            <div
                className={classnames(styles.Track)}
                ref={node => (this.node = node)}
            >
                <div
                    className={styles.TrackActive}
                    style={this.getActiveTrackStyle()}
                />
                {this.props.children}
            </div>
        );
    }
}

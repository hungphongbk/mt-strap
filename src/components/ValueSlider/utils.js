// eslint no-unused-expressions: 0
import React from "react";

function clamp(value, min, max) {
    return Math.min(Math.max(value, min), max);
}
export function length(numA, numB) {
    return Math.abs(numA - numB);
}
export function getPositionFromEvent(event, clientRect) {
    const len = clientRect.width;
    const { clientX } = event.touches ? event.touches[0] : event;

    return {
        x: clamp(clientX - clientRect.left, 0, len),
        y: 0
    };
}

/**
 * Convert a model value into a percentage value
 * @ignore
 * @param {number} value
 * @param {number} minValue
 * @param {number} maxValue
 * @return {number}
 */
export function getPercentageFromValue(value, minValue, maxValue) {
    const validValue = clamp(value, minValue, maxValue);
    const valueDiff = maxValue - minValue;
    const valuePerc = (validValue - minValue) / valueDiff;

    return valuePerc || 0;
}

/**
 * Convert a value into a point
 * @ignore
 * @param {number} value
 * @param {number} minValue
 * @param {number} maxValue
 * @param {ClientRect} clientRect
 * @return {Point} Position
 */
export function getPositionFromValue(value, minValue, maxValue, clientRect) {
    const length = clientRect.width;
    const valuePerc = getPercentageFromValue(value, minValue, maxValue);
    const positionValue = valuePerc * length;

    return {
        x: positionValue,
        y: 0
    };
}

/**
 * Convert a range of values into points
 * @ignore
 * @param {Range} values
 * @param {number} minValue
 * @param {number} maxValue
 * @param {ClientRect} clientRect
 * @return {Range}
 */
export function getPositionsFromValues(values, minValue, maxValue, clientRect) {
    return {
        min: getPositionFromValue(values.min, minValue, maxValue, clientRect),
        max: getPositionFromValue(values.max, minValue, maxValue, clientRect)
    };
}

/**
 * Convert a value into a step value
 * @ignore
 * @param {number} value
 * @param {number} valuePerStep
 * @return {number}
 */
export function getStepValueFromValue(value, valuePerStep) {
    return Math.round(value / valuePerStep) * valuePerStep;
}

/**
 * Convert a point into a model value
 * @ignore
 * @param {Point} position
 * @param {number} minValue
 * @param {number} maxValue
 * @param {ClientRect} clientRect
 * @return {number}
 */
export function getValueFromPosition(position, minValue, maxValue, clientRect) {
    const sizePerc = getPercentageFromPosition(position, clientRect);
    const valueDiff = maxValue - minValue;

    return minValue + valueDiff * sizePerc;
}

/**
 * Convert a point into a percentage value
 * @ignore
 * @param {Point} position
 * @param {ClientRect} clientRect
 * @return {number} Percentage value
 */
export function getPercentageFromPosition(position, clientRect) {
    const len = clientRect.width;
    const sizePerc = position.x / len;

    return sizePerc || 0;
}

export const withInteraction = Component => {
    class withInteractionWrapper extends React.Component {
        /**
         *
         * @type {?HTMLElement}
         */
        nodeDoc = null;
        /**
         *
         * @type {?HTMLElement}
         */
        node = null;
        _onInteractionEnd = () => {};

        bindElement = node => {
            this.node = node;
            this.nodeDoc = node?.ownerDocument;
        };
        setInteractionEndListener = cb => (this._onInteractionEnd = cb);

        //region Event Handlers
        componentWillUnmount() {
            if (this.props.onDrag) {
                this.removeDocMouseMoveListener();
                this.removeDocTouchMoveListener();
            }
            this.removeDocMouseUpListener();
            this.removeDocTouchEndListener();
        }
        removeDocTouchMoveListener = () => {
            // eslint-disable-next-line no-unused-expressions
            this.nodeDoc?.removeEventListener(
                "touchmove",
                this.handleTouchMove
            );
        };
        addDocTouchMoveListener = () => {
            this.removeDocTouchMoveListener();
            // eslint-disable-next-line no-unused-expressions
            this.nodeDoc?.addEventListener("touchmove", this.handleTouchMove);
        };
        handleTouchMove = e => {
            // eslint-disable-next-line no-unused-expressions
            this.props.onDrag?.(e);
        };

        removeDocTouchEndListener = () => {
            // eslint-disable-next-line no-unused-expressions
            this.nodeDoc?.removeEventListener("touchend", this.handleTouchEnd);
        };
        addDocTouchEndListener = () => {
            this.removeDocTouchEndListener();
            // eslint-disable-next-line no-unused-expressions
            this.nodeDoc?.addEventListener("touchend", this.handleTouchEnd);
        };
        handleTouchEnd = () => {
            this._onInteractionEnd();
            this.removeDocTouchMoveListener();
            this.removeDocTouchEndListener();
        };

        removeDocMouseMoveListener = () => {
            // eslint-disable-next-line no-unused-expressions
            this.nodeDoc?.removeEventListener(
                "mousemove",
                this.handleMouseMove
            );
        };
        addDocMouseMoveListener = () => {
            this.removeDocMouseMoveListener();
            // eslint-disable-next-line no-unused-expressions
            this.nodeDoc?.addEventListener("mousemove", this.handleMouseMove);
        };
        handleMouseMove = e => {
            // eslint-disable-next-line no-unused-expressions
            this.props.onDrag?.(e);
        };

        removeDocMouseUpListener = () => {
            // eslint-disable-next-line no-unused-expressions
            this.nodeDoc?.removeEventListener("mouseup", this.handleMouseUp);
        };
        addDocMouseUpListener = () => {
            this.removeDocMouseUpListener();
            // eslint-disable-next-line no-unused-expressions
            this.nodeDoc?.addEventListener("mouseup", this.handleMouseUp);
        };
        handleMouseUp = () => {
            this._onInteractionEnd();
            this.removeDocMouseMoveListener();
            this.removeDocMouseUpListener();
        };

        /**
         * This function will be injected to component as a prop
         */
        handleMouseDown = () => {
            this.addDocMouseMoveListener();
            this.addDocMouseUpListener();
        };

        /**
         * This function will be injected to component as a prop
         */
        handleTouchStart = () => {
            this.addDocTouchEndListener();
            this.addDocTouchMoveListener();
        };
        //endregion

        render() {
            const additional = {
                node: this.node,
                bindElement: this.bindElement,
                setInteractionEndListener: this.setInteractionEndListener,
                onMouseDown: this.handleMouseDown,
                onTouchStart: this.handleTouchStart
            };
            return <Component {...this.props} {...additional} />;
        }
    }

    return withInteractionWrapper;
};

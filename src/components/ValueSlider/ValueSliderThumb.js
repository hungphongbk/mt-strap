import React from "react";
import * as PropTypes from "prop-types";
import styles from "./ValueSlider.module.scss";
import { withInteraction } from "./utils";
import classnames from "classnames";

@withInteraction
class ValueSliderThumb extends React.PureComponent {
    static propTypes = {
        value: PropTypes.number.isRequired,
        onDrag: PropTypes.func.isRequired,
        className: PropTypes.any
    };
    static defaultProps = {
        className: null
    };

    getStyle = () => {
        const perc = (this.props.value || 0) * 100;
        return {
            position: "absolute",
            left: `${perc}%`
        };
    };

    render() {
        return (
            <span ref={this.props.bindElement} style={this.getStyle()}>
                <div
                    draggable={false}
                    className={classnames(styles.Thumb, this.props.className)}
                    onMouseDown={this.props.onMouseDown}
                    onTouchStart={this.props.onTouchStart}
                />
            </span>
        );
    }
}

export default ValueSliderThumb;

import React from "react";
import ValueSlider from "./ValueSlider";
import { storiesOf } from "@storybook/react";
import * as PropTypes from "prop-types";

class DoubleSliderDemo extends React.Component {
    static propTypes = {
        rippleEffect: PropTypes.bool
    };
    state = {
        value: { min: 100000, max: 2000000 }
    };
    render() {
        return (
            <div>
                <ValueSlider
                    type={"range"}
                    min={0}
                    max={10000000}
                    step={1000}
                    value={this.state.value}
                    rippleEffect={this.props.rippleEffect}
                    onChange={value => this.setState({ value })}
                />
                <p>
                    Min value: <strong>{this.state.value.min}</strong> | Max
                    value: <strong>{this.state.value.max}</strong>
                </p>
            </div>
        );
    }
}

storiesOf("ValueSlider", module).add("range slider", () => (
    <div>
        <div style={{ display: "inline-block", width: "300px" }}>
            <DoubleSliderDemo />
        </div>
        <div style={{ display: "inline-block", width: "300px" }}>
            <DoubleSliderDemo rippleEffect={true} />
        </div>
    </div>
));

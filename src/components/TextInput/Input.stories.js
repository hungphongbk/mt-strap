import React from "react";
import TextInput from "./TextInput";
import { storiesOf } from "@storybook/react";

import DemoSimple from "../../demo/Input_simple";

storiesOf("Input/TextInput", module).add("default", () => DemoSimple);

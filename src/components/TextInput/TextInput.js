import React, { Component } from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import "../_helpers/_bootstrap.scss";
import "../../sass/components/_inputs.scss";
// import debounce from "lodash/debounce";
import FormGroup from "../FormGroup/FormGroup";
import { addClassName } from "../_helpers/utils";
/**
 * @see https://philippspiess.com/scheduling-in-react/
 */
import { unstable_next } from "scheduler";

const TextInput = React.forwardRef(
    (
        {
            inputComponent,
            label,
            type,
            placeholder,
            value,
            onChange,
            variant,
            size,
            className,
            helperText,
            success,
            error,
            icon,
            iconPosition,
            disabled,
            ...otherProps
        },
        ref
    ) => {
        const [internalValue, setInternalValue] = React.useState(value),
            handleChange = e =>
                setInternalValue(
                    e.target.value,
                    unstable_next(() => onChange(internalValue))
                );

        const inputClassNames = [
            "mt-form-control",
            `mt-input-${variant}`,
            `mt-size-${size}`
        ];
        const InputComponent = inputComponent,
            baseInputProps = {
                ref,
                type,
                placeholder,
                disabled,
                className: inputClassNames
            },
            formGroupProps = {};
        let Icon;

        /**
         * Add some classes when TextInput has icon
         */
        if (icon) {
            addClassName(formGroupProps, "mt-form-group-icon");
            addClassName(baseInputProps, `mt-icon-input-${iconPosition}`);
            Icon = React.cloneElement(icon, {
                className: classnames(
                    icon.props.className,
                    `mt-input-icon mt-input-${iconPosition}-icon mt-input-icon-opacity`
                )
            });
        }

        /**
         * Attach user-customized className
         */
        baseInputProps.className = classnames(
            baseInputProps.className,
            className
        );

        return (
            <FormGroup
                error={error}
                success={success}
                {...formGroupProps}
                label={label}
            >
                <InputComponent
                    {...baseInputProps}
                    value={internalValue}
                    onChange={handleChange}
                />
                {icon ? Icon : null}
                {helperText && (
                    <span
                        className={
                            "mt-input-validated-text mt-text-desc mt-text-blueberry-light-10"
                        }
                    >
                        {helperText}
                    </span>
                )}
            </FormGroup>
        );
    }
);
TextInput.propTypes = {
    label: PropTypes.string,
    helperText: PropTypes.element,
    helperTextPosition: PropTypes.oneOf(["top", "bottom"]),
    /**
     * The component used for the `input` element.
     * Either a string to use a DOM element or a component.
     *
     * @type string|Component
     * @default 'input'
     */
    inputComponent: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.elementType
    ]),
    /**
     * Type of the `input` element. It should be [a valid HTML5 input type](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#Form_%3Cinput%3E_types).
     *
     * @type string
     * @default 'text'
     */
    type: PropTypes.string,
    /**
     * The short hint displayed in the input before the user enters a value.
     *
     * @type string
     * @default null
     */
    placeholder: PropTypes.string,
    disabled: PropTypes.bool,
    /**
     * The value of the `input` element
     *
     * @required
     * @type any
     */
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired,
    variant: PropTypes.string,
    size: PropTypes.string,
    className: PropTypes.any,
    error: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    success: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    icon: PropTypes.element,
    iconPosition: PropTypes.oneOf(["left", "right"])
};
TextInput.defaultProps = {
    helperTextPosition: "bottom",
    inputComponent: "input",
    type: "text",
    disabled: false,
    variant: "style",
    size: "md",
    error: false,
    success: false,
    iconPosition: "left"
};
export default TextInput;

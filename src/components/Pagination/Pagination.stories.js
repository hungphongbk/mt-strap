import React from "react";
import Pagination from "./Pagination";
import { storiesOf } from "@storybook/react";
import DemoSimple from "../../demo/Pagination_1_simple";
import PaginationWithTable from "../../demo/Pagination_withTable";

storiesOf("Pagination", module)
    .add("simple", () => DemoSimple)
    .add("with table", () => PaginationWithTable);

import React, { Component } from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import "../_helpers/_bootstrap.scss";
import styles from "./Pagination.module.scss";
import ReactPaginate from "react-paginate";

function Pagination(props) {
    return (
        <ReactPaginate
            previousLabel={<i className="li li-arrow-left" />}
            nextLabel={<i className="li li-arrow-right" />}
            breakLabel={"..."}
            breakClassName={styles.BreakMe}
            pageCount={props.pageCount}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            containerClassName={styles.Pagination}
            subContainerClassName={"pages pagination"}
            activeClassName={styles.Active}
            previousClassName={styles.Previous}
            nextClassName={styles.Next}
            onPageChange={props.onPageChange}
        />
    );
}

Pagination.propTypes = {
    /**
     * The total number of pages.
     * @required
     * @type number
     */
    pageCount: PropTypes.number.isRequired,
    /**
     * The method to call when a page is clicked. Exposes the current page object as an argument.
     * @type function
     */
    onPageChange: PropTypes.func,
    /**
     * The initial page selected.
     *
     * @type number
     * @default 0
     */
    initialPage: PropTypes.number
};
Pagination.defaultProps = {
    onPageChange: () => {}
};

export default Pagination;

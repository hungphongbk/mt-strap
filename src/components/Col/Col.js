import React from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";

const Col = props => {
    // check propTypes: at least one prop in xsml,xs,sm,md,lg must not be null
    // certainly, just check in development mode
    // update: default xsml=12, so ignore throw error
    const responsiveProps = { ...props };
    if (!responsiveProps.xsml) responsiveProps.xsml = 12;
    // if (process.env.NODE_ENV !== "production") {
    //     let allNull = true;
    //     ["xsml", "xs", "sm", "md", "lg"].forEach(prop => {
    //         if (props[prop] !== null) {
    //             allNull = false;
    //             return false;
    //         }
    //         return true;
    //     });
    //     if (allNull)
    //         // throw new Error(
    //         //     "Error: at least one prop in xsml,xs,sm,md,lg must not be null"
    //         // );
    // }

    const classes = [];
    ["xsml", "xs", "sm", "md", "lg"].forEach(prop => {
        if (responsiveProps[prop] !== null)
            classes.push(`mt-col-${prop}-${responsiveProps[prop]}`);
    });
    classes.push(props.className);

    return <div className={classnames(classes)}>{props.children}</div>;
};
Col.propTypes = {
    className: PropTypes.any,
    xsml: PropTypes.number,
    xs: PropTypes.number,
    sm: PropTypes.number,
    md: PropTypes.number,
    lg: PropTypes.number,
    offset: PropTypes.oneOfType([PropTypes.number, PropTypes.object])
};
Col.defaultProps = {
    xsml: null,
    xs: null,
    sm: null,
    md: null,
    lg: null
};

export default Col;

import React, { Component } from "react";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import "../_helpers/_bootstrap.scss";
import "./Step.scss";

export default class Step extends Component {
    // constructor(props) {
    //     super(props);
    //     this.state = {};
    // }
    render() {
        return <div className="" />;
    }
}

Step.propTypes = {
    /**
     * Set the active step (zero based index).
     *
     * @type number
     * @default 0
     */
    activeStep: PropTypes.number
};
Step.defaultProps = {
    activeStep: 0
};

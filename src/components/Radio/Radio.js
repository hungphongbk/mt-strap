import CheckboxRadioBase from "../CheckboxRadioBase/CheckboxRadioBase";
import withProps from "../_helpers/withProps";
import * as PropTypes from "prop-types";

const Radio = withProps({ type: "radio" })(CheckboxRadioBase);

Radio.propTypes = {
    /**
     * Specifies a large or default checkbox.
     *
     * @type ('default'|'lg')
     * @default 'default'
     */
    size: PropTypes.oneOf(["default", "lg"]),
    /**
     * Disables the checkbox, preventing mouse events, even if the underlying component is an <a> element
     * @type boolean
     * @default false
     */
    disabled: PropTypes.bool,
    checked: PropTypes.bool.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    name: PropTypes.string,
    inline: PropTypes.bool
};
Radio.defaultProps = {
    size: "default",
    disabled: false,
    inline: false
};

export default Radio;

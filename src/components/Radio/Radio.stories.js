import React from "react";
import { storiesOf } from "@storybook/react";

import radioForm from "../../demo/Radio_form";

storiesOf("Input/Radio", module).add("with form", () => radioForm);

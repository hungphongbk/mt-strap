import React from "react";
import classnames from "classnames";
import * as PropTypes from "prop-types";

const Row = ({ className, children, component: Component }) => (
    <Component className={classnames("mt-row", className)}>
        {children}
    </Component>
);

Row.propTypes = {
    component: PropTypes.oneOf([PropTypes.string, PropTypes.elementType]),
    className: PropTypes.any
};
Row.defaultProps = {
    component: "div"
};

export default Row;

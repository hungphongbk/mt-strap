import React, { PureComponent } from "react";
import "../_helpers/_bootstrap.scss";
import "../../sass/components/_tabs.scss";
import * as PropTypes from "prop-types";
import classnames from "classnames";
import uniqueId from "../_helpers/uniqueId";

export default class Tabs extends PureComponent {
    static defaultProps = {
        border: true,
        activeKey: null,
        onChange: () => {},
        justify: false
    };
    state = {
        id: uniqueId(5, "tab-"),
        initialized: false,
        activeKey: "",
        outerWidth: 0,
        innerWidth: 0,
        overflow: false,
        tabKeys: [],
        tabs: {}
    };
    static getDerivedStateFromProps(props, state) {
        const newState = {},
            assign = obj => Object.assign(newState, obj);
        if (state.tabKeys.length === 0)
            assign({
                tabKeys: React.Children.map(props.children, c => c.props.tabKey)
            });
        if (!props.activeKey && !state.initialized)
            assign({
                initialized: true,
                activeKey: (newState.tabKeys || state.tabKeys)[0]
            });
        if (props.activeKey && props.activeKey !== state.activeKey)
            assign({
                activeKey: props.activeKey
            });
        return newState;
    }

    get classnames() {
        const classes = ["mt-tab mt-tab-style mt-tab-size"];
        if (this.props.border) classes.push("mt-tab-border");

        return classnames(classes);
    }
    selectTab = tabKey =>
        this.setState({ activeKey: tabKey }, () => this.props.onChange(tabKey));
    changeTabPosition = () => {};
    setTabWrapper = el =>
        el !== null &&
        this.setState({
            innerWidth: el.scrollWidth,
            outerWidth: el.clientWidth,
            overflow: el.scrollWidth > el.clientWidth
        });
    setTabElement = (tabKey, el) =>
        el &&
        !this.state.tabs[tabKey] &&
        this.setState(state => ({
            tabs: Object.assign({}, state.tabs, {
                [tabKey]: el
            })
        }));
    render() {
        const tabItemStyle = {};
        if (this.props.justify)
            tabItemStyle.width =
                100.0 / React.Children.count(this.props.children) + "%";
        return (
            <div className={this.classnames}>
                <div className="mt-tab-body" ref={this.setTabWrapper}>
                    <div role="tablist">
                        {React.Children.map(
                            this.props.children,
                            (Tab, index) => {
                                return (
                                    <div
                                        role="tab"
                                        id={this.state.id + Tab.props.tabKey}
                                        ref={el =>
                                            this.setTabElement(
                                                Tab.props.tabKey,
                                                el
                                            )
                                        }
                                        key={Tab.props.tabKey}
                                        style={tabItemStyle}
                                        className={classnames(
                                            "mt-tab-item",
                                            Tab.props.tabKey ===
                                                this.state.activeKey &&
                                                "mt-tab-active"
                                        )}
                                        onClickCapture={() =>
                                            this.selectTab(Tab.props.tabKey)
                                        }
                                    >
                                        <a
                                            className="mt-tab-link"
                                            href="#"
                                            onClick={e => e.preventDefault()}
                                        >
                                            {Tab.props.title}
                                        </a>
                                    </div>
                                );
                            }
                        )}
                        {this.state.overflow && [
                            <div
                                key={"__tab-nav-left"}
                                className="mt-tab-arrow mt-tab-arrow-left"
                            >
                                <li className="li-chevron-left mt-tab-icon" />
                            </div>,
                            <div
                                key={"__tab-nav-right"}
                                className="mt-tab-arrow mt-tab-arrow-right"
                            >
                                <li className="li-chevron-right mt-tab-icon" />
                            </div>
                        ]}
                    </div>
                </div>
                <div className="mt-tab-content">
                    {React.Children.map(this.props.children, Tab =>
                        React.cloneElement(Tab, {
                            role: "tabpanel",
                            "aria-labelledby": this.state.id + Tab.props.tabKey,
                            key: Tab.props.tabKey,
                            isActive: Tab.props.tabKey === this.state.activeKey
                        })
                    )}
                </div>
            </div>
        );
    }
}

Tabs.propTypes = {
    /**
     * This component requires an array of Tab components
     *
     * @required
     * @type Tab[]
     */
    children: PropTypes.arrayOf(PropTypes.element).isRequired,
    /**
     * Make a border around Tabs becomes available
     *
     * @type boolean
     * @default true
     */
    border: PropTypes.bool,
    activeKey: PropTypes.string,
    onChange: PropTypes.func,
    justify: PropTypes.bool
};

import React from "react";
import Tabs from "./Tabs";
import Tab from "../Tab/Tab";
import { storiesOf } from "@storybook/react";
import styled from "styled-components";

const renderTabs = (suffix = "") => [
    <Tab tabKey={"tab1" + suffix} title={"First Tab"}>
        <h3>HTML Ipsum Presents</h3>
        <p>
            <strong>Pellentesque habitant morbi tristique</strong> senectus et
            netus et malesuada fames ac turpis egestas. Vestibulum tortor quam,
            feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu
            libero sit amet quam egestas semper.{" "}
            <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend
            leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum
            erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit
            amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros
            ipsum rutrum orci, sagittis tempus lacus enim ac dui.{" "}
            <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut
            felis.
        </p>
        <blockquote>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus
                magna. Cras in mi at felis aliquet congue. Ut a est eget ligula
                molestie gravida. Curabitur massa. Donec eleifend, libero at
                sagittis mollis, tellus est malesuada tellus, at luctus turpis
                elit sit amet quam. Vivamus pretium ornare est.
            </p>
        </blockquote>
    </Tab>,
    <Tab tabKey={"tab2" + suffix} title={"Another Second Tab"}>
        <h2>Header Level 2</h2>
        <ol>
            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
            <li>Aliquam tincidunt mauris eu risus.</li>
        </ol>
        <p>
            Pellentesque habitant morbi tristique senectus et netus et malesuada
            fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae,
            ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam
            egestas semper. Aenean ultricies mi vitae est. Mauris placerat
            eleifend leo.
        </p>
    </Tab>,
    <Tab tabKey={"tab3" + suffix} title={"And the last Tab"}>
        <ul>
            <li>
                Morbi in sem quis dui placerat ornare. Pellentesque odio nisi,
                euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras
                consequat.
            </li>
            <li>
                Praesent dapibus, neque id cursus faucibus, tortor neque egestas
                augue, eu vulputate magna eros eu erat. Aliquam erat volutpat.
                Nam dui mi, tincidunt quis, accumsan porttitor, facilisis
                luctus, metus.
            </li>
            <li>
                Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec
                consectetuer ligula vulputate sem tristique cursus. Nam nulla
                quam, gravida non, commodo a, sodales sit amet, nisi.
            </li>
            <li>
                Pellentesque fermentum dolor. Aliquam quam lectus, facilisis
                auctor, ultrices ut, elementum vulputate, nunc.
            </li>
        </ul>
    </Tab>
];

const Panel = styled.div`
    width: ${props => props.w || 50}%;
    max-width: ${props => props.w || 50}%;
`;

storiesOf("Tabs", module)
    .add("Tab default", () => <Tabs>{renderTabs()}</Tabs>)
    .add("Tab with justify layout", () => (
        <Tabs justify={true}>{renderTabs()}</Tabs>
    ))
    .add("Tab scrollable", () => (
        <Panel w={50}>
            <Tabs>
                {renderTabs()}
                {renderTabs("-1")}
            </Tabs>
        </Panel>
    ));

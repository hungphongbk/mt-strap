import React from "react";
import ReactDOM from "react-dom";
import Button from "mt-strap/es/Button/Button";

function App() {
    return <Button variant={"blue"}>Hello world!</Button>;
}

ReactDOM.render(<App />, document.querySelector("#app"));

export default <App />;

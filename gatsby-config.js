const path = require("path");

const pluginSass = {
    resolve: `gatsby-plugin-sass`,
    options: {
        includePaths: [path.resolve(__dirname, "src/sass/core/_mixins-mt.scss")]
    }
};
if (process.env.NODE_ENV !== "development")
    pluginSass.options.cssLoaderOptions = {
        localIdentName: "[name]--[local]-[hash:base64:5]"
    };

module.exports = {
    pathPrefix: "/react",
    siteMetadata: {
        title: `Gatsby Default Starter`
    },
    plugins: [
        {
            resolve: `gatsby-mdx`,
            options: {
                defaultLayouts: {
                    default: path.resolve(
                        "./src/pages-components/Layout/Layout.js"
                    )
                },
                gatsbyRemarkPlugins: [
                    {
                        resolve: `gatsby-remark-prismjs`
                    }
                ],
                globalScope: `
                import ReactPlayground from "${__dirname}/src/pages-components/ReactPlayground/ReactPlayground";
                import ComponentApi from "${__dirname}/src/pages-components/ComponentAPI/ComponentAPI";
                `
            }
        },
        `gatsby-plugin-react-helmet`,
        pluginSass,
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `pages`,
                path: `${__dirname}/src/pages`
            }
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `components`,
                path: `${__dirname}/src/components`
            }
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `demo`,
                path: `${__dirname}/src/demo/`,
                ignore: [`**\/.*`]
            }
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `images`,
                path: `${__dirname}/src/media`
            }
        },
        `gatsby-plugin-sharp`,
        `gatsby-transformer-sharp`,
        {
            resolve: `gatsby-plugin-react-svg`,
            options: {
                rule: {
                    include: /src\/media/ // See below to configure properly
                }
            }
        }
        // `frontmatter-custom-fields`
    ]
};

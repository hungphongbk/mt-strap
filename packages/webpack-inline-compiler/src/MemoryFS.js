const MemoryFileSystem = require("memory-fs"),
    fs = require("fs");

class MemoryFS extends MemoryFileSystem {
    stat(_path, cb) {
        super.stat(_path, function(err, result) {
            if (err) return fs.stat(_path, cb);
            else return cb(err, result);
        });
    }
    readFile(_path, cb) {
        super.readFile(_path, function(err, result) {
            if (err) return fs.readFile(_path, cb);
            else return cb(err, result);
        });
    }
}

module.exports = MemoryFS;

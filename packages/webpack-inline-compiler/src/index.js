const webpack = require("webpack"),
    MemoryFS = require("./MemoryFS"),
    root = require("app-root-path"),
    path = require("path"),
    thenify = require("thenify");

const memoryFs = new MemoryFS();

async function compile(code, configCustom = config => config) {
    const rootDir = root.toString(),
        outputName = `entry.js`,
        entry = path.join(rootDir, outputName);
    // console.log(rootDir);
    // process.exit(0);
    const rootExists = memoryFs.existsSync(rootDir);
    if (!rootExists) memoryFs.mkdirpSync(rootDir);
    memoryFs.writeFileSync(entry, code);

    const config = configCustom({
            entry: entry,
            output: {
                filename: outputName
            },
            module: {
                rules: []
            },
            externals: [
                {
                    react: "React",
                    "prop-types": "PropTypes",
                    classnames: "classnames",
                    lodash: "_",
                    "react-router-dom": "ReactRouterDOM"
                }
            ]
        }),
        compiler = webpack(config);
    compiler.inputFileSystem = memoryFs;
    compiler.resolvers.normal.fileSystem = memoryFs;
    compiler.outputFileSystem = memoryFs;
    compiler.run = thenify(compiler.run);

    const stats = await compiler.run();
    if (memoryFs.existsSync(entry)) memoryFs.unlinkSync(entry);

    const errors = stats.compilation.errors;
    if (errors && errors.length > 0) {
        //if there are errors, throw the first one
        throw errors[0];
    }
    // console.log(Object.getOwnPropertyNames(stats.compilation.assets));
    return stats.compilation.assets[outputName].source();
}

module.exports = compile;

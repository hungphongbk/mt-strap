const compile = require("./src/index");

const customWebpack = config => {
    config.module.rules.push({
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
            loader: "babel-loader",
            options: {
                presets: ["@babel/preset-env", "@babel/react"]
            }
        }
    });
    return config;
};

compile(
    `
import React, {render} from 'react';

render(<div>Foo</div>);
`,
    customWebpack
).then(source => {
    console.log(source);
    process.exit(0);
});

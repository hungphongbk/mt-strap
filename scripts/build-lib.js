const { green, cyan, red } = require("chalk");

const path = require("path");
const fse = require("fs-extra");
const execa = require("execa");
const rimraf = require("rimraf");
const util = require("util");

const targets = process.argv.slice(2),
    srcRoot = path.join(__dirname, "../src/components"),
    libRoot = path.join(__dirname, "../lib"),
    esRoot = path.join(libRoot, "es"),
    clean = () => fse.existsSync(libRoot) && fse.removeSync(libRoot),
    rimrafAsync = util.promisify(rimraf);

const step = (name, root, fn) => async () => {
    console.log(cyan("Building: ") + green(name));
    await fn();
    console.log(cyan("Built: ") + green(name));
};

const shell = cmd => execa.shell(cmd, { stdio: ["pipe", "pipe", "inherit"] });

const buildLib = step("commonjs modules", libRoot, () =>
    shell(
        `npx babel ${srcRoot} --out-dir ${libRoot} --env-name "lib" --copy-files --ignore ${srcRoot}/**/*.stories.js`
    )
);

const buildEsm = step("es modules", esRoot, () =>
    shell(
        `npx babel ${srcRoot} --out-dir ${esRoot} --env-name "esm" --copy-files --ignore ${srcRoot}/**/*.stories.js`
    ).then(() => rimrafAsync(esRoot + "/**/*.stories.js"))
);

console.log(
    green(`Building targets: ${targets.length ? targets.join(", ") : "all"}\n`)
);

clean();

// .catch(err => {
//     if (err) console.error(red(err.stack || err.toString()));
//     process.exit(1);
// });
(async function() {
    await Promise.all([buildEsm()]);
    await Promise.all(
        ["sass", "theme"].map(dir =>
            shell(
                `cp -a ${path.join(
                    __dirname,
                    "../src"
                )}/${dir}/. ${libRoot}/${dir}`
            )
        )
    );
})();

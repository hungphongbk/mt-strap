const inquirer = require("inquirer"),
    execa = require("execa"),
    path = require("path"),
    fs = require("fs-extra");

const shell = cmd => execa.shell(cmd, { stdio: ["pipe", "pipe", "inherit"] }),
    getPath = filePath => path.join(__dirname, "../src/components", filePath),
    deleteFile = filePath => fs.unlinkSync(getPath(filePath)),
    renameFile = (filePath, _newFilePath) => {
        let newFilePath = _newFilePath;
        if (typeof _newFilePath === "function")
            newFilePath = _newFilePath(filePath);
        fs.moveSync(getPath(filePath), getPath(newFilePath));
    };

function updateFile(filename, callback) {
    return new Promise(function(resolve) {
        fs.readFile(getPath(filename), "utf-8", function(err, data) {
            if (err) {
                throw err;
            } else {
                fs.writeFile(
                    getPath(filename),
                    callback(data),
                    "utf-8",
                    function(_err) {
                        if (_err) {
                            throw _err;
                        } else {
                            resolve();
                        }
                    }
                );
            }
        });
    });
}

inquirer
    .prompt([
        {
            type: "rawlist",
            name: "type",
            message: "What type of component you want to scaffold?",
            choices: [
                { name: "ES6 Classes", value: "component" },
                { name: "Functional", value: "fcomponent" }
            ],
            default: "component"
        },
        {
            type: "input",
            name: "name",
            message: "What's component name?"
        },
        {
            type: "rawlist",
            name: "style",
            message: "Ship component with CSS/SCSS?",
            choices: [
                { name: "None", value: "none" },
                { name: "CSS", value: "css" },
                { name: "SCSS", value: "scss" }
            ],
            default: "scss"
        },
        {
            type: "confirm",
            name: "story",
            message: "Create storybook?",
            default: true
        }
    ])
    .then(async ({ type, name, style, story }) => {
        await shell(`rg component -t ${type} ${name}`);
        if (style === "none") {
            deleteFile(`${name}/${name}.module.scss`);
            await updateFile(`${name}/${name}.js`, content =>
                content.replace(/^.*?\.module\.scss.*$/gm, "")
            );
        } else if (style === "css") {
            renameFile(`${name}/${name}.module.scss`, p =>
                p.replace(/scss/, "css")
            );
            await updateFile(`${name}/${name}.js`, content =>
                content.replace(/^(.*?\.module\.)scss(.*)$/gm, "$1css$2")
            );
        }

        if (story === false) {
            deleteFile(`${name}/${name}.stories.js`);
        }
    })
    .then(() => process.exit(0));

const glob = require("glob");
const _ = require("lodash");
const fs = require("fs");
const path = require("path");

function traverseFile(file) {
    if (/stories/.test(file)) return null;
    if (/_/.test(file)) return null;
    if (/index\.js/.test(file)) return null;
    const _file = file.replace("src/components/", "").split("/");

    return _file[0];
}

glob("src/components/**/*.js", {}, (err, files) => {
    if (err) {
        throw err;
    }
    const components = _.uniq(files.map(traverseFile).filter(c => c !== null)),
        content =
            components
                .map(
                    component =>
                        `export ${component} from "./${component}/${component}";`
                )
                .join("\n") + "\n",
        dTsContent =
            components
                .map(
                    component =>
                        `export { default as ${component} } from "./${component}/${component}";`
                )
                .join("\n") + "\n";
    fs.writeFileSync(
        path.join(__dirname, "../src/components/index.js"),
        content
    );
    fs.writeFileSync(path.join(__dirname, "../lib/es/index.d.ts"), dTsContent);
    process.exit(0);
});

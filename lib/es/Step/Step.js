"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _react = _interopRequireWildcard(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

require("./Step.scss");

var React$createElement = _react.default.createElement;

var Step =
/*#__PURE__*/
function (_Component) {
  (0, _inheritsLoose2.default)(Step, _Component);

  function Step() {
    return _Component.apply(this, arguments) || this;
  }

  var _proto = Step.prototype;

  // constructor(props) {
  //     super(props);
  //     this.state = {};
  // }
  _proto.render = function render() {
    return React$createElement("div", {
      className: ""
    });
  };

  return Step;
}(_react.Component);

exports.default = Step;
Step.propTypes = {
  /**
   * Set the active step (zero based index).
   *
   * @type number
   * @default 0
   */
  activeStep: PropTypes.number
};
Step.defaultProps = {
  activeStep: 0
};
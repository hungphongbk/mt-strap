"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _objectWithoutPropertiesLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutPropertiesLoose"));

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _react = _interopRequireWildcard(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

var _BreadcrumbModule = _interopRequireDefault(require("./Breadcrumb.module.scss"));

var _reactRouterDom = require("react-router-dom");

var React$createElement = _react.default.createElement;

var Breadcrumb =
/*#__PURE__*/
function (_React$PureComponent) {
  (0, _inheritsLoose2.default)(Breadcrumb, _React$PureComponent);

  function Breadcrumb() {
    var _this;

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _React$PureComponent.call.apply(_React$PureComponent, [this].concat(args)) || this;

    _this.manipulateChildren = function () {
      var newArr = [];

      _react.default.Children.forEach(_this.props.children, function (child) {
        if (typeof child !== "undefined" && child !== null) newArr.push(React$createElement("li", null, _this.renderBreadCrumbItem(child)), React$createElement("li", null, React$createElement("span", {
          className: _BreadcrumbModule.default.Separator
        }, _this.props.separator)));
      });

      newArr.pop();
      return React$createElement("nav", {
        "aria-label": "Breadcrumb"
      }, React$createElement("ol", {
        className: (0, _classnames.default)(_BreadcrumbModule.default.BreadCrumbOl, _this.props.className)
      }, newArr));
    };

    _this.renderBreadCrumbItem = function (child) {
      if (typeof child === "undefined" || child === null) return null;
      var _child$props = child.props,
          className = _child$props.className,
          props = (0, _objectWithoutPropertiesLoose2.default)(_child$props, ["className"]);
      return React$createElement(_reactRouterDom.NavLink, Object.assign({
        className: (0, _classnames.default)(_BreadcrumbModule.default.Item, className)
      }, props, {
        activeClassName: _BreadcrumbModule.default.ItemActive
      }));
    };

    return _this;
  }

  var _proto = Breadcrumb.prototype;

  _proto.render = function render() {
    return this.manipulateChildren();
  };

  return Breadcrumb;
}(_react.default.PureComponent);

Breadcrumb.propTypes = {
  className: PropTypes.any,
  separator: PropTypes.element
};
Breadcrumb.defaultProps = {
  separator: React$createElement("i", {
    className: "li li-chevron-right"
  })
};
var _default = Breadcrumb;
exports.default = _default;
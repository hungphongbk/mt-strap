"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var React$createElement = _react.default.createElement;

var Col = function Col(props) {
  // check propTypes: at least one prop in xsml,xs,sm,md,lg must not be null
  // certainly, just check in development mode
  // update: default xsml=12, so ignore throw error
  var responsiveProps = Object.assign({}, props);
  if (!responsiveProps.xsml) responsiveProps.xsml = 12; // if (process.env.NODE_ENV !== "production") {
  //     let allNull = true;
  //     ["xsml", "xs", "sm", "md", "lg"].forEach(prop => {
  //         if (props[prop] !== null) {
  //             allNull = false;
  //             return false;
  //         }
  //         return true;
  //     });
  //     if (allNull)
  //         // throw new Error(
  //         //     "Error: at least one prop in xsml,xs,sm,md,lg must not be null"
  //         // );
  // }

  var classes = [];
  ["xsml", "xs", "sm", "md", "lg"].forEach(function (prop) {
    if (responsiveProps[prop] !== null) classes.push("mt-col-" + prop + "-" + responsiveProps[prop]);
  });
  classes.push(props.className);
  return React$createElement("div", {
    className: (0, _classnames.default)(classes)
  }, props.children);
};

Col.propTypes = {
  className: PropTypes.any,
  xsml: PropTypes.number,
  xs: PropTypes.number,
  sm: PropTypes.number,
  md: PropTypes.number,
  lg: PropTypes.number,
  offset: PropTypes.oneOfType([PropTypes.number, PropTypes.object])
};
Col.defaultProps = {
  xsml: null,
  xs: null,
  sm: null,
  md: null,
  lg: null
};
var _default = Col;
exports.default = _default;
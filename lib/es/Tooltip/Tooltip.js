"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _react = _interopRequireDefault(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

require("./Tooltip.scss");

var React$createElement = _react.default.createElement;

var Tooltip =
/*#__PURE__*/
function (_React$PureComponent) {
  (0, _inheritsLoose2.default)(Tooltip, _React$PureComponent);

  function Tooltip() {
    var _this;

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _React$PureComponent.call.apply(_React$PureComponent, [this].concat(args)) || this;
    _this.state = {
      isMouseEnter: false
    };

    _this.handleMouseEnter = function () {
      !_this.state.isMouseEnter && _this.setState({
        isMouseEnter: true
      });
    };

    _this.handleMouseOut = function () {
      _this.setState({
        isMouseEnter: false
      });
    };

    return _this;
  }

  var _proto = Tooltip.prototype;

  _proto.render = function render() {
    var _this$props = this.props,
        variant = _this$props.variant,
        position = _this$props.position,
        content = _this$props.content,
        children = _this$props.children,
        className = _this$props.className,
        contentClassName = _this$props.contentClassName; // console.count(`mouseenter = ${this.state.isMouseEnter}`);

    return React$createElement("div", {
      className: (0, _classnames.default)("mt-tooltip", "mt-tooltip-default", this.state.isMouseEnter && "mt-tooltip-hover", variant === "light" ? "mt-tooltip-style" : "mt-tooltip-dark", "mt-tooltip-" + position, className),
      onMouseEnter: this.handleMouseEnter,
      onMouseLeave: this.handleMouseOut
    }, children, React$createElement("div", {
      className: (0, _classnames.default)("mt-tooltip-content", contentClassName)
    }, content));
  };

  return Tooltip;
}(_react.default.PureComponent);

Tooltip.propTypes = {
  /**
   * Specific tooltip style (light or dark)
   *
   * @type ('light'|'dark')
   * @default 'light'
   */
  variant: PropTypes.oneOf(["light", "dark"]),

  /**
   * Sets the direction the Tooltip is positioned towards.
   *
   * @type ('top'|'bottom'|'right'|'left')
   * @default 'top'
   */
  position: PropTypes.oneOf(["top", "bottom", "right", "left"]),

  /**
   * Tooltip content
   *
   * @type (string|Element)
   */
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
  children: PropTypes.element,
  className: PropTypes.any,

  /**
   * @type string
   */
  contentClassName: PropTypes.any
};
Tooltip.defaultProps = {
  variant: "light",
  position: "top",
  contentClassName: null
};
var _default = Tooltip;
exports.default = _default;
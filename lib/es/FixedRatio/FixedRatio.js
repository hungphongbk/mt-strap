"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _style = _interopRequireDefault(require("styled-jsx/style"));

var _objectWithoutPropertiesLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutPropertiesLoose"));

var _react = _interopRequireWildcard(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

require("../_helpers/_bootstrap.scss");

var React$createElement = _react.default.createElement;

// import './FixedRatio.scss'
var FixedRatio = _react.default.forwardRef(function (_ref, ref) {
  var width = _ref.width,
      height = _ref.height,
      _ref$className = _ref.className,
      className = _ref$className === void 0 ? "" : _ref$className,
      _ref$style = _ref.style,
      style = _ref$style === void 0 ? {} : _ref$style,
      children = _ref.children,
      rest = (0, _objectWithoutPropertiesLoose2.default)(_ref, ["width", "height", "className", "style", "children"]);
  return React$createElement("div", {
    style: style,
    className: _style.default.dynamic([["3128998080", [height * 100 / width]]]) + " " + ((0, _classnames.default)("FixedRatio__root", className) || "")
  }, React$createElement("div", {
    ref: ref,
    className: _style.default.dynamic([["3128998080", [height * 100 / width]]]) + " " + "FixedRatio__content"
  }, children), React$createElement(_style.default, {
    id: "3128998080",
    dynamic: [height * 100 / width]
  }, [".FixedRatio__root.__jsx-style-dynamic-selector{position:relative;}", ".FixedRatio__root.__jsx-style-dynamic-selector:before{content:\"\";display:block;padding-top:" + height * 100 / width + "%;}", ".FixedRatio__content.__jsx-style-dynamic-selector{position:absolute;top:0;left:0;bottom:0;right:0;}"]));
});

FixedRatio.propTypes = {
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  className: PropTypes.any,
  style: PropTypes.object
};
var _default = FixedRatio;
exports.default = _default;
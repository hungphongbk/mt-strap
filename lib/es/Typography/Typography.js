"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutPropertiesLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutPropertiesLoose"));

var _react = _interopRequireDefault(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

var React$createElement = _react.default.createElement;
var defaultVariantMapping = {
  h1: "h1",
  h2: "h2",
  h3: "h3",
  h4: "h4",
  h5: "h5",
  h6: "h6",
  subtitle1: "h6",
  subtitle2: "h6",
  "text-df": "p",
  "text-desc": "p",
  "text-smallest": "p"
};

var Typography = function Typography(_ref) {
  var className = _ref.className,
      component = _ref.component,
      variant = _ref.variant,
      color = _ref.color,
      children = _ref.children,
      other = (0, _objectWithoutPropertiesLoose2.default)(_ref, ["className", "component", "variant", "color", "children"]);
  var Component = component;
  if (!Component) Component = defaultVariantMapping[variant];
  var classes = ["mt-" + variant, "mt-text-" + color];
  return React$createElement(Component, (0, _extends2.default)({
    className: (0, _classnames.default)(classes, className)
  }, other), children);
};

Typography.propTypes = {
  className: PropTypes.string,

  /**
   * The component used for the root node. Either a string to use a DOM element or a component. By default, it maps the variant to a good default headline component.
   */
  component: PropTypes.elementType,

  /**
   * The content of the component.
   */
  children: PropTypes.any,

  /**
   * The color of the component. It supports those theme colors that make sense for this component.
   */
  color: PropTypes.oneOf(["blue", "blue-light-10", "blue-light-20", "blue-light-30", "blue-dark-10", "blue-dark-20", "blue-dark-30", "pink", "pink-light-10", "pink-light-20", "pink-light-30", "pink-dark-10", "pink-dark-20", "pink-dark-30", "green", "green-light-10", "green-light-20", "green-light-30", "green-dark-10", "green-dark-20", "green-dark-30", "orange", "orange-light-10", "orange-light-20", "orange-light-30", "orange-dark-10", "orange-dark-20", "orange-dark-30", "red", "red-light-10", "red-light-20", "red-light-30", "red-dark-10", "red-dark-20", "red-dark-30", "blueberry", "blueberry-light-10", "blueberry-light-20", "blueberry-light-30", "white"]),
  variant: PropTypes.oneOf(["h1", "h2", "h3", "h4", "h5", "h6", "text-df", "text-desc", "text-smallest"])
};
Typography.defaultProps = {
  color: "blueberry",
  variant: "text-df"
};
var _default = Typography;
exports.default = _default;
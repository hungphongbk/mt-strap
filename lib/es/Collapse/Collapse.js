"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutPropertiesLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutPropertiesLoose"));

var _react = _interopRequireDefault(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames2 = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

var _CollapseModule = _interopRequireDefault(require("./Collapse.module.scss"));

var _withForwardRef = _interopRequireDefault(require("../_helpers/withForwardRef"));

var _transitions = require("../_helpers/styles/transitions");

var _reactTransitionGroup = require("react-transition-group");

/* eslint no-unused-expressions: 0 */
var React$createElement = _react.default.createElement;

var Collapse = function Collapse(props) {
  var children = props.children,
      className = props.className,
      _props$collapsedHeigh = props.collapsedHeight,
      collapsedHeight = _props$collapsedHeigh === void 0 ? "0px" : _props$collapsedHeigh,
      _props$component = props.component,
      Component = _props$component === void 0 ? "div" : _props$component,
      inProp = props.isOpen,
      onEnter = props.onEnter,
      onEntered = props.onEntered,
      onEntering = props.onEntering,
      onExit = props.onExit,
      onExiting = props.onExiting,
      style = props.style,
      theme = props.theme,
      _props$timeout = props.timeout,
      timeout = _props$timeout === void 0 ? _transitions.duration.standard : _props$timeout,
      ref = props.ref,
      other = (0, _objectWithoutPropertiesLoose2.default)(props, ["children", "className", "collapsedHeight", "component", "isOpen", "onEnter", "onEntered", "onEntering", "onExit", "onExiting", "style", "theme", "timeout", "ref"]);

  var timer = _react.default.useRef(null),

  /**
   *
   * @type {React.MutableRefObject<HTMLElement>}
   */
  wrapperRef = _react.default.useRef(null),
      autoTransitionDuration = _react.default.useRef();

  _react.default.useEffect(function () {
    return function () {
      clearTimeout(timer.current);
    };
  }, []);

  var handleEnter = function handleEnter(node) {
    node.style.height = collapsedHeight;
    onEnter === null || onEnter === void 0 ? void 0 : onEnter(node);
  };

  var handleEntering = function handleEntering(node) {
    var _wrapperRef$current;

    var wrapperHeight = ((_wrapperRef$current = wrapperRef.current) === null || _wrapperRef$current === void 0 ? void 0 : _wrapperRef$current.clientHeight) || 0;

    var _getTransitionProps = (0, _transitions.getTransitionProps)({
      style: style,
      timeout: timeout
    }, {
      mode: "enter"
    }),
        transitionDuration = _getTransitionProps.duration;

    if (timeout === "auto") {
      var duration2 = (0, _transitions.getAutoHeightDuration)(wrapperHeight);
      node.style.transitionDuration = duration2 + "ms";
      autoTransitionDuration.current = duration2;
    } else {
      node.style.transitionDuration = typeof transitionDuration === "string" ? transitionDuration : transitionDuration + "ms";
    }

    node.style.height = wrapperHeight + "px";
    onEntering === null || onEntering === void 0 ? void 0 : onEntering(node);
  };

  var handleEntered = function handleEntered(node) {
    node.style.height = "auto";
    onEntered === null || onEntered === void 0 ? void 0 : onEntered(node);
  };

  var handleExit = function handleExit(node) {
    var _wrapperRef$current2;

    var wrapperHeight = ((_wrapperRef$current2 = wrapperRef.current) === null || _wrapperRef$current2 === void 0 ? void 0 : _wrapperRef$current2.clientHeight) || 0;
    node.style.height = wrapperHeight + "px";
    onExit === null || onExit === void 0 ? void 0 : onExit(node);
  };

  var handleExiting = function handleExiting(node) {
    var _wrapperRef$current3;

    var wrapperHeight = ((_wrapperRef$current3 = wrapperRef.current) === null || _wrapperRef$current3 === void 0 ? void 0 : _wrapperRef$current3.clientHeight) || 0;

    var _getTransitionProps2 = (0, _transitions.getTransitionProps)({
      style: style,
      timeout: timeout
    }, {
      mode: "exit"
    }),
        transitionDuration = _getTransitionProps2.duration;

    if (timeout === "auto") {
      var duration2 = (0, _transitions.getAutoHeightDuration)(wrapperHeight);
      node.style.transitionDuration = duration2 + "ms";
      autoTransitionDuration.current = duration2;
    } else {
      node.style.transitionDuration = typeof transitionDuration === "string" ? transitionDuration : transitionDuration + "ms";
    }

    node.style.height = collapsedHeight;
    console.log(collapsedHeight);
    onExiting === null || onExiting === void 0 ? void 0 : onExiting(node);
  };

  var addEndListener = function addEndListener(_, next) {
    if (timeout === "auto") {
      timer.current = setTimeout(next, autoTransitionDuration.current || 0);
    }
  };

  return React$createElement(_reactTransitionGroup.Transition, (0, _extends2.default)({
    in: inProp,
    onEnter: handleEnter,
    onEntered: handleEntered,
    onEntering: handleEntering,
    onExit: handleExit,
    onExiting: handleExiting,
    addEndListener: addEndListener,
    timeout: timeout === "auto" ? null : timeout
  }, other), function (state, childProps) {
    var _classnames;

    return React$createElement(Component, {
      className: (0, _classnames2.default)(_CollapseModule.default.Container, (_classnames = {}, _classnames[_CollapseModule.default.Entered] = state === "entered", _classnames[_CollapseModule.default.Hidden] = state === "exited" && !inProp && collapsedHeight === "0px", _classnames), className),
      style: Object.assign({
        minHeight: collapsedHeight
      }, style),
      ref: ref
    }, React$createElement("div", {
      className: _CollapseModule.default.Wrapper,
      ref: wrapperRef
    }, React$createElement("div", {
      className: _CollapseModule.default.WrapperInner
    }, children)));
  });
};

Collapse = (0, _withForwardRef.default)(Collapse);
Collapse.propTypes = {
  /**
   * The height of the container when collapsed.
   */
  collapsedHeight: PropTypes.string,

  /**
   * If `true`, the component will transition in.
   */
  isOpen: PropTypes.bool,

  /**
   * @ignore
   */
  onEnter: PropTypes.func,

  /**
   * @ignore
   */
  onEntered: PropTypes.func,

  /**
   * @ignore
   */
  onEntering: PropTypes.func,

  /**
   * @ignore
   */
  onExit: PropTypes.func,

  /**
   * @ignore
   */
  onExiting: PropTypes.func,

  /**
   * The duration for the transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   *
   * Set to 'auto' to automatically calculate transition time based on height.
   */
  timeout: PropTypes.oneOfType([PropTypes.number, PropTypes.shape({
    enter: PropTypes.number,
    exit: PropTypes.number
  }), PropTypes.oneOf(["auto"])]),

  /**
   * @ignore
   */
  innerRef: PropTypes.any
};
Collapse.defaultProps = {};
var _default = Collapse;
exports.default = _default;
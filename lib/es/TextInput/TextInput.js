"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutPropertiesLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutPropertiesLoose"));

var _react = _interopRequireWildcard(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

require("../../sass/components/_inputs.scss");

var _FormGroup = _interopRequireDefault(require("../FormGroup/FormGroup"));

var _utils = require("../_helpers/utils");

var _scheduler = require("scheduler");

var React$createElement = _react.default.createElement;

var TextInput = _react.default.forwardRef(function (_ref, ref) {
  var inputComponent = _ref.inputComponent,
      label = _ref.label,
      type = _ref.type,
      placeholder = _ref.placeholder,
      value = _ref.value,
      onChange = _ref.onChange,
      variant = _ref.variant,
      size = _ref.size,
      className = _ref.className,
      helperText = _ref.helperText,
      success = _ref.success,
      error = _ref.error,
      icon = _ref.icon,
      iconPosition = _ref.iconPosition,
      disabled = _ref.disabled,
      otherProps = (0, _objectWithoutPropertiesLoose2.default)(_ref, ["inputComponent", "label", "type", "placeholder", "value", "onChange", "variant", "size", "className", "helperText", "success", "error", "icon", "iconPosition", "disabled"]);

  var _React$useState = _react.default.useState(value),
      internalValue = _React$useState[0],
      setInternalValue = _React$useState[1],
      handleChange = function handleChange(e) {
    return setInternalValue(e.target.value, (0, _scheduler.unstable_next)(function () {
      return onChange(internalValue);
    }));
  };

  var inputClassNames = ["mt-form-control", "mt-input-" + variant, "mt-size-" + size];
  var InputComponent = inputComponent,
      baseInputProps = {
    ref: ref,
    type: type,
    placeholder: placeholder,
    disabled: disabled,
    className: inputClassNames
  },
      formGroupProps = {};
  var Icon;
  /**
   * Add some classes when TextInput has icon
   */

  if (icon) {
    (0, _utils.addClassName)(formGroupProps, "mt-form-group-icon");
    (0, _utils.addClassName)(baseInputProps, "mt-icon-input-" + iconPosition);
    Icon = _react.default.cloneElement(icon, {
      className: (0, _classnames.default)(icon.props.className, "mt-input-icon mt-input-" + iconPosition + "-icon mt-input-icon-opacity")
    });
  }
  /**
   * Attach user-customized className
   */


  baseInputProps.className = (0, _classnames.default)(baseInputProps.className, className);
  return React$createElement(_FormGroup.default, (0, _extends2.default)({
    error: error,
    success: success
  }, formGroupProps, {
    label: label
  }), React$createElement(InputComponent, (0, _extends2.default)({}, baseInputProps, {
    value: internalValue,
    onChange: handleChange
  })), icon ? Icon : null, helperText && React$createElement("span", {
    className: "mt-input-validated-text mt-text-desc mt-text-blueberry-light-10"
  }, helperText));
});

TextInput.propTypes = {
  label: PropTypes.string,
  helperText: PropTypes.element,
  helperTextPosition: PropTypes.oneOf(["top", "bottom"]),

  /**
   * The component used for the `input` element.
   * Either a string to use a DOM element or a component.
   *
   * @type string|Component
   * @default 'input'
   */
  inputComponent: PropTypes.oneOfType([PropTypes.string, PropTypes.elementType]),

  /**
   * Type of the `input` element. It should be [a valid HTML5 input type](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#Form_%3Cinput%3E_types).
   *
   * @type string
   * @default 'text'
   */
  type: PropTypes.string,

  /**
   * The short hint displayed in the input before the user enters a value.
   *
   * @type string
   * @default null
   */
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,

  /**
   * The value of the `input` element
   *
   * @required
   * @type any
   */
  value: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired,
  variant: PropTypes.string,
  size: PropTypes.string,
  className: PropTypes.any,
  error: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  success: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  icon: PropTypes.element,
  iconPosition: PropTypes.oneOf(["left", "right"])
};
TextInput.defaultProps = {
  helperTextPosition: "bottom",
  inputComponent: "input",
  type: "text",
  disabled: false,
  variant: "style",
  size: "md",
  error: false,
  success: false,
  iconPosition: "left"
};
var _default = TextInput;
exports.default = _default;
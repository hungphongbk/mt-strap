"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _react = _interopRequireWildcard(require("react"));

require("../_helpers/_bootstrap.scss");

require("./ButtonGroup.scss");

var React$createElement = _react.default.createElement;

// import { connect } from "react-redux";
// import { bindActionCreators } from "redux";
// import * as ButtonGroupActions from "../../store/ButtonGroup/actions";
var ButtonGroup =
/*#__PURE__*/
function (_Component) {
  (0, _inheritsLoose2.default)(ButtonGroup, _Component);

  function ButtonGroup() {
    return _Component.apply(this, arguments) || this;
  }

  var _proto = ButtonGroup.prototype;

  // constructor(props) {
  //     super(props);
  //     this.state = {};
  // }
  _proto.render = function render() {
    return React$createElement("div", {
      className: "component-button-group"
    }, "Hello! component ButtonGroup");
  };

  return ButtonGroup;
}(_react.Component); // export default connect(
//     ({ ButtonGroup }) => ({ ...ButtonGroup }),
//     dispatch => bindActionCreators({ ...ButtonGroupActions }, dispatch)
//   )( ButtonGroup );


exports.default = ButtonGroup;
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _fastdom2 = _interopRequireDefault(require("fastdom"));

var _fastdomPromised = _interopRequireDefault(require("fastdom/extensions/fastdom-promised"));

// if (process.env.NODE_ENV !== "production") require("fastdom/fastdom-strict");
var fastdom = _fastdom2.default.extend(_fastdomPromised.default);

var _default = fastdom;
exports.default = _default;
"use strict";

exports.__esModule = true;
exports.wordLefts = void 0;

/**
 * Extract first N words from sentences (default 10 words)
 *
 * @author hungphongbk
 * @param {string} str
 * @param {number} count
 * @returns {string}
 */
var wordLefts = function wordLefts(str, count) {
  if (count === void 0) {
    count = 10;
  }

  var newStr = str.split(" ").slice(0, count).join(" ");
  if (newStr.length < str.length) newStr += "...";
  return newStr;
};

exports.wordLefts = wordLefts;
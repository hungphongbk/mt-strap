"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.ContextModal = exports.addClassName = void 0;

var _react = _interopRequireDefault(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var React$createElement = _react.default.createElement;

var addClassName = function addClassName(obj) {
  for (var _len = arguments.length, classes = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    classes[_key - 1] = arguments[_key];
  }

  obj.className = _classnames.default.apply(void 0, [obj.className].concat(classes));
};

exports.addClassName = addClassName;

var ContextModal = _react.default.createContext({
  inModal: true
});

exports.ContextModal = ContextModal;
"use strict";

exports.__esModule = true;
exports.getAutoHeightDuration = getAutoHeightDuration;
exports.getTransitionProps = getTransitionProps;
exports.duration = void 0;
var duration = {
  shortest: 150,
  shorter: 200,
  short: 250,
  // most basic recommended timing
  standard: 300,
  // this is to be used in complex animations
  complex: 375,
  // recommended when something is entering screen
  enteringScreen: 225,
  // recommended when something is leaving screen
  leavingScreen: 195
};
exports.duration = duration;

function getAutoHeightDuration(height) {
  if (!height) {
    return 0;
  }

  var constant = height / 36; // https://www.wolframalpha.com/input/?i=(4+%2B+15+*+(x+%2F+36+)+**+0.25+%2B+(x+%2F+36)+%2F+5)+*+10

  return Math.round((4 + 15 * Math.pow(constant, 0.25) + constant / 5) * 10);
}

function getTransitionProps(props, options) {
  var timeout = props.timeout,
      _props$style = props.style,
      style = _props$style === void 0 ? {} : _props$style;
  return {
    duration: style.transitionDuration || typeof timeout === "number" ? timeout : timeout[options.mode],
    delay: style.transitionDelay
  };
}
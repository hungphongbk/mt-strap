"use strict";

exports.__esModule = true;
exports.capture = void 0;

var capture = function capture(callback) {
  return function (e) {
    e.stopPropagation();
    callback();
  };
};

exports.capture = capture;
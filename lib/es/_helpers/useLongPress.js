"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

exports.__esModule = true;
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var React$createElement = _react.default.createElement;

var LongPress = function LongPress(props) {
  var _useState = (0, _react.useState)(false),
      startLongPress = _useState[0],
      setStartLongPress = _useState[1];

  (0, _react.useEffect)(function () {
    if (startLongPress) {
      props.progressCb();
    } else {
      props.completedCb();
    } // this is here because minimum edit on SO is 6 characters :)

  }, [startLongPress]);
  return _react.default.cloneElement(_react.default.Children.only(props.children), {
    onMouseDown: function onMouseDown() {
      return setStartLongPress(true);
    },
    onMouseUp: function onMouseUp() {
      return setStartLongPress(false);
    },
    onMouseLeave: function onMouseLeave() {
      return setStartLongPress(false);
    },
    onTouchStart: function onTouchStart() {
      return setStartLongPress(true);
    },
    onTouchEnd: function onTouchEnd() {
      return setStartLongPress(false);
    }
  });
};

LongPress.propTypes = {
  progressCb: PropTypes.func,
  completedCb: PropTypes.func
};
LongPress.defaultProps = {
  progressCb: function progressCb() {},
  completedCb: function completedCb() {}
};
var _default = LongPress;
exports.default = _default;
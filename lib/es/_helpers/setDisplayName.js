"use strict";

exports.__esModule = true;
exports.setDisplayName = setDisplayName;

function setDisplayName(WrappedComponent, prefix, SourceComponent) {
  var sourceName = SourceComponent.displayName || SourceComponent.name || "AnonymousComponent";
  WrappedComponent.displayName = prefix + "(" + sourceName + ")";
}
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.useClickOutside = useClickOutside;
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutPropertiesLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutPropertiesLoose"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _setDisplayName = require("./setDisplayName");

var hoistNonReactStatic = require("hoist-non-react-statics");

var React = require("react");

var React$createElement = React.createElement;

var ReactDOM = require("react-dom");

function withClickOutside(Component) {
  var EnhancedComponent =
  /*#__PURE__*/
  function (_React$Component) {
    (0, _inheritsLoose2.default)(EnhancedComponent, _React$Component);

    function EnhancedComponent(props) {
      var _this;

      _this = _React$Component.call(this, props) || this;
      _this.handleClickOutside = _this.handleClickOutside.bind((0, _assertThisInitialized2.default)(_this));
      return _this;
    }

    var _proto = EnhancedComponent.prototype;

    _proto.componentDidMount = function componentDidMount() {
      document.addEventListener("click", this.handleClickOutside, true);
    };

    _proto.componentWillUnmount = function componentWillUnmount() {
      document.removeEventListener("click", this.handleClickOutside, true);
    };

    _proto.handleClickOutside = function handleClickOutside(e) {
      var domNode = this.__domNode;
      var wrappedInstance = this.__wrappedInstance;

      if ((!domNode || !domNode.contains(e.target)) && wrappedInstance && typeof wrappedInstance.handleClickOutside === "function") {
        wrappedInstance.handleClickOutside(e);
      }
    };

    _proto.render = function render() {
      var _this2 = this;

      var _this$props = this.props,
          wrappedRef = _this$props.wrappedRef,
          rest = (0, _objectWithoutPropertiesLoose2.default)(_this$props, ["wrappedRef"]);
      return React$createElement(Component, (0, _extends2.default)({}, rest, {
        ref: function ref(c) {
          _this2.__wrappedInstance = c; // eslint-disable-next-line react/no-find-dom-node

          _this2.__domNode = ReactDOM.findDOMNode(c);
          wrappedRef && wrappedRef(c);
        }
      }));
    };

    return EnhancedComponent;
  }(React.Component);

  (0, _setDisplayName.setDisplayName)(EnhancedComponent, "withClickOutside", Component);
  return hoistNonReactStatic(EnhancedComponent, Component);
}

function useClickOutside(onClickOutside) {
  if (onClickOutside === void 0) {
    onClickOutside = function onClickOutside() {};
  }

  var _React$useState = React.useState(null),
      domNode = _React$useState[0],
      setDomNode = _React$useState[1];

  React.useEffect(function () {
    var onClick = function onClick(e) {
      // console.log(domNode);
      // console.log(e.target);
      console.log(domNode);
      if ((!domNode || !domNode.contains(e.target)) && onClickOutside) onClickOutside(e);
    };

    document.addEventListener("click", onClick, true);
    return function () {
      document.removeEventListener("click", onClick, true);
    };
  }, [domNode, onClickOutside]);
  return React.useCallback(setDomNode, [onClickOutside]);
}

var _default = withClickOutside;
exports.default = _default;
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _react = _interopRequireDefault(require("react"));

var _src = _interopRequireDefault(require("hoist-non-react-statics/src"));

var React$createElement = _react.default.createElement;

var withForwardRef = function withForwardRef(Component) {
  var WithForwardRef = _react.default.forwardRef(function (props, ref) {
    return React$createElement(Component, (0, _extends2.default)({}, props, {
      innerRef: ref
    }));
  });

  (0, _src.default)(WithForwardRef, Component);
  return WithForwardRef;
};

var _default = withForwardRef;
exports.default = _default;
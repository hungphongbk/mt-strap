"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var React$createElement = _react.default.createElement;

function domTraverse(node, visitor) {
  return _traverse(node, visitor, {
    level: 0,
    parent: null
  });
}

function _traverse(node, visitor, state) {
  visitor(node, state);
  if (!node.props) return;

  var children = _react.default.Children.toArray(node.props.children);

  children.forEach(function (child) {
    return _traverse(child, visitor, {
      level: state.level + 1,
      parent: node
    });
  });
}

var _default = domTraverse;
exports.default = _default;
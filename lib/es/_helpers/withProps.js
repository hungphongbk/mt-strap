"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _hoistNonReactStatics = _interopRequireDefault(require("hoist-non-react-statics"));

var _setDisplayName = require("./setDisplayName");

var React$createElement = _react.default.createElement;

var withProps = function withProps(propsFn) {
  return function (Component) {
    var withPropsWrapper = _react.default.memo(function (props) {
      function throwError() {
        throw new Error("propsFn must be valid javascript pure object or function which accept props as first parameter");
      } //validate propFn must be valid object or function


      if (typeof propsFn === "undefined" || propsFn === null) throwError();
      if (typeof propsFn !== "function" && typeof propsFn !== "object") throwError();
      var newProps = Object.assign({}, props, typeof propsFn === "object" ? propsFn : propsFn(props));
      return React$createElement(Component, newProps);
    });

    (0, _hoistNonReactStatics.default)(withPropsWrapper, Component);
    (0, _setDisplayName.setDisplayName)(withPropsWrapper, "withProps", Component);
    return withPropsWrapper;
  };
};

var _default = withProps;
exports.default = _default;
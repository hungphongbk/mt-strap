"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

exports.__esModule = true;
exports.sizingPropType = exports.variantPropType = void 0;

var PropTypes = _interopRequireWildcard(require("prop-types"));

var variantPropType = PropTypes.oneOf(["blue", "green", "white", "pink", "red", "orange"]);
exports.variantPropType = variantPropType;
var sizingPropType = PropTypes.oneOf(["lg", "md", "sm"]);
exports.sizingPropType = sizingPropType;
"use strict";

exports.__esModule = true;
exports.default = void 0;

var uniqueId = function uniqueId(length, prefix) {
  if (length === void 0) {
    length = 10;
  }

  if (prefix === void 0) {
    prefix = "";
  }

  return prefix + [].concat(Array(length).fill(" ")).map(function (i) {
    return (~~(Math.random() * 36)).toString(36);
  }).join("");
};

var _default = uniqueId;
exports.default = _default;
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.length = length;
exports.getPositionFromEvent = getPositionFromEvent;
exports.getPercentageFromValue = getPercentageFromValue;
exports.getPositionFromValue = getPositionFromValue;
exports.getPositionsFromValues = getPositionsFromValues;
exports.getStepValueFromValue = getStepValueFromValue;
exports.getValueFromPosition = getValueFromPosition;
exports.getPercentageFromPosition = getPercentageFromPosition;
exports.withInteraction = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _react = _interopRequireDefault(require("react"));

// eslint no-unused-expressions: 0
var React$createElement = _react.default.createElement;

function clamp(value, min, max) {
  return Math.min(Math.max(value, min), max);
}

function length(numA, numB) {
  return Math.abs(numA - numB);
}

function getPositionFromEvent(event, clientRect) {
  var len = clientRect.width;

  var _ref = event.touches ? event.touches[0] : event,
      clientX = _ref.clientX;

  return {
    x: clamp(clientX - clientRect.left, 0, len),
    y: 0
  };
}
/**
 * Convert a model value into a percentage value
 * @ignore
 * @param {number} value
 * @param {number} minValue
 * @param {number} maxValue
 * @return {number}
 */


function getPercentageFromValue(value, minValue, maxValue) {
  var validValue = clamp(value, minValue, maxValue);
  var valueDiff = maxValue - minValue;
  var valuePerc = (validValue - minValue) / valueDiff;
  return valuePerc || 0;
}
/**
 * Convert a value into a point
 * @ignore
 * @param {number} value
 * @param {number} minValue
 * @param {number} maxValue
 * @param {ClientRect} clientRect
 * @return {Point} Position
 */


function getPositionFromValue(value, minValue, maxValue, clientRect) {
  var length = clientRect.width;
  var valuePerc = getPercentageFromValue(value, minValue, maxValue);
  var positionValue = valuePerc * length;
  return {
    x: positionValue,
    y: 0
  };
}
/**
 * Convert a range of values into points
 * @ignore
 * @param {Range} values
 * @param {number} minValue
 * @param {number} maxValue
 * @param {ClientRect} clientRect
 * @return {Range}
 */


function getPositionsFromValues(values, minValue, maxValue, clientRect) {
  return {
    min: getPositionFromValue(values.min, minValue, maxValue, clientRect),
    max: getPositionFromValue(values.max, minValue, maxValue, clientRect)
  };
}
/**
 * Convert a value into a step value
 * @ignore
 * @param {number} value
 * @param {number} valuePerStep
 * @return {number}
 */


function getStepValueFromValue(value, valuePerStep) {
  return Math.round(value / valuePerStep) * valuePerStep;
}
/**
 * Convert a point into a model value
 * @ignore
 * @param {Point} position
 * @param {number} minValue
 * @param {number} maxValue
 * @param {ClientRect} clientRect
 * @return {number}
 */


function getValueFromPosition(position, minValue, maxValue, clientRect) {
  var sizePerc = getPercentageFromPosition(position, clientRect);
  var valueDiff = maxValue - minValue;
  return minValue + valueDiff * sizePerc;
}
/**
 * Convert a point into a percentage value
 * @ignore
 * @param {Point} position
 * @param {ClientRect} clientRect
 * @return {number} Percentage value
 */


function getPercentageFromPosition(position, clientRect) {
  var len = clientRect.width;
  var sizePerc = position.x / len;
  return sizePerc || 0;
}

var withInteraction = function withInteraction(Component) {
  var withInteractionWrapper =
  /*#__PURE__*/
  function (_React$Component) {
    (0, _inheritsLoose2.default)(withInteractionWrapper, _React$Component);

    function withInteractionWrapper() {
      var _this;

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      _this = _React$Component.call.apply(_React$Component, [this].concat(args)) || this;
      _this.nodeDoc = null;
      _this.node = null;

      _this._onInteractionEnd = function () {};

      _this.bindElement = function (node) {
        _this.node = node;
        _this.nodeDoc = node === null || node === void 0 ? void 0 : node.ownerDocument;
      };

      _this.setInteractionEndListener = function (cb) {
        return _this._onInteractionEnd = cb;
      };

      _this.removeDocTouchMoveListener = function () {
        var _this$nodeDoc;

        // eslint-disable-next-line no-unused-expressions
        (_this$nodeDoc = _this.nodeDoc) === null || _this$nodeDoc === void 0 ? void 0 : _this$nodeDoc.removeEventListener("touchmove", _this.handleTouchMove);
      };

      _this.addDocTouchMoveListener = function () {
        var _this$nodeDoc2;

        _this.removeDocTouchMoveListener(); // eslint-disable-next-line no-unused-expressions


        (_this$nodeDoc2 = _this.nodeDoc) === null || _this$nodeDoc2 === void 0 ? void 0 : _this$nodeDoc2.addEventListener("touchmove", _this.handleTouchMove);
      };

      _this.handleTouchMove = function (e) {
        var _this$props$onDrag, _this$props;

        // eslint-disable-next-line no-unused-expressions
        (_this$props$onDrag = (_this$props = _this.props).onDrag) === null || _this$props$onDrag === void 0 ? void 0 : _this$props$onDrag.call(_this$props, e);
      };

      _this.removeDocTouchEndListener = function () {
        var _this$nodeDoc3;

        // eslint-disable-next-line no-unused-expressions
        (_this$nodeDoc3 = _this.nodeDoc) === null || _this$nodeDoc3 === void 0 ? void 0 : _this$nodeDoc3.removeEventListener("touchend", _this.handleTouchEnd);
      };

      _this.addDocTouchEndListener = function () {
        var _this$nodeDoc4;

        _this.removeDocTouchEndListener(); // eslint-disable-next-line no-unused-expressions


        (_this$nodeDoc4 = _this.nodeDoc) === null || _this$nodeDoc4 === void 0 ? void 0 : _this$nodeDoc4.addEventListener("touchend", _this.handleTouchEnd);
      };

      _this.handleTouchEnd = function () {
        _this._onInteractionEnd();

        _this.removeDocTouchMoveListener();

        _this.removeDocTouchEndListener();
      };

      _this.removeDocMouseMoveListener = function () {
        var _this$nodeDoc5;

        // eslint-disable-next-line no-unused-expressions
        (_this$nodeDoc5 = _this.nodeDoc) === null || _this$nodeDoc5 === void 0 ? void 0 : _this$nodeDoc5.removeEventListener("mousemove", _this.handleMouseMove);
      };

      _this.addDocMouseMoveListener = function () {
        var _this$nodeDoc6;

        _this.removeDocMouseMoveListener(); // eslint-disable-next-line no-unused-expressions


        (_this$nodeDoc6 = _this.nodeDoc) === null || _this$nodeDoc6 === void 0 ? void 0 : _this$nodeDoc6.addEventListener("mousemove", _this.handleMouseMove);
      };

      _this.handleMouseMove = function (e) {
        var _this$props$onDrag2, _this$props2;

        // eslint-disable-next-line no-unused-expressions
        (_this$props$onDrag2 = (_this$props2 = _this.props).onDrag) === null || _this$props$onDrag2 === void 0 ? void 0 : _this$props$onDrag2.call(_this$props2, e);
      };

      _this.removeDocMouseUpListener = function () {
        var _this$nodeDoc7;

        // eslint-disable-next-line no-unused-expressions
        (_this$nodeDoc7 = _this.nodeDoc) === null || _this$nodeDoc7 === void 0 ? void 0 : _this$nodeDoc7.removeEventListener("mouseup", _this.handleMouseUp);
      };

      _this.addDocMouseUpListener = function () {
        var _this$nodeDoc8;

        _this.removeDocMouseUpListener(); // eslint-disable-next-line no-unused-expressions


        (_this$nodeDoc8 = _this.nodeDoc) === null || _this$nodeDoc8 === void 0 ? void 0 : _this$nodeDoc8.addEventListener("mouseup", _this.handleMouseUp);
      };

      _this.handleMouseUp = function () {
        _this._onInteractionEnd();

        _this.removeDocMouseMoveListener();

        _this.removeDocMouseUpListener();
      };

      _this.handleMouseDown = function () {
        _this.addDocMouseMoveListener();

        _this.addDocMouseUpListener();
      };

      _this.handleTouchStart = function () {
        _this.addDocTouchEndListener();

        _this.addDocTouchMoveListener();
      };

      return _this;
    }

    var _proto = withInteractionWrapper.prototype;

    //region Event Handlers
    _proto.componentWillUnmount = function componentWillUnmount() {
      if (this.props.onDrag) {
        this.removeDocMouseMoveListener();
        this.removeDocTouchMoveListener();
      }

      this.removeDocMouseUpListener();
      this.removeDocTouchEndListener();
    };

    //endregion
    _proto.render = function render() {
      var additional = {
        node: this.node,
        bindElement: this.bindElement,
        setInteractionEndListener: this.setInteractionEndListener,
        onMouseDown: this.handleMouseDown,
        onTouchStart: this.handleTouchStart
      };
      return React$createElement(Component, (0, _extends2.default)({}, this.props, additional));
    };

    return withInteractionWrapper;
  }(_react.default.Component);

  return withInteractionWrapper;
};

exports.withInteraction = withInteraction;
"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _react = _interopRequireDefault(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _ValueSliderModule = _interopRequireDefault(require("./ValueSlider.module.scss"));

var _classnames = _interopRequireDefault(require("classnames"));

var React$createElement = _react.default.createElement;

var ValueSliderTrack =
/*#__PURE__*/
function (_React$Component) {
  (0, _inheritsLoose2.default)(ValueSliderTrack, _React$Component);

  function ValueSliderTrack() {
    var _this;

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _React$Component.call.apply(_React$Component, [this].concat(args)) || this;
    _this.node = null;

    _this.getActiveTrackStyle = function () {
      var value = _this.props.value;
      var width = (value.max - value.min) * 100 + "%";
      var left = value.min * 100 + "%";
      return {
        left: left,
        width: width
      };
    };

    return _this;
  }

  var _proto = ValueSliderTrack.prototype;

  _proto.getClientRect = function getClientRect() {
    return this.node.getBoundingClientRect();
  };

  _proto.render = function render() {
    var _this2 = this;

    return React$createElement("div", {
      className: (0, _classnames.default)(_ValueSliderModule.default.Track),
      ref: function ref(node) {
        return _this2.node = node;
      }
    }, React$createElement("div", {
      className: _ValueSliderModule.default.TrackActive,
      style: this.getActiveTrackStyle()
    }), this.props.children);
  };

  return ValueSliderTrack;
}(_react.default.Component);

exports.default = ValueSliderTrack;
ValueSliderTrack.propTypes = {
  value: PropTypes.shape({
    min: PropTypes.number,
    max: PropTypes.number
  })
};
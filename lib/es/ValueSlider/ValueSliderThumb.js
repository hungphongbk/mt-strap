"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

exports.__esModule = true;
exports.default = void 0;

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _react = _interopRequireDefault(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _ValueSliderModule = _interopRequireDefault(require("./ValueSlider.module.scss"));

var _utils = require("./utils");

var _classnames = _interopRequireDefault(require("classnames"));

var _class, _class2, _temp;

var React$createElement = _react.default.createElement;

var ValueSliderThumb = (0, _utils.withInteraction)(_class = (_temp = _class2 =
/*#__PURE__*/
function (_React$PureComponent) {
  (0, _inheritsLoose2.default)(ValueSliderThumb, _React$PureComponent);

  function ValueSliderThumb() {
    var _this;

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _React$PureComponent.call.apply(_React$PureComponent, [this].concat(args)) || this;

    _this.getStyle = function () {
      var perc = (_this.props.value || 0) * 100;
      return {
        position: "absolute",
        left: perc + "%"
      };
    };

    return _this;
  }

  var _proto = ValueSliderThumb.prototype;

  _proto.render = function render() {
    return React$createElement("span", {
      ref: this.props.bindElement,
      style: this.getStyle()
    }, React$createElement("div", {
      draggable: false,
      className: (0, _classnames.default)(_ValueSliderModule.default.Thumb, this.props.className),
      onMouseDown: this.props.onMouseDown,
      onTouchStart: this.props.onTouchStart
    }));
  };

  return ValueSliderThumb;
}(_react.default.PureComponent), _class2.propTypes = {
  value: PropTypes.number.isRequired,
  onDrag: PropTypes.func.isRequired,
  className: PropTypes.any
}, _class2.defaultProps = {
  className: null
}, _temp)) || _class;

var _default = ValueSliderThumb;
exports.default = _default;
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

exports.__esModule = true;
exports.default = void 0;

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _react = _interopRequireDefault(require("react"));

var _ValueSliderModule = _interopRequireDefault(require("./ValueSlider.module.scss"));

var classnames = _interopRequireWildcard(require("classnames"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _ValueSliderTrack = _interopRequireDefault(require("./ValueSliderTrack"));

var _ValueSliderThumb = _interopRequireDefault(require("./ValueSliderThumb"));

var _utils = require("./utils");

var _class, _temp;

var React$createElement = _react.default.createElement;

var ValueSliderRange = (0, _utils.withInteraction)(_class = (_temp =
/*#__PURE__*/
function (_React$PureComponent) {
  (0, _inheritsLoose2.default)(ValueSliderRange, _React$PureComponent);

  /**
   *
   * @type {?Component}
   */
  function ValueSliderRange(props) {
    var _this;

    _this = _React$PureComponent.call(this, props) || this;
    _this.track = null;
    _this.state = {
      isSliderDragging: null
    };

    _this.getValuePercentage = function () {
      var _this$props = _this.props,
          value = _this$props.value,
          min = _this$props.min,
          max = _this$props.max,
          abs = Math.abs;
      var percentageMin = abs(value.min - min) * 1.0 / abs(max - min),
          percentageMax = abs(value.max - min) * 1.0 / abs(max - min);
      return {
        min: percentageMin,
        max: percentageMax
      };
    };

    _this.getTrackClientRect = function () {
      return _this.track.getClientRect();
    };

    _this.handleThumbDrag = function (e, key) {
      if (_this.props.disabled) return;
      var position = (0, _utils.getPositionFromEvent)(e, _this.getTrackClientRect());

      _this.setState({
        isSliderDragging: key
      }, function () {
        return requestAnimationFrame(function () {
          return _this.updatePosition(key, position);
        });
      });
    };

    _this.handleInteractionEnd = function () {
      if (_this.state.isSliderDragging) {
        _this.setState({
          isSliderDragging: null
        });
      }
    };

    _this.updatePosition = function (key, position) {
      var positions = (0, _utils.getPositionsFromValues)(_this.props.value, _this.props.min, _this.props.max, _this.getTrackClientRect());
      positions[key] = position;

      _this.updatePositions(positions);
    };

    _this.updatePositions = function (positions) {
      var values = {
        min: (0, _utils.getValueFromPosition)(positions.min, _this.props.min, _this.props.max, _this.getTrackClientRect()),
        max: (0, _utils.getValueFromPosition)(positions.max, _this.props.min, _this.props.max, _this.getTrackClientRect())
      };
      var transformedValues = {
        min: (0, _utils.getStepValueFromValue)(values.min, _this.props.step),
        max: (0, _utils.getStepValueFromValue)(values.max, _this.props.step)
      }; // console.log(transformedValues);

      _this.updateValues(transformedValues);
    };

    _this.updateValues = function (values) {
      if (!_this.shouldUpdate(values)) {
        return;
      }

      _this.props.onChange(values);
    };

    _this.shouldUpdate = function (values) {
      return _this.isWithinRange(values) && _this.hasStepDifference(values);
    };

    _this.isWithinRange = function (values) {
      return values.min >= _this.props.min && values.max <= _this.props.max &&
      /*this.props.allowSameValues
      ? values.min <= values.max
      :*/
      values.min < values.max;
    };

    _this.hasStepDifference = function (values) {
      var currentValues = _this.props.value;
      return (0, _utils.length)(values.min, currentValues.min) >= _this.props.step || (0, _utils.length)(values.max, currentValues.max) >= _this.props.step;
    };

    _this.props.setInteractionEndListener(_this.handleInteractionEnd);

    return _this;
  }

  var _proto = ValueSliderRange.prototype;

  _proto.render = function render() {
    var _this2 = this;

    var props = this.props,
        percentage = this.getValuePercentage();
    return React$createElement("div", {
      className: classnames(_ValueSliderModule.default.Container, _ValueSliderModule.default.MultipleRange, props.rippleEffect && _ValueSliderModule.default.WithRippleEffect, props.className),
      ref: this.props.bindElement,
      onMouseDown: this.props.onMouseDown,
      onTouchStart: this.props.onTouchStart
    }, React$createElement(_ValueSliderTrack.default, {
      value: percentage,
      ref: function ref($track) {
        _this2.track = $track;
      }
    }, React$createElement(_ValueSliderThumb.default, {
      className: classnames(this.state.isSliderDragging === "min" && _ValueSliderModule.default.Dragging),
      value: percentage.min,
      onDrag: function onDrag(e) {
        return _this2.handleThumbDrag(e, "min");
      }
    }), React$createElement(_ValueSliderThumb.default, {
      className: classnames(this.state.isSliderDragging === "max" && _ValueSliderModule.default.Dragging),
      value: percentage.max,
      onDrag: function onDrag(e) {
        return _this2.handleThumbDrag(e, "max");
      }
    })));
  };

  return ValueSliderRange;
}(_react.default.PureComponent), _temp)) || _class;

ValueSliderRange.propTypes = {
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  step: PropTypes.number,
  value: PropTypes.shape({
    min: PropTypes.number,
    max: PropTypes.number
  }).isRequired,
  onChange: PropTypes.func.isRequired,
  onChangeCompleted: PropTypes.func,
  disabled: PropTypes.bool,
  rippleEffect: PropTypes.bool,
  className: PropTypes.any
};
ValueSliderRange.defaultProps = {
  onChangeCompleted: function onChangeCompleted() {},
  step: 1,
  disabled: false,
  rippleEffect: false
};
var _default = ValueSliderRange;
exports.default = _default;
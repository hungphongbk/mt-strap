"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _objectWithoutPropertiesLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutPropertiesLoose"));

var _react = _interopRequireDefault(require("react"));

require("../_helpers/_bootstrap.scss");

var _ValueSliderRange = _interopRequireDefault(require("./ValueSliderRange"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var React$createElement = _react.default.createElement;
var types = {
  range: _ValueSliderRange.default
};

var ValueSlider = function ValueSlider(_ref) {
  var type = _ref.type,
      rest = (0, _objectWithoutPropertiesLoose2.default)(_ref, ["type"]);
  var Component = types[type];
  return React$createElement(Component, rest);
};

ValueSlider.propTypes = {
  /**
   * The minimum allowed value of the slider. Should not be equal to max.
   *
   * @type number
   * @required
   */
  min: PropTypes.number.isRequired,

  /**
   * The maximum allowed value of the slider. Should not be equal to min.
   *
   * @type number
   * @required
   */
  max: PropTypes.number.isRequired,

  /**
   * The granularity the slider can step through values.
   *
   * @type number
   * @default 1
   */
  step: PropTypes.number,

  /**
   * The value of the slider.
   */
  value: PropTypes.shape({
    min: PropTypes.number,
    max: PropTypes.number
  }).isRequired,

  /**
   * Callback function that is fired when the slider's value changed.
   *
   * @type function
   * @required
   */
  onChange: PropTypes.func.isRequired,

  /**
   * Callback function that is fired when the slider's value changed & slider thumb was released.
   *
   * @type function
   */
  onChangeCompleted: PropTypes.func,

  /**
   * If `true`, the slider will be disabled.
   *
   * @type boolean
   * @default false
   */
  disabled: PropTypes.bool,

  /**
   * @type boolean
   * @default false
   */
  rippleEffect: PropTypes.bool,

  /**
   * Direction of the slider (horizontal by default or vertical)
   *
   * @type ('horizontal'|'vertical')
   * @default 'horizontal'
   */
  direction: PropTypes.oneOf(["horizontal", "vertical"]),

  /**
   * @type any
   */
  className: PropTypes.any
};
ValueSlider.defaultProps = {
  direction: "horizontal"
};
var _default = ValueSlider;
exports.default = _default;
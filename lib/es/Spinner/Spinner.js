"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _react = _interopRequireWildcard(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

var _SpinnerModule = _interopRequireDefault(require("./Spinner.module.scss"));

var React$createElement = _react.default.createElement;

// import Holdable from "react-touch/lib/Holdable.react";
// import defineHold from "react-touch/lib/defineHold";
var HOLD_THRESHOLD = 700,
    HOLD_INTERVAL = 300,
    contextualClass = function contextualClass(bool) {
  return bool ? "mt-text-blue" : "mt-text-blueberry-light-20";
},
    toBool = function toBool(num) {
  return num !== 0;
};

var Spinner =
/*#__PURE__*/
function (_React$PureComponent) {
  (0, _inheritsLoose2.default)(Spinner, _React$PureComponent);

  function Spinner() {
    var _this;

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _React$PureComponent.call.apply(_React$PureComponent, [this].concat(args)) || this;
    _this.state = {
      holdDown: 0,
      holdUp: 0
    };
    _this.handlers = {
      holdDown: 0,
      holdUp: 0
    };
    _this.timeOuts = {
      holdUp: 0,
      holdDown: 0
    };

    _this.getUpDownAvailable = function () {
      /**
       * if `min` props is null, the component becomes has no lower boundary
       * and vice-versa to `max` props
       */
      var _this$props = _this.props,
          min = _this$props.min,
          max = _this$props.max,
          value = _this$props.value,
          canDown = min === null || value > min,
          canUp = max === null || value < max;
      return {
        canUp: canUp,
        canDown: canDown
      };
    };

    _this.handleHold = function (key, callback) {
      if (_this.state[key]) {
        _this.timeOuts[key] = setTimeout(function () {
          _this.handlers[key] = setInterval(callback, HOLD_INTERVAL);
        }, HOLD_THRESHOLD);
      } else {
        clearTimeout(_this.timeOuts[key]);
        clearInterval(_this.handlers[key]);
      }
    };

    _this.handleHoldWithKey = function (key, multiplier) {
      return _this.handleHold(key, function () {
        return _this.enter(key)(function () {
          var value = Math.floor(_this.props.value + (1 + Math.pow(_this.state[key] * 1.0 / 5, 2)) * multiplier);

          _this.props.onChange(value);
        });
      });
    };

    _this.enter = function (key) {
      return function (callback) {
        if (callback === void 0) {
          callback = function callback() {};
        }

        return _this.setState(function (state) {
          var _ref;

          return _ref = {}, _ref[key] = state[key] + 1, _ref;
        }, callback);
      };
    };

    _this.leave = function (key) {
      return function (callback) {
        var _this$setState;

        if (callback === void 0) {
          callback = function callback() {};
        }

        return _this.setState((_this$setState = {}, _this$setState[key] = 0, _this$setState), callback);
      };
    };

    _this.holdEventHandlers = function (key) {
      return {
        onMouseDown: function onMouseDown() {
          return _this.enter(key)();
        },
        onMouseUp: function onMouseUp() {
          return _this.leave(key)();
        },
        onMouseLeave: function onMouseLeave() {
          return _this.leave(key)();
        }
      };
    };

    return _this;
  }

  var _proto = Spinner.prototype;

  _proto.componentDidMount = function componentDidMount() {};

  _proto.componentDidUpdate = function componentDidUpdate(prevProps, prevState, snapshot) {
    if (toBool(this.state.holdDown) !== toBool(prevState.holdDown)) this.handleHoldWithKey("holdDown", -1);
    if (toBool(this.state.holdUp) !== toBool(prevState.holdUp)) this.handleHoldWithKey("holdUp", 1);
  };

  _proto.render = function render() {
    var props = this.props,
        _this$getUpDownAvaila = this.getUpDownAvailable(),
        canUp = _this$getUpDownAvaila.canUp,
        canDown = _this$getUpDownAvaila.canDown;

    return React$createElement("div", {
      className: (0, _classnames.default)(_SpinnerModule.default.Spinner, props.className)
    }, React$createElement("div", null, React$createElement("span", this.holdEventHandlers("holdDown"), React$createElement("i", {
      onMouseDown: function onMouseDown() {
        return props.onChange(props.value - 1);
      },
      className: (0, _classnames.default)("li li-circle-minus mt-pr-2", contextualClass(canDown))
    })), React$createElement("input", {
      className: _SpinnerModule.default.InputArea,
      type: "text",
      value: props.value
    }), React$createElement("span", this.holdEventHandlers("holdUp"), React$createElement("i", {
      onMouseDown: function onMouseDown() {
        return props.onChange(props.value + 1);
      },
      className: (0, _classnames.default)("li li-plus-circle mt-pl-2", contextualClass(canUp))
    }))));
  };

  return Spinner;
}(_react.default.PureComponent);

Spinner.propTypes = {
  /**
   * The value of the spinner
   *
   * @type number
   * @required
   */
  value: PropTypes.number.isRequired,

  /**
   * Callback function that is fired when the spinner's value changed.
   *
   * @type function
   * @required
   */
  onChange: PropTypes.func.isRequired,

  /**
   * The minimum allowed value of the spinner. Should not be equal to max.
   *
   * @type number
   * @default null
   */
  min: PropTypes.number,

  /**
   * The maximum allowed value of the spinner. Should not be equal to min.
   * @type number
   * @default null
   */
  max: PropTypes.number,
  className: PropTypes.any
};
Spinner.defaultProps = {
  min: null,
  max: null
};
var _default = Spinner;
exports.default = _default;
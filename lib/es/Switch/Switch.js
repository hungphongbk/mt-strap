"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutPropertiesLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutPropertiesLoose"));

var _react = _interopRequireDefault(require("react"));

require("../_helpers/_bootstrap.scss");

require("./Switch.scss");

var PropTypes = _interopRequireWildcard(require("prop-types"));

var classnames = _interopRequireWildcard(require("classnames"));

var React$createElement = _react.default.createElement;

var Switch = function Switch(_ref) {
  var value = _ref.value,
      disabled = _ref.disabled,
      children = _ref.children,
      className = _ref.className,
      onChange = _ref.onChange,
      other = (0, _objectWithoutPropertiesLoose2.default)(_ref, ["value", "disabled", "children", "className", "onChange"]);
  return React$createElement("label", (0, _extends2.default)({
    className: classnames("mt-switch", className)
  }, other), React$createElement("input", {
    type: "checkbox",
    checked: value,
    disabled: disabled
  }), React$createElement("span", {
    className: "mt-switch-slider",
    onClick: function onClick() {
      return onChange(!value);
    }
  }, React$createElement("span", {
    className: "mt-switch-slider-thumb"
  })), React$createElement("span", {
    className: "mt-switch-title"
  }, children));
};

Switch.propTypes = {
  /**
   * If true, the component is checked.
   *
   * @type bool
   * @required
   */
  value: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,

  /**
   * If true, the switch will be disabled.
   *
   * @type bool
   * @default false
   */
  disabled: PropTypes.bool,
  children: PropTypes.any,
  className: PropTypes.any
};
Switch.defaultProps = {
  disabled: false
};
var _default = Switch;
exports.default = _default;
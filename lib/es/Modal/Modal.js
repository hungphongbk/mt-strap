"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutPropertiesLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutPropertiesLoose"));

var _react = _interopRequireDefault(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

require("../../sass/components/_modals.scss");

var _PortalCompat = _interopRequireDefault(require("react-portal/lib/PortalCompat"));

var _uniqueId = _interopRequireDefault(require("../_helpers/uniqueId"));

var _reactClickOutside = require("../_helpers/react-click-outside");

var _ModalHeader = _interopRequireDefault(require("../ModalHeader/ModalHeader"));

var _utils = require("../_helpers/utils");

var _reactTransitionGroup = require("react-transition-group");

var _transitions = require("../_helpers/styles/transitions");

var _ModalModule = _interopRequireDefault(require("./Modal.module.scss"));

var React$createElement = _react.default.createElement;

function createElement(id) {
  var node = document.createElement("div");
  [["id", id], ["role", "presentation"]].forEach(function (_ref) {
    var attr = _ref[0],
        val = _ref[1];
    node.setAttribute(attr, val);
  });
  return node;
}

function Modal(_ref2) {
  var isOpen = _ref2.isOpen,
      toggle = _ref2.toggle,
      size = _ref2.size,
      position = _ref2.position,
      disablePortal = _ref2.disablePortal,
      disableBackdropClick = _ref2.disableBackdropClick,
      closeButton = _ref2.closeButton,
      className = _ref2.className,
      children = _ref2.children,
      disableCloseOnEsc = _ref2.disableCloseOnEsc,
      animation = _ref2.animation,
      others = (0, _objectWithoutPropertiesLoose2.default)(_ref2, ["isOpen", "toggle", "size", "position", "disablePortal", "disableBackdropClick", "closeButton", "className", "children", "disableCloseOnEsc", "animation"]);

  var id = _react.default.useRef((0, _uniqueId.default)(5, "modal-")),
      portalDom = _react.default.useRef(createElement(id.current + "-bg")),
      refClickOutside = (0, _reactClickOutside.useClickOutside)(_react.default.useCallback(function () {
    if (!disableBackdropClick) // eslint-disable-next-line no-unused-expressions
      toggle === null || toggle === void 0 ? void 0 : toggle(false);
  }, [isOpen]));

  _react.default.useEffect(function () {
    if (!disablePortal) document.body.appendChild(portalDom.current);
    return function () {
      if (!disablePortal) // eslint-disable-next-line react-hooks/exhaustive-deps
        portalDom.current.remove();
    };
  }, []);

  _react.default.useEffect(function () {
    if (!animation) {
      if (isOpen) portalDom.current.classList.add("mt-modal-bg");else portalDom.current.classList.remove("mt-modal-bg");
    }
  }, [isOpen, animation]);

  var newChildren = _react.default.Children.map(children, function (child) {
    if (child.type === _ModalHeader.default) {
      var newProps = {
        _modalId: id.current + "-title"
      };
      if (closeButton) newProps.children = [child.props.children, React$createElement("i", {
        key: "close-button",
        className: "li-cross2 mt-modal-close-icon",
        onClick: function onClick() {
          return toggle(false);
        }
      })];
      return _react.default.cloneElement(child, newProps);
    }

    return child;
  });

  var modal = React$createElement("div", (0, _extends2.default)({
    tabIndex: 1,
    role: "dialog",
    id: id.current,
    "aria-modal": true,
    className: (0, _classnames.default)("mt-modal mt-modal-" + size + " mt-modal-position-" + position, className),
    ref: refClickOutside,
    onKeyDown: function onKeyDown(e) {
      return e.keyCode === 27 && !disableCloseOnEsc && toggle(false);
    }
  }, others), React$createElement(_utils.ContextModal.Provider, {
    value: {
      inModal: true
    }
  }, newChildren));
  if (disablePortal) return isOpen ? modal : null;
  if (!animation) return React$createElement(_PortalCompat.default, {
    node: portalDom.current
  }, isOpen ? React$createElement("div", {
    className: "mt-modal-bg-overlay",
    ref: function ref(el) {
      return el === null || el === void 0 ? void 0 : el.firstChild.focus();
    }
  }, modal) : null);
  return React$createElement(_PortalCompat.default, {
    node: portalDom.current
  }, React$createElement(_reactTransitionGroup.Transition, {
    in: isOpen,
    timeout: _transitions.duration.complex,
    mountOnEnter: true,
    unmountOnExit: true
  }, function (state) {
    return React$createElement("div", {
      className: (0, _classnames.default)("mt-modal-bg", _ModalModule.default.Overlay, _ModalModule.default[state])
    }, React$createElement("div", {
      className: (0, _classnames.default)("mt-modal-bg-overlay"),
      ref: function ref(el) {
        return el === null || el === void 0 ? void 0 : el.firstChild.focus();
      }
    }, React$createElement("div", {
      className: _ModalModule.default.Modal
    }, modal)));
  }));
}

Modal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func,
  size: PropTypes.oneOf(["md", "lg"]),
  position: PropTypes.oneOf(["center", "top"]),
  disablePortal: PropTypes.bool,
  disableBackdropClick: PropTypes.bool,
  disableCloseOnEsc: PropTypes.bool,
  closeButton: PropTypes.bool,
  animation: PropTypes.oneOfType([PropTypes.bool, PropTypes.number, PropTypes.shape({
    enter: PropTypes.number,
    exit: PropTypes.number
  })])
};
Modal.defaultProps = {
  size: "md",
  position: "center",
  disablePortal: false,
  disableBackdropClick: false,
  disableCloseOnEsc: false,
  closeButton: true,
  animation: false
};
var _default = Modal;
exports.default = _default;
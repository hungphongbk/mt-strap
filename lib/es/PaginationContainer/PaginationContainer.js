"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = exports.withPaginationContainer = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _react = _interopRequireDefault(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames3 = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

var _PaginationContainerModule = _interopRequireDefault(require("./PaginationContainer.module.scss"));

var _hoistNonReactStatics = _interopRequireDefault(require("hoist-non-react-statics"));

var _setDisplayName = require("../_helpers/setDisplayName");

var React$createElement = _react.default.createElement;

var withPaginationContainer = function withPaginationContainer(options) {
  return function (Component) {
    var PaginationContainerBase =
    /*#__PURE__*/
    function (_React$Component) {
      (0, _inheritsLoose2.default)(PaginationContainerBase, _React$Component);

      function PaginationContainerBase() {
        var _this;

        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        _this = _React$Component.call.apply(_React$Component, [this].concat(args)) || this;

        _this.getOptions = function () {
          if (typeof options === "function") return options(_this.props);else return options;
        };

        _this.getPreviousThumbClasses = function () {
          var _classnames;

          return (0, _classnames3.default)(_PaginationContainerModule.default.PaginationThumb, (_classnames = {}, _classnames[_PaginationContainerModule.default.Rounded] = _this.getOptions().previousLabel.length === 0, _classnames));
        };

        _this.getNextThumbClasses = function () {
          var _classnames2;

          return (0, _classnames3.default)(_PaginationContainerModule.default.PaginationThumb, (_classnames2 = {}, _classnames2[_PaginationContainerModule.default.Rounded] = _this.getOptions().nextLabel.length === 0, _classnames2));
        };

        _this.renderPreviousThumb = function () {
          return React$createElement("a", {
            className: _this.getPreviousThumbClasses(),
            role: "button"
          }, _this.getOptions().arrow && React$createElement("i", {
            className: "li li-arrow-left"
          }), _this.getOptions().previousLabel);
        };

        _this.renderNextThumb = function () {
          return React$createElement("a", {
            className: _this.getNextThumbClasses(),
            role: "button"
          }, _this.getOptions().arrow && React$createElement("i", {
            className: "li li-arrow-right"
          }), _this.getOptions().nextLabel);
        };

        return _this;
      }

      var _proto = PaginationContainerBase.prototype;

      _proto.render = function render() {
        var paginationProps = {
          renderNextThumb: this.renderNextThumb(),
          renderPreviousThumb: this.renderPreviousThumb()
        };
        return React$createElement(Component, (0, _extends2.default)({}, this.props, {
          paginationProps: paginationProps
        }));
      };

      return PaginationContainerBase;
    }(_react.default.Component);

    (0, _hoistNonReactStatics.default)(PaginationContainerBase, Component);
    (0, _setDisplayName.setDisplayName)(PaginationContainerBase, "withPaginationContainer", Component);
    return PaginationContainerBase;
  };
};

exports.withPaginationContainer = withPaginationContainer;
var PaginationContainer = withPaginationContainer(function (props) {
  return props;
})(function (props) {
  return props.children(props.paginationProps);
});
var _default = PaginationContainer;
exports.default = _default;
PaginationContainer.propTypes = {
  /**
   * The total number of pages.
   * @required
   * @type number
   */
  pageCount: PropTypes.number.isRequired,

  /**
   * The method to call when a page is clicked. Exposes the current page object as an argument.
   * @type function
   */
  onPageChange: PropTypes.func,

  /**
   * The initial page selected.
   *
   * @type number
   * @default 0
   */
  initialPage: PropTypes.number,

  /**
   * @type string
   * @default ''
   */
  previousLabel: PropTypes.string,

  /**
   * @type string
   * @default ''
   */
  nextLabel: PropTypes.string,

  /**
   * @type boolean
   * @default true
   */
  arrow: PropTypes.bool,

  /**
   * @type function
   * @required
   */
  children: PropTypes.func.isRequired
};
PaginationContainer.defaultProps = {
  previousLabel: "",
  nextLabel: "",
  arrow: true,
  onPageChange: function onPageChange() {}
};
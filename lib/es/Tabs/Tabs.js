"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _react = _interopRequireWildcard(require("react"));

require("../_helpers/_bootstrap.scss");

require("../../sass/components/_tabs.scss");

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _uniqueId = _interopRequireDefault(require("../_helpers/uniqueId"));

var React$createElement = _react.default.createElement;

var Tabs =
/*#__PURE__*/
function (_PureComponent) {
  (0, _inheritsLoose2.default)(Tabs, _PureComponent);

  function Tabs() {
    var _this;

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _PureComponent.call.apply(_PureComponent, [this].concat(args)) || this;
    _this.state = {
      id: (0, _uniqueId.default)(5, "tab-"),
      initialized: false,
      activeKey: "",
      outerWidth: 0,
      innerWidth: 0,
      overflow: false,
      tabKeys: [],
      tabs: {}
    };

    _this.selectTab = function (tabKey) {
      return _this.setState({
        activeKey: tabKey
      }, function () {
        return _this.props.onChange(tabKey);
      });
    };

    _this.changeTabPosition = function () {};

    _this.setTabWrapper = function (el) {
      return el !== null && _this.setState({
        innerWidth: el.scrollWidth,
        outerWidth: el.clientWidth,
        overflow: el.scrollWidth > el.clientWidth
      });
    };

    _this.setTabElement = function (tabKey, el) {
      return el && !_this.state.tabs[tabKey] && _this.setState(function (state) {
        var _Object$assign;

        return {
          tabs: Object.assign({}, state.tabs, (_Object$assign = {}, _Object$assign[tabKey] = el, _Object$assign))
        };
      });
    };

    return _this;
  }

  Tabs.getDerivedStateFromProps = function getDerivedStateFromProps(props, state) {
    var newState = {},
        assign = function assign(obj) {
      return Object.assign(newState, obj);
    };

    if (state.tabKeys.length === 0) assign({
      tabKeys: _react.default.Children.map(props.children, function (c) {
        return c.props.tabKey;
      })
    });
    if (!props.activeKey && !state.initialized) assign({
      initialized: true,
      activeKey: (newState.tabKeys || state.tabKeys)[0]
    });
    if (props.activeKey && props.activeKey !== state.activeKey) assign({
      activeKey: props.activeKey
    });
    return newState;
  };

  var _proto = Tabs.prototype;

  _proto.render = function render() {
    var _this2 = this;

    var tabItemStyle = {};
    if (this.props.justify) tabItemStyle.width = 100.0 / _react.default.Children.count(this.props.children) + "%";
    return React$createElement("div", {
      className: this.classnames
    }, React$createElement("div", {
      className: "mt-tab-body",
      ref: this.setTabWrapper
    }, React$createElement("div", {
      role: "tablist"
    }, _react.default.Children.map(this.props.children, function (Tab, index) {
      return React$createElement("div", {
        role: "tab",
        id: _this2.state.id + Tab.props.tabKey,
        ref: function ref(el) {
          return _this2.setTabElement(Tab.props.tabKey, el);
        },
        key: Tab.props.tabKey,
        style: tabItemStyle,
        className: (0, _classnames.default)("mt-tab-item", Tab.props.tabKey === _this2.state.activeKey && "mt-tab-active"),
        onClickCapture: function onClickCapture() {
          return _this2.selectTab(Tab.props.tabKey);
        }
      }, React$createElement("a", {
        className: "mt-tab-link",
        href: "#",
        onClick: function onClick(e) {
          return e.preventDefault();
        }
      }, Tab.props.title));
    }), this.state.overflow && [React$createElement("div", {
      key: "__tab-nav-left",
      className: "mt-tab-arrow mt-tab-arrow-left"
    }, React$createElement("li", {
      className: "li-chevron-left mt-tab-icon"
    })), React$createElement("div", {
      key: "__tab-nav-right",
      className: "mt-tab-arrow mt-tab-arrow-right"
    }, React$createElement("li", {
      className: "li-chevron-right mt-tab-icon"
    }))])), React$createElement("div", {
      className: "mt-tab-content"
    }, _react.default.Children.map(this.props.children, function (Tab) {
      return _react.default.cloneElement(Tab, {
        role: "tabpanel",
        "aria-labelledby": _this2.state.id + Tab.props.tabKey,
        key: Tab.props.tabKey,
        isActive: Tab.props.tabKey === _this2.state.activeKey
      });
    })));
  };

  (0, _createClass2.default)(Tabs, [{
    key: "classnames",
    get: function get() {
      var classes = ["mt-tab mt-tab-style mt-tab-size"];
      if (this.props.border) classes.push("mt-tab-border");
      return (0, _classnames.default)(classes);
    }
  }]);
  return Tabs;
}(_react.PureComponent);

exports.default = Tabs;
Tabs.defaultProps = {
  border: true,
  activeKey: null,
  onChange: function onChange() {},
  justify: false
};
Tabs.propTypes = {
  /**
   * This component requires an array of Tab components
   *
   * @required
   * @type Tab[]
   */
  children: PropTypes.arrayOf(PropTypes.element).isRequired,

  /**
   * Make a border around Tabs becomes available
   *
   * @type boolean
   * @default true
   */
  border: PropTypes.bool,
  activeKey: PropTypes.string,
  onChange: PropTypes.func,
  justify: PropTypes.bool
};
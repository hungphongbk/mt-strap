"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

exports.__esModule = true;
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

var _PaginationModule = _interopRequireDefault(require("./Pagination.module.scss"));

var _reactPaginate = _interopRequireDefault(require("react-paginate"));

var React$createElement = _react.default.createElement;

function Pagination(props) {
  return React$createElement(_reactPaginate.default, {
    previousLabel: React$createElement("i", {
      className: "li li-arrow-left"
    }),
    nextLabel: React$createElement("i", {
      className: "li li-arrow-right"
    }),
    breakLabel: "...",
    breakClassName: _PaginationModule.default.BreakMe,
    pageCount: props.pageCount,
    marginPagesDisplayed: 2,
    pageRangeDisplayed: 5,
    containerClassName: _PaginationModule.default.Pagination,
    subContainerClassName: "pages pagination",
    activeClassName: _PaginationModule.default.Active,
    previousClassName: _PaginationModule.default.Previous,
    nextClassName: _PaginationModule.default.Next,
    onPageChange: props.onPageChange
  });
}

Pagination.propTypes = {
  /**
   * The total number of pages.
   * @required
   * @type number
   */
  pageCount: PropTypes.number.isRequired,

  /**
   * The method to call when a page is clicked. Exposes the current page object as an argument.
   * @type function
   */
  onPageChange: PropTypes.func,

  /**
   * The initial page selected.
   *
   * @type number
   * @default 0
   */
  initialPage: PropTypes.number
};
Pagination.defaultProps = {
  onPageChange: function onPageChange() {}
};
var _default = Pagination;
exports.default = _default;
"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutPropertiesLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutPropertiesLoose"));

var _react = _interopRequireWildcard(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

require("../../sass/components/_cards.scss");

var React$createElement = _react.default.createElement;

var CardBody = function CardBody(props) {
  var className = props.className,
      children = props.children,
      other = (0, _objectWithoutPropertiesLoose2.default)(props, ["className", "children"]);
  return React$createElement("div", (0, _extends2.default)({
    className: (0, _classnames.default)("mt-card-body", className)
  }, other), children);
};

CardBody.propTypes = {};
CardBody.defaultProps = {};
var _default = CardBody;
exports.default = _default;
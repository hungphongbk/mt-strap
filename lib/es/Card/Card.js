"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutPropertiesLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutPropertiesLoose"));

var _react = _interopRequireDefault(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

require("../../sass/components/_cards.scss");

var React$createElement = _react.default.createElement;

var Card = function Card(props) {
  var Component = props.component,
      className = props.className,
      children = props.children,
      other = (0, _objectWithoutPropertiesLoose2.default)(props, ["component", "className", "children"]);
  return React$createElement(Component, (0, _extends2.default)({
    className: (0, _classnames.default)("mt-card mt-card-style mt-card-default", className)
  }, other), children);
};

Card.propTypes = {
  component: PropTypes.elementType
};
Card.defaultProps = {
  component: "div"
};
var _default = Card;
exports.default = _default;
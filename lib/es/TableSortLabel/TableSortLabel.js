"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _react = _interopRequireDefault(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

var _TableSortLabelModule = _interopRequireDefault(require("./TableSortLabel.module.scss"));

var React$createElement = _react.default.createElement;
var nextValue = {
  none: "asc",
  asc: "desc",
  desc: "none"
};

var TableSortLabel =
/*#__PURE__*/
function (_React$PureComponent) {
  (0, _inheritsLoose2.default)(TableSortLabel, _React$PureComponent);

  function TableSortLabel() {
    var _this;

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _React$PureComponent.call.apply(_React$PureComponent, [this].concat(args)) || this;
    _this.th = null;

    _this.onChange = function () {
      return _this.props.onChange(nextValue[_this.props.sortValue]);
    };

    _this.attachEventHandler = function (el) {
      if (!el) return;
      var th = el.parentElement;
      th.addEventListener("click", _this.onChange);

      if (!th.classList.contains(_TableSortLabelModule.default.SortLabelTh)) {
        th.classList.add(_TableSortLabelModule.default.SortLabelTh);
      }

      _this.th = th;
    };

    return _this;
  }

  var _proto = TableSortLabel.prototype;

  _proto.componentWillUnmount = function componentWillUnmount() {
    var th = this.th;
    th.removeEventListener("click", this.onChange);
    if (th.classList.contains(_TableSortLabelModule.default.SortLabelTh)) th.classList.remove(_TableSortLabelModule.default.SortLabelTh);
  };

  _proto.render = function render() {
    var props = this.props;

    if (!props.sortValue) {
      props.onChange("none");
      return null;
    }

    return React$createElement("span", {
      ref: this.attachEventHandler,
      className: (0, _classnames.default)(_TableSortLabelModule.default.SortLabel, "sort-label-" + props.sortValue)
    }, React$createElement("i", {
      className: "li li-chevrons-expand-vertical"
    }));
  };

  return TableSortLabel;
}(_react.default.PureComponent);

TableSortLabel.propTypes = {
  sortValue: PropTypes.oneOf(["none", "asc", "desc"]).isRequired,
  onChange: PropTypes.func.isRequired
};
var _default = TableSortLabel;
exports.default = _default;
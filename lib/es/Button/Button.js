"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _react = _interopRequireWildcard(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

require("../_helpers/_bootstrap.scss");

require("../../sass/components/_buttons.scss");

var _classnames = _interopRequireDefault(require("classnames"));

var _utils = require("../_helpers/utils");

var React$createElement = _react.default.createElement;

var buttonClassname = function buttonClassname(props, inModal) {
  var classes = ["mt-btn", "mt-size-" + props.size];
  if (/outlined/.test(props.shape) && props.variant !== "white") classes.push("mt-btn-outline-" + props.variant);else classes.push("mt-btn-" + props.variant); // if is rounded

  /round/.test(props.shape) && classes.push("mt-btn-rounded"); // if is disabled

  props.disabled && classes.push("disabled");
  inModal && classes.push("mt-modal-btn-style");
  classes.push(props.className || null);
  return (0, _classnames.default)(classes);
};

var Button = function Button(props) {
  var children = props.isLoading ? React$createElement("div", {
    className: "lds-spinner"
  }, new Array(12).fill(0).map(function (_, i) {
    return React$createElement("div", {
      key: i
    });
  })) : props.children;

  var _React$useContext = _react.default.useContext(_utils.ContextModal),
      inModal = _React$useContext.inModal;

  var eventProps = {};

  for (var prop in props) {
    if (props.hasOwnProperty(prop) && /^on/.test(prop)) eventProps[prop] = props[prop];
  }

  return React$createElement("button", (0, _extends2.default)({
    className: buttonClassname(props, inModal)
  }, eventProps), children);
};

Button.propTypes = {
  /**
   * Specify button border style
   *
   * @type ('normal'|'outlined'|'round'|'round-outlined')
   * @default 'normal'
   */
  shape: PropTypes.oneOf(["normal", "outlined", "round", "round-outlined"]),

  /**
   * One or more button variant combinations
   * @type ('blue'|'green'|'white'|'pink'|'red'|'orange')
   * @required
   */
  variant: PropTypes.oneOf(["blue", "green", "white", "pink", "red", "orange", "text-gray"]).isRequired,

  /**
   * Specifies a large or smaller or smallest button.
   *
   * @type ('lg'|'md'|'sm'|'xs')
   * @default 'md'
   */
  size: PropTypes.oneOf(["lg", "md", "sm"]),

  /**
   * Disables the Button, preventing mouse events, even if the underlying component is an <a> element
   * @type boolean
   * @default false
   */
  disabled: PropTypes.bool,

  /**
   * Toggle the loading spinner indicator
   * @type boolean
   * @default false
   */
  isLoading: PropTypes.bool
};
Button.defaultProps = {
  shape: "normal",
  size: "md",
  disabled: false,
  isLoading: false
};
var _default = Button;
exports.default = _default;
"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _react = _interopRequireWildcard(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

var _StepperModule = _interopRequireDefault(require("./Stepper.module.scss"));

var React$createElement = _react.default.createElement;

var Stepper =
/*#__PURE__*/
function (_Component) {
  (0, _inheritsLoose2.default)(Stepper, _Component);

  function Stepper() {
    return _Component.apply(this, arguments) || this;
  }

  var _proto = Stepper.prototype;

  // constructor(props) {
  //     super(props);
  //     this.state = {};
  // }
  _proto.render = function render() {
    return React$createElement("div", {
      className: _StepperModule.default.Container
    });
  };

  return Stepper;
}(_react.Component);

exports.default = Stepper;
Stepper.propTypes = {};
Stepper.defaultProps = {};
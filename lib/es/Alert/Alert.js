"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

exports.__esModule = true;
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

require("../_helpers/_bootstrap.scss");

require("../../sass/components/_alert.scss");

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var React$createElement = _react.default.createElement;

// import { connect } from "react-redux";
// import { bindActionCreators } from "redux";
// import * as AlertActions from "../../store/Alert/actions";
var Alert = function Alert(props) {
  var classes = ["mt-alert", "mt-alert-" + props.variant];
  props.withIcon && classes.push("mt-alert-with-icon");
  var htmlRendered = React$createElement("div", {
    className: (0, _classnames.default)(classes)
  }, props.withIcon && React$createElement("i", {
    className: "li li-" + props.withIcon
  }), props.children, props.dismissible && React$createElement("i", {
    className: "mt-alert-close li-cross",
    onClick: function onClick() {
      return props.onClose();
    }
  }));
  return props.dismissible ? props.show && htmlRendered : htmlRendered;
};

Alert.propTypes = {
  /**
   * The Alert visual variant
   *
   * @type ('success'|'danger'|'warning'|'info')
   * @required
   */
  variant: PropTypes.oneOf(["success", "info", "warning", "danger"]).isRequired,
  withIcon: PropTypes.string,
  dismissible: PropTypes.bool,
  show: PropTypes.bool,
  onClose: PropTypes.func
};
Alert.defaultProps = {
  withIcon: null
};
var _default = Alert;
exports.default = _default;
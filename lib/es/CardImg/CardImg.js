"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutPropertiesLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutPropertiesLoose"));

var _react = _interopRequireDefault(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

var _Image = _interopRequireDefault(require("../Image/Image"));

var React$createElement = _react.default.createElement;

var CardImg = function CardImg(props) {
  var children = props.children,
      className = props.className,
      src = props.src,
      _props$alt = props.alt,
      alt = _props$alt === void 0 ? "" : _props$alt,
      ratio = props.ratio,
      others = (0, _objectWithoutPropertiesLoose2.default)(props, ["children", "className", "src", "alt", "ratio"]),
      classes = (0, _classnames.default)("mt-card-img", className);
  if (children !== null) return React$createElement("div", (0, _extends2.default)({
    className: classes
  }, others), React$createElement(_Image.default, {
    src: src,
    alt: alt,
    ratio: ratio
  }), React$createElement("div", {
    className: "mt-card-img-overlay"
  }), children);else return React$createElement(_Image.default, {
    className: classes,
    src: src,
    alt: alt,
    ratio: ratio
  });
};

CardImg.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string,
  children: PropTypes.oneOf([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]),
  className: PropTypes.any,
  ratio: PropTypes.string
};
CardImg.defaultProps = {
  alt: "",
  children: null,
  ratio: null
};
var _default = CardImg;
exports.default = _default;
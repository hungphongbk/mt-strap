"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _react = _interopRequireWildcard(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _SliderModule = _interopRequireDefault(require("./Slider.module.scss"));

require("../_helpers/_bootstrap.scss");

var _domTraverse = _interopRequireDefault(require("../_helpers/domTraverse"));

var _imagesloaded = _interopRequireDefault(require("imagesloaded"));

var React$createElement = _react.default.createElement;

var Slider =
/*#__PURE__*/
function (_Component) {
  (0, _inheritsLoose2.default)(Slider, _Component);

  function Slider(props) {
    var _this;

    _this = _Component.call(this, props) || this;
    _this.state = {
      idx: 0,
      sliding: false,
      initialized: false,
      style: {}
    };
    _this.firstSlide = null;

    _this.initialize = function () {
      return (0, _imagesloaded.default)(_this.firstSlide, function () {
        return _this.setState({
          initialized: true,
          style: {
            width: _this.firstSlide.clientWidth + "px",
            height: _this.firstSlide.clientHeight + "px"
          }
        });
      });
    };

    _this.getImageUrl = function (idx) {
      return new Promise(function (resolve) {
        _react.default.Children.forEach(_this.props.children, function (child, i) {
          return i === idx && (0, _domTraverse.default)(child, function (node) {
            return node.type === "img" && console.log(node);
          });
        });
      });
    };

    _this.getSlideWrapperTransition = function () {
      var styles = function styles(w) {
        return {
          transition: "transform " + _this.props.slideDuration + "s ease-in-out",
          transform: "translateX(-" + w + "px)"
        };
      };

      if (!_this.state.initialized) return styles(0);

      var w = 1 * _this.state.style.width.replace(/px/, "");

      return styles(w * _this.state.idx);
    };

    return _this;
  }

  var _proto = Slider.prototype;

  _proto.componentWillMount = function componentWillMount() {
    this.getImageUrl(0);
  };

  _proto.render = function render() {
    var _this2 = this;

    return React$createElement("div", {
      className: (0, _classnames.default)(_SliderModule.default.Container, this.state.initialized && _SliderModule.default.Initialized),
      style: this.state.style
    }, React$createElement("div", {
      className: _SliderModule.default.Wrapper,
      style: this.getSlideWrapperTransition()
    }, _react.default.Children.map(this.props.children, function (child, i) {
      return _react.default.cloneElement(child, {
        key: i,
        ref: function ref(el) {
          if (el && !_this2.firstSlide) {
            _this2.firstSlide = el;

            _this2.initialize();
          }
        },
        style: _this2.state.style
      });
    })), React$createElement("div", {
      className: _SliderModule.default.NextButton
    }), React$createElement("div", {
      className: _SliderModule.default.PrevButton
    }));
  };

  return Slider;
}(_react.Component);

exports.default = Slider;
Slider.propTypes = {
  children: PropTypes.element.isRequired,
  slideDuration: PropTypes.number,
  onInitialized: PropTypes.func
};
Slider.defaultProps = {
  slideDuration: 0.5,
  onInitialized: function onInitialized() {}
};
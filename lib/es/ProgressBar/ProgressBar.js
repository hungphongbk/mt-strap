"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _react = _interopRequireWildcard(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

require("../_helpers/_bootstrap.scss");

require("./ProgressBar.scss");

var React$createElement = _react.default.createElement;

var ProgressBar =
/*#__PURE__*/
function (_Component) {
  (0, _inheritsLoose2.default)(ProgressBar, _Component);

  function ProgressBar() {
    return _Component.apply(this, arguments) || this;
  }

  var _proto = ProgressBar.prototype;

  // constructor(props) {
  //     super(props);
  //     this.state = {};
  // }
  _proto.render = function render() {
    return React$createElement("div", {
      className: ""
    });
  };

  return ProgressBar;
}(_react.Component);

exports.default = ProgressBar;
ProgressBar.propTypes = {};
ProgressBar.defaultProps = {};
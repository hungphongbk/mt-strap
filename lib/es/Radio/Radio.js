"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _CheckboxRadioBase = _interopRequireDefault(require("../CheckboxRadioBase/CheckboxRadioBase"));

var _withProps = _interopRequireDefault(require("../_helpers/withProps"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var Radio = (0, _withProps.default)({
  type: "radio"
})(_CheckboxRadioBase.default);
Radio.propTypes = {
  /**
   * Specifies a large or default checkbox.
   *
   * @type ('default'|'lg')
   * @default 'default'
   */
  size: PropTypes.oneOf(["default", "lg"]),

  /**
   * Disables the checkbox, preventing mouse events, even if the underlying component is an <a> element
   * @type boolean
   * @default false
   */
  disabled: PropTypes.bool,
  checked: PropTypes.bool.isRequired,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  name: PropTypes.string,
  inline: PropTypes.bool
};
Radio.defaultProps = {
  size: "default",
  disabled: false,
  inline: false
};
var _default = Radio;
exports.default = _default;
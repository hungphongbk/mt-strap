"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutPropertiesLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutPropertiesLoose"));

var _react = _interopRequireDefault(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

require("../../sass/components/_checkboxes-radios.scss");

var React$createElement = _react.default.createElement;

var checkboxClassnames = function checkboxClassnames(props) {
  var classes = ["mt-checkbox-radio", "mt-checkbox-radio-" + props.size];
  props.disabled && classes.push("disabled");
  props.inline && classes.push("mt-checkbox-radio-inline");
  classes.push(props.className || null);
  return (0, _classnames.default)(classes);
};

var CheckboxRadioBase = function CheckboxRadioBase(props) {
  var type = props.type,
      size = props.size,
      className = props.className,
      children = props.children,
      _onChange = props.onChange,
      other = (0, _objectWithoutPropertiesLoose2.default)(props, ["type", "size", "className", "children", "onChange"]);
  return React$createElement("label", {
    className: checkboxClassnames(props),
    disabled: props.disabled
  }, React$createElement("input", (0, _extends2.default)({
    type: type
  }, other, {
    onChange: function onChange(e) {
      return _onChange(e.target.value);
    }
  })), React$createElement("span", {
    className: "li-checkmark li-checkmark-" + type
  }), children);
};

CheckboxRadioBase.propTypes = {
  type: PropTypes.oneOf(["checkbox", "radio"]).isRequired,

  /**
   * Specifies a large or default checkbox.
   *
   * @type ('default'|'lg')
   * @default 'default'
   */
  size: PropTypes.oneOf(["default", "lg"]),

  /**
   * Disables the checkbox, preventing mouse events, even if the underlying component is an <a> element
   * @type boolean
   * @default false
   */
  disabled: PropTypes.bool,
  checked: PropTypes.bool.isRequired,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  name: PropTypes.string,
  inline: PropTypes.bool
};
CheckboxRadioBase.defaultProps = {
  size: "default",
  disabled: false,
  inline: false
};
var _default = CheckboxRadioBase;
exports.default = _default;
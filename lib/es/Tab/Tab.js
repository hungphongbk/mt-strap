"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutPropertiesLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutPropertiesLoose"));

var _react = _interopRequireWildcard(require("react"));

require("../_helpers/_bootstrap.scss");

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var React$createElement = _react.default.createElement;

var Tab = function Tab(_ref) {
  var isActive = _ref.isActive,
      children = _ref.children,
      props = (0, _objectWithoutPropertiesLoose2.default)(_ref, ["isActive", "children"]);
  return React$createElement("div", (0, _extends2.default)({
    className: (0, _classnames.default)("mt-tab-pane", isActive && "active")
  }, props), children);
};

Tab.propTypes = {
  /**
   * Let Tabs component identifies particular tabs
   *
   * @type string
   * @required
   */
  tabKey: PropTypes.string.isRequired,

  /**
   *
   *
   * @type string
   * @required
   */
  title: PropTypes.oneOfType([PropTypes.string]).isRequired,
  isActive: PropTypes.bool
};
var _default = Tab;
exports.default = _default;
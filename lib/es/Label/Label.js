"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _react = _interopRequireWildcard(require("react"));

require("../_helpers/_bootstrap.scss");

var React$createElement = _react.default.createElement;

// import { connect } from "react-redux";
// import { bindActionCreators } from "redux";
// import * as LabelActions from "../../store/Label/actions";
var Label =
/*#__PURE__*/
function (_Component) {
  (0, _inheritsLoose2.default)(Label, _Component);

  function Label() {
    return _Component.apply(this, arguments) || this;
  }

  var _proto = Label.prototype;

  // constructor(props) {
  //     super(props);
  //     this.state = {};
  // }
  _proto.render = function render() {
    return React$createElement("div", {
      className: "component-label"
    }, "Hello! component Label");
  };

  return Label;
}(_react.Component); // export default connect(
//     ({ Label }) => ({ ...Label }),
//     dispatch => bindActionCreators({ ...LabelActions }, dispatch)
//   )( Label );


exports.default = Label;
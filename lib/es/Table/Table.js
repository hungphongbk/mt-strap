"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _react = _interopRequireWildcard(require("react"));

require("../_helpers/_bootstrap.scss");

require("../../sass/components/_tables.scss");

var React$createElement = _react.default.createElement;

var Table =
/*#__PURE__*/
function (_Component) {
  (0, _inheritsLoose2.default)(Table, _Component);

  function Table() {
    return _Component.apply(this, arguments) || this;
  }

  var _proto = Table.prototype;

  _proto.render = function render() {
    return React$createElement("table", {
      className: "mt-table mt-table-style mt-table-size"
    }, this.props.children);
  };

  return Table;
}(_react.Component);

Table.propTypes = {};
Table.defaultProps = {};
var _default = Table;
exports.default = _default;
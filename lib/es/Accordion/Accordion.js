"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

exports.__esModule = true;
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

var _AccordionModule = _interopRequireDefault(require("./Accordion.module.scss"));

var React$createElement = _react.default.createElement;

var Accordion = function Accordion(props) {
  return React$createElement("div", {
    className: "mt-accordion"
  }, props.children);
};

Accordion.propTypes = {};
Accordion.defaultProps = {};
var _default = Accordion;
exports.default = _default;
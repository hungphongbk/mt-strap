"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

require("../_helpers/_bootstrap.scss");

var _ImageModule = _interopRequireDefault(require("./Image.module.scss"));

var _FixedRatio = _interopRequireDefault(require("../FixedRatio/FixedRatio"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactResizeObserver = _interopRequireDefault(require("react-resize-observer"));

var React$createElement = _react.default.createElement;
var symbolShow = Symbol("show"),
    observer = typeof IntersectionObserver !== "undefined" && new IntersectionObserver(function (entries) {
  entries.forEach(function (entry) {
    if (entry.intersectionRatio) {
      var $img = entry.target.children[0];
      $img.hasOwnProperty(symbolShow) && $img[symbolShow]();
    }
  });
}),
    // TODO: #info SSR support
observe = observer ? observer.observe.bind(observer) : function () {};

var Image = function Image(_ref) {
  var src = _ref.src,
      alt = _ref.alt,
      children = _ref.children,
      borderStyle = _ref.borderStyle,
      ratio = _ref.ratio,
      className = _ref.className,
      style = _ref.style;

  if (ratio && typeof ratio === "string") {
    /**
     *
     * @type {React.MutableRefObject<HTMLImageElement>}
     */
    // eslint-disable-next-line react-hooks/rules-of-hooks
    var $img = _react.default.useRef(null);

    var ratioDiff = 0;

    var _ratio$split = ratio.split("-"),
        width = _ratio$split[0],
        height = _ratio$split[1],
        setHeight = function setHeight() {
      var img = $img.current;
      if (!img) return;
      img.style.height = img.parentElement.clientHeight + "px";
      img.style.width = "auto"; // console.log("set height", img?.parentElement.clientHeight);
    },
        setWidth = function setWidth() {
      var img = $img.current;
      if (!img) return;
      img.style.height = "auto";
      img.style.width = img.parentElement.clientWidth + "px"; // console.log("set width");
    },
        onLoad = function onLoad() {
      var _$img$current = $img.current,
          naturalWidth = _$img$current.naturalWidth,
          naturalHeight = _$img$current.naturalHeight;
      ratioDiff = naturalWidth * 1.0 / naturalHeight / (width * 1.0 / height);
      Object.defineProperty($img.current, symbolShow, {
        get: function get() {
          return function () {
            // console.log("what?");
            if (ratioDiff > 1.0) setHeight();else setWidth();
          };
        }
      });
      $img.current[symbolShow]();
    },
        onResize = function onResize() {
      if (Math.abs(ratio) < 0.01) return;
      $img.current.hasOwnProperty(symbolShow) && $img.current[symbolShow]();
    };

    return React$createElement(_FixedRatio.default, {
      className: (0, _classnames.default)(_ImageModule.default.Image, {
        rect: null,
        rounded: _ImageModule.default.Rounded,
        circle: _ImageModule.default.Circle
      }[borderStyle], className),
      height: height,
      width: width,
      style: style,
      ref: function ref(el) {
        el && observe(el);
      }
    }, React$createElement("img", {
      ref: $img,
      src: src,
      alt: alt,
      onLoad: onLoad
    }), children && React$createElement("div", {
      className: _ImageModule.default.Children
    }, children), React$createElement(_reactResizeObserver.default, {
      onResize: onResize
    }));
  } else {}
};

Image.FIXED_RATIO_5_3 = "5-3";
Image.FIXED_RATIO_4_3 = "4-3";
Image.FIXED_RATIO_1_1 = "1-1";
Image.propTypes = {
  /**
   * Image src
   *
   * @type string
   * @required
   */
  src: PropTypes.string.isRequired,

  /**
   * Image alt
   *
   * @type string
   * @default ''
   */
  alt: PropTypes.string,

  /**
   * Set image border shape as rectangular, rounded or circle
   *
   * @type ('rect'|'rounded'|'circle')
   * @default 'rect'
   */
  borderStyle: PropTypes.oneOf(["rect", "rounded", "circle"]),

  /**
   * Enable this option to maintain aspect ratio with new image sizes
   *
   * @type (string|null)
   * @default null
   */
  ratio: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.object
};
Image.defaultProps = {
  borderStyle: "rect",
  ratio: null,
  children: null,
  className: "",
  style: {}
};
var _default = Image;
exports.default = _default;
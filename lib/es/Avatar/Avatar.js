"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

require("../_helpers/_bootstrap.scss");

var _Image = _interopRequireDefault(require("../Image/Image"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var React$createElement = _react.default.createElement;

var Avatar = function Avatar(_ref) {
  var src = _ref.src,
      alt = _ref.alt,
      size = _ref.size;
  return React$createElement(_Image.default, {
    src: src,
    alt: alt,
    ratio: _Image.default.FIXED_RATIO_1_1,
    borderStyle: "circle",
    className: "mt-avatar",
    style: {
      width: size + "px"
    }
  });
};

Avatar.propTypes = {
  size: PropTypes.number
};
Avatar.defaultProps = {
  size: 30
};
var _default = Avatar;
exports.default = _default;
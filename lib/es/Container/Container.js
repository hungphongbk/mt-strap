"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var React$createElement = _react.default.createElement;

var Container = function Container(_ref) {
  var className = _ref.className,
      children = _ref.children,
      Component = _ref.component;
  return React$createElement(Component, {
    className: (0, _classnames.default)("mt-container", className)
  }, children);
};

Container.propTypes = {
  component: PropTypes.oneOf([PropTypes.string, PropTypes.elementType]),
  className: PropTypes.any
};
Container.defaultProps = {
  component: "div"
};
var _default = Container;
exports.default = _default;
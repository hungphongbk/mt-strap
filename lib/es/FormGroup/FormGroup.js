"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutPropertiesLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutPropertiesLoose"));

var _react = _interopRequireWildcard(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

require("../../sass/components/_inputs.scss");

var _uniqueId = _interopRequireDefault(require("../_helpers/uniqueId"));

var _utils = require("../_helpers/utils");

var React$createElement = _react.default.createElement;

var FormGroup = function FormGroup(props) {
  var children = props.children,
      error = props.error,
      success = props.success,
      className = props.className,
      label = props.label,
      other = (0, _objectWithoutPropertiesLoose2.default)(props, ["children", "error", "success", "className", "label"]);

  var classes = ["mt-form-group"],
      renderAfters = [],
      addAfter = function addAfter(after) {
    if (typeof after === "string") renderAfters.push(React$createElement("span", {
      className: "mt-input-validated-text mt-text-desc"
    }, after));
  },
      labelProps = {},
      _React$useRef = _react.default.useRef((0, _uniqueId.default)(5, "form-group-")),
      componentId = _React$useRef.current;

  var newChildren = children;
  /**
   * Add some classes when TextInput has label
   */

  if (label) {
    classes.push("mt-form-group-label");
    Object.assign(labelProps, {
      for: componentId
    });
    newChildren = _react.default.Children.map(newChildren, function (child, index) {
      if (index > 0) return child;
      return _react.default.cloneElement(child, {
        id: componentId
      });
    });
  }

  if (error) {
    classes.push("mt-validated-error");
    addAfter(error);
  }

  if (success) {
    classes.push("mt-validated-success");
    addAfter(success);
  }

  return React$createElement("div", (0, _extends2.default)({
    className: (0, _classnames.default)(classes, className)
  }, other), label && React$createElement("label", (0, _extends2.default)({
    className: "mt-label-default"
  }, labelProps), label), newChildren, renderAfters);
};

FormGroup.propTypes = {
  className: PropTypes.any,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  error: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  success: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
};
FormGroup.defaultProps = {
  error: false,
  success: false
};
var _default = FormGroup;
exports.default = _default;
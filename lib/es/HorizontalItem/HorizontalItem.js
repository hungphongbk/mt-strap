"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _react = _interopRequireDefault(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

var _HorizontalItemModule = _interopRequireDefault(require("./HorizontalItem.module.scss"));

var _Image = _interopRequireDefault(require("../Image/Image"));

var _stringManipulate = require("../_helpers/stringManipulate");

var React$createElement = _react.default.createElement;

var HorizontalItem =
/*#__PURE__*/
function (_React$PureComponent) {
  (0, _inheritsLoose2.default)(HorizontalItem, _React$PureComponent);

  function HorizontalItem() {
    var _this;

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _React$PureComponent.call.apply(_React$PureComponent, [this].concat(args)) || this;
    _this.state = {
      contentEl: null
    };

    _this.bindContentEl = function (contentEl) {
      return contentEl && _this.setState({
        contentEl: (0, _stringManipulate.wordLefts)(contentEl.innerText)
      });
    };

    _this.getThumbnailAlt = function () {
      var thumbnailAlt = _this.props.thumbnailAlt,
          contentEl = _this.state.contentEl;
      if (thumbnailAlt && thumbnailAlt.length > 0) return thumbnailAlt;
      if (contentEl) return contentEl;
      return "";
    };

    return _this;
  }

  var _proto = HorizontalItem.prototype;

  _proto.render = function render() {
    var props = this.props;
    return React$createElement("div", {
      className: (0, _classnames.default)(_HorizontalItemModule.default.HorizontalItem, props.className)
    }, React$createElement("div", {
      className: _HorizontalItemModule.default.Thumbnail,
      style: {
        width: props.thumbnailWidth
      }
    }, React$createElement(_Image.default, {
      src: props.thumbnailSrc,
      ratio: props.thumbnailRatio,
      alt: this.getThumbnailAlt()
    })), React$createElement("div", {
      className: _HorizontalItemModule.default.Content,
      ref: this.bindContentEl
    }, props.children));
  };

  return HorizontalItem;
}(_react.default.PureComponent);

HorizontalItem.propTypes = {
  thumbnailSrc: PropTypes.string.isRequired,
  thumbnailAlt: PropTypes.string,
  thumbnailRatio: PropTypes.string,
  thumbnailWidth: PropTypes.string,
  className: PropTypes.any,
  children: PropTypes.any
};
HorizontalItem.defaultProps = {
  thumbnailRatio: "1-1",
  thumbnailWidth: "25%"
};
var _default = HorizontalItem;
exports.default = _default;
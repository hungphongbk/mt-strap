"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

exports.__esModule = true;
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _inheritsLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/inheritsLoose"));

var _react = _interopRequireDefault(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

require("../_helpers/_bootstrap.scss");

var _DropdownEffectModule = _interopRequireDefault(require("./DropdownEffect.module.scss"));

var _withProps = _interopRequireDefault(require("../_helpers/withProps"));

var _fastDom = _interopRequireDefault(require("../_helpers/fastDom"));

var _uniqueId = _interopRequireDefault(require("../_helpers/uniqueId"));

var _dec, _class, _temp;

var React$createElement = _react.default.createElement;

var frameRate = 1000 / 60,
    clamp = function clamp(value, min, max) {
  return Math.max(min, Math.min(max, value));
},
    ease = function ease(v, pow) {
  if (pow === void 0) {
    pow = 4;
  }

  var _v = clamp(v, 0, 1);

  return 1 - Math.pow(1 - _v, pow);
};

var DropdownEffect = (_dec = (0, _withProps.default)(function (props) {
  return {
    nFrames: props.animationDuration / frameRate,
    translateY: -props.index * 100
  };
}), _dec(_class = (_temp =
/*#__PURE__*/
function (_React$PureComponent) {
  (0, _inheritsLoose2.default)(DropdownEffect, _React$PureComponent);

  function DropdownEffect() {
    var _this;

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _React$PureComponent.call.apply(_React$PureComponent, [this].concat(args)) || this;
    _this.menu = null;
    _this.menuContent = null;
    _this.menuTitle = null;
    _this.state = {
      id: (0, _uniqueId.default)(5),
      isOpen: true,
      animate: false,
      collapsed: {
        x: 0,
        y: 0
      }
    };

    _this.assignElement = function (name) {
      return function (el) {
        if (typeof el !== "undefined" && el !== null) _this[name] = el;
      };
    };

    _this.activate =
    /*#__PURE__*/
    (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee() {
      return _regenerator.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _fastDom.default.mutate(function () {
                _this.menu.classList.add(_DropdownEffectModule.default.MenuActive);
              });

            case 2:
              _this.setState({
                animate: true
              });

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));
    _this.expand =
    /*#__PURE__*/
    (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee2() {
      return _regenerator.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              if (!_this.state.isOpen) {
                _context2.next = 2;
                break;
              }

              return _context2.abrupt("return");

            case 2:
              _this.setState({
                isOpen: true
              });

              if (!_this.state.animate) {
                _context2.next = 7;
                break;
              }

              _this.createEaseAnimation();

              _context2.next = 7;
              return _this.applyAnimation({
                expand: true
              });

            case 7:
              _context2.next = 9;
              return _fastDom.default.mutate(function () {
                _this.menu.style.transform = "scale(1,1) translateY(" + _this.props.translateY + "%)";
                _this.menuContent.style.transform = "scale(1,1) translateY(0%)";
              });

            case 9:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));
    _this.collapse =
    /*#__PURE__*/
    (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee3() {
      var _this$state$collapsed, x, y, invX, invY;

      return _regenerator.default.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              if (_this.state.isOpen) {
                _context3.next = 2;
                break;
              }

              return _context3.abrupt("return");

            case 2:
              _this.setState({
                isOpen: false
              }); // console.log("collapsed " + this.state.isOpen);


              _this$state$collapsed = _this.state.collapsed, x = _this$state$collapsed.x, y = _this$state$collapsed.y;
              invX = 1 / x;
              invY = 1 / y;

              if (!_this.state.animate) {
                _context3.next = 10;
                break;
              }

              _this.createEaseAnimation();

              _context3.next = 10;
              return _this.applyAnimation({
                expand: false
              });

            case 10:
              _context3.next = 12;
              return _fastDom.default.mutate(function () {
                _this.menu.style.transform = "scale(" + x + ", " + y + ") translateY(0%)";
                _this.menuContent.style.transform = "scale(" + invX + ", " + invY + ") translateY(" + _this.props.translateY + "%)";
              });

            case 12:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3);
    }));

    _this.toggle = function () {
      return new Promise(function (resolve) {
        if (_this.state.isOpen) {
          _this.collapse().then(resolve);
        } else _this.expand().then(resolve);
      });
    };

    _this.createEaseAnimation = function () {
      var menuEase = document.querySelector(_DropdownEffectModule.default.MenuEase + _this.state.id);

      if (!menuEase) {
        menuEase = document.createElement("style");
        menuEase.classList.add(_DropdownEffectModule.default.MenuEase + _this.state.id);
      } else return;

      var duration = _this.props.animationDuration;
      var menuExpandAnimation = [];
      var menuExpandContentsAnimation = [];
      var menuCollapseAnimation = [];
      var menuCollapseContentsAnimation = [];
      var percentIncrement = 100 / _this.props.nFrames;

      for (var i = 0; i <= _this.props.nFrames; i++) {
        var step = ease(i / _this.props.nFrames).toFixed(5);
        console.log(step);
        var percentage = (i * percentIncrement).toFixed(5);
        var startX = _this.state.collapsed.x;
        var startY = _this.state.collapsed.y;
        var endX = 1;
        var endY = 1;
        var startTranslateY = 0;
        var endTranslateY = _this.props.translateY; // expand animation

        _this.append({
          percentage: percentage,
          step: step,
          startX: startX,
          startY: startY,
          endX: endX,
          endY: endY,
          startTranslateY: startTranslateY,
          endTranslateY: endTranslateY,
          outerAnimation: menuExpandAnimation,
          innerAnimation: menuExpandContentsAnimation
        }); // Collapse animation.


        _this.append({
          percentage: percentage,
          step: step,
          startX: 1,
          startY: 1,
          endX: _this.state.collapsed.x,
          endY: _this.state.collapsed.y,
          startTranslateY: _this.props.translateY,
          endTranslateY: 0,
          outerAnimation: menuCollapseAnimation,
          innerAnimation: menuCollapseContentsAnimation
        });
      }

      menuEase.textContent = ["@keyframes " + _DropdownEffectModule.default.menuExpandAnimation + " {" + menuExpandAnimation.join("") + "}", "@keyframes " + _DropdownEffectModule.default.menuExpandContentsAnimation + " {" + menuExpandContentsAnimation.join("") + "}", "@keyframes " + _DropdownEffectModule.default.menuCollapseAnimation + " {" + menuCollapseAnimation.join("") + "}", "@keyframes " + _DropdownEffectModule.default.menuCollapseContentsAnimation + " {" + menuCollapseContentsAnimation.join("") + "}", "." + _DropdownEffectModule.default.Menu + "{animation-duration:" + duration + "ms}", "." + _DropdownEffectModule.default.MenuContents + "{animation-duration:" + duration + "ms}"].join("");
      console.log(menuEase.textContent);
      document.head.appendChild(menuEase);
      return menuEase;
    };

    _this.append = function (_ref4) {
      var percentage = _ref4.percentage,
          step = _ref4.step,
          startX = _ref4.startX,
          startY = _ref4.startY,
          endX = _ref4.endX,
          endY = _ref4.endY,
          startTranslateY = _ref4.startTranslateY,
          endTranslateY = _ref4.endTranslateY,
          outerAnimation = _ref4.outerAnimation,
          innerAnimation = _ref4.innerAnimation;

      var f = function f(start, end) {
        return (start + (end - start) * step).toFixed(5);
      };

      var xScale = f(startX, endX);
      var yScale = f(startY, endY);
      var yTranslate = f(startTranslateY, endTranslateY);
      var invScaleX = (1 / xScale).toFixed(5);
      var invScaleY = (1 / yScale).toFixed(5);
      var invTranslateY = f(endTranslateY, startTranslateY);
      console.log(yTranslate, invTranslateY);
      outerAnimation.push(percentage + "% {transform: scale(" + xScale + ", " + yScale + ") translateY(" + yTranslate + "%);}");
      innerAnimation.push(percentage + "% {transform: scale(" + invScaleX + ", " + invScaleY + ") translateY(" + invTranslateY + "%);}");
    };

    _this.applyAnimation =
    /*#__PURE__*/
    function () {
      var _ref6 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee4(_ref5) {
        var expand;
        return _regenerator.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                expand = _ref5.expand;
                _context4.next = 3;
                return _fastDom.default.mutate(function () {
                  _this.menu.classList.remove(_DropdownEffectModule.default.Expanded);

                  _this.menu.classList.remove(_DropdownEffectModule.default.Collapsed); // eslint-disable-next-line no-unused-expressions


                  window.getComputedStyle(_this.menu).transform;
                });

              case 3:
                if (!expand) {
                  _context4.next = 8;
                  break;
                }

                _context4.next = 6;
                return _fastDom.default.mutate(function () {
                  _this.menu.classList.add(_DropdownEffectModule.default.Expanded);
                });

              case 6:
                _context4.next = 10;
                break;

              case 8:
                _context4.next = 10;
                return _fastDom.default.mutate(function () {
                  _this.menu.classList.add(_DropdownEffectModule.default.Collapsed);
                });

              case 10:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      return function (_x) {
        return _ref6.apply(this, arguments);
      };
    }();

    _this.calculateScales =
    /*#__PURE__*/
    (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee5() {
      var collapsed, expanded;
      return _regenerator.default.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              _context5.next = 2;
              return _fastDom.default.measure(function () {
                collapsed = _this.menuTitle.getBoundingClientRect();
                expanded = _this.menu.getBoundingClientRect();
              });

            case 2:
              _this.setState({
                collapsed: {
                  x: collapsed.width / expanded.width,
                  y: collapsed.height / expanded.height
                }
              });

            case 3:
            case "end":
              return _context5.stop();
          }
        }
      }, _callee5);
    }));
    return _this;
  }

  var _proto = DropdownEffect.prototype;

  _proto.componentDidMount = function componentDidMount() {
    var _this2 = this;

    (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee6() {
      return _regenerator.default.wrap(function _callee6$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              _context6.next = 2;
              return _this2.calculateScales();

            case 2:
              _this2.createEaseAnimation();

              _context6.next = 5;
              return _this2.collapse();

            case 5:
              _context6.next = 7;
              return _this2.activate();

            case 7:
            case "end":
              return _context6.stop();
          }
        }
      }, _callee6);
    }))();
  };

  _proto.render = function render() {
    return React$createElement("div", {
      className: (0, _classnames.default)(_DropdownEffectModule.default.Container, this.props.className)
    }, React$createElement("div", {
      className: _DropdownEffectModule.default.Menu,
      ref: this.assignElement("menu")
    }, React$createElement("div", {
      className: _DropdownEffectModule.default.MenuContents,
      ref: this.assignElement("menuContent")
    }, this.props.children({
      itemRef: this.assignElement("menuTitle"),
      toggle: this.toggle
    }))));
  };

  return DropdownEffect;
}(_react.default.PureComponent), _temp)) || _class);
DropdownEffect.propTypes = {
  dropdownHeight: PropTypes.any,
  animationDuration: PropTypes.number,
  index: PropTypes.number.isRequired,
  // computed
  nFrames: PropTypes.number,
  translateY: PropTypes.number,
  // default
  className: PropTypes.any,
  children: PropTypes.func.isRequired
};
DropdownEffect.defaultProps = {
  animationDuration: 300
};
var _default = DropdownEffect;
exports.default = _default;
"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

exports.__esModule = true;
exports.default = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutPropertiesLoose2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutPropertiesLoose"));

var _react = _interopRequireWildcard(require("react"));

var PropTypes = _interopRequireWildcard(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactTagsinput = _interopRequireDefault(require("react-tagsinput"));

require("../_helpers/_bootstrap.scss");

var _TagsInputModule = _interopRequireDefault(require("./TagsInput.module.scss"));

var React$createElement = _react.default.createElement;

var TagsInput = function TagsInput(_ref) {
  var value = _ref.value,
      onChange = _ref.onChange,
      className = _ref.className,
      rest = (0, _objectWithoutPropertiesLoose2.default)(_ref, ["value", "onChange", "className"]);
  return React$createElement("div", {
    className: (0, _classnames.default)(_TagsInputModule.default.TagsInput, className)
  }, React$createElement(_reactTagsinput.default, (0, _extends2.default)({
    value: value,
    onChange: onChange
  }, rest)));
};

TagsInput.propTypes = {
  /**
   * The value of the TagsInput. Must be a valid array
   *
   * @type Array
   * @required
   */
  value: PropTypes.arrayOf(PropTypes.any).isRequired,

  /**
   * Callback function that is fired when the TagsInput's value changed.
   *
   * @type function
   * @required
   */
  onChange: PropTypes.func.isRequired
};
TagsInput.defaultProps = {};
var _default = TagsInput;
exports.default = _default;
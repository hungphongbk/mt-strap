const path = require("path"),
    jsDoc = require("jsdoc-api");

module.exports = {};
module.exports.typeDefs = [
    `
    type Query {
        component(name: String): ComponentAPI
    }
    type ComponentAPI {
        name: String
        specs: [Spec]
    }
    type Spec {
        name: String
        type: String
        default: String
        description: String
    }
    schema {
        query: Query
    }
`
];
module.exports.resolvers = {
    Query: {
        component: (root, { name }) =>
            new Promise(resolve => {
                jsDoc
                    .explain({
                        files: path.resolve(
                            __dirname,
                            "../../",
                            `src/components/${name}/${name}.js`
                        )
                    })
                    .then(explained => {
                        const rs = { name };
                        rs.specs = explained
                            .filter(i => i.memberof === `${name}.propTypes`)
                            .map(i => {
                                // console.log(
                                //     i.type,
                                //     Object.getOwnPropertyNames(i.type)
                                // );
                                return {
                                    name: i.name,
                                    description: i.description,
                                    type:
                                        i.type && i.type.hasOwnProperty("names")
                                            ? i.type.names.join(" | ")
                                            : i.type || "string",
                                    default: i.defaultvalue || ""
                                };
                            });
                        resolve(rs);
                    });
            })
    }
};

// prefer default export if available
const preferDefault = m => m && m.default || m

exports.components = {
  "component---cache-dev-404-page-js": () => import("/home/myowngrave/WebstormProjects/components/.cache/dev-404-page.js" /* webpackChunkName: "component---cache-dev-404-page-js" */),
  "component---src-pages-index-js": () => import("/home/myowngrave/WebstormProjects/components/src/pages/index.js" /* webpackChunkName: "component---src-pages-index-js" */),
  "component---src-pages-react-alert-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/alert.mdx" /* webpackChunkName: "component---src-pages-react-alert-mdx" */),
  "component---src-pages-react-cards-accordions-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/cards-accordions.mdx" /* webpackChunkName: "component---src-pages-react-cards-accordions-mdx" */),
  "component---src-pages-react-colors-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/colors.mdx" /* webpackChunkName: "component---src-pages-react-colors-mdx" */),
  "component---src-pages-react-buttons-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/buttons.mdx" /* webpackChunkName: "component---src-pages-react-buttons-mdx" */),
  "component---src-pages-react-faq-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/faq.mdx" /* webpackChunkName: "component---src-pages-react-faq-mdx" */),
  "component---src-pages-react-dev-docs-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/dev-docs.mdx" /* webpackChunkName: "component---src-pages-react-dev-docs-mdx" */),
  "component---src-pages-react-grid-system-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/grid-system.mdx" /* webpackChunkName: "component---src-pages-react-grid-system-mdx" */),
  "component---src-pages-react-custom-input-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/custom-input.mdx" /* webpackChunkName: "component---src-pages-react-custom-input-mdx" */),
  "component---src-pages-react-install-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/install.mdx" /* webpackChunkName: "component---src-pages-react-install-mdx" */),
  "component---src-pages-react-breadcrumb-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/breadcrumb.mdx" /* webpackChunkName: "component---src-pages-react-breadcrumb-mdx" */),
  "component---src-pages-react-images-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/images.mdx" /* webpackChunkName: "component---src-pages-react-images-mdx" */),
  "component---src-pages-react-radio-checkboxes-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/radio-checkboxes.mdx" /* webpackChunkName: "component---src-pages-react-radio-checkboxes-mdx" */),
  "component---src-pages-react-references-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/references.mdx" /* webpackChunkName: "component---src-pages-react-references-mdx" */),
  "component---src-pages-react-getting-started-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/getting-started.mdx" /* webpackChunkName: "component---src-pages-react-getting-started-mdx" */),
  "component---src-pages-react-paginations-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/paginations.mdx" /* webpackChunkName: "component---src-pages-react-paginations-mdx" */),
  "component---src-pages-react-tabs-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/tabs.mdx" /* webpackChunkName: "component---src-pages-react-tabs-mdx" */),
  "component---src-pages-react-modal-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/modal.mdx" /* webpackChunkName: "component---src-pages-react-modal-mdx" */),
  "component---src-pages-react-tooltip-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/tooltip.mdx" /* webpackChunkName: "component---src-pages-react-tooltip-mdx" */),
  "component---src-pages-react-value-slider-progress-bar-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/value-slider-progress-bar.mdx" /* webpackChunkName: "component---src-pages-react-value-slider-progress-bar-mdx" */),
  "component---src-pages-react-coding-styles-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/coding-styles.mdx" /* webpackChunkName: "component---src-pages-react-coding-styles-mdx" */),
  "component---src-pages-react-selection-controls-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/selection-controls.mdx" /* webpackChunkName: "component---src-pages-react-selection-controls-mdx" */),
  "component---src-pages-react-text-input-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/text-input.mdx" /* webpackChunkName: "component---src-pages-react-text-input-mdx" */),
  "component---src-pages-react-table-mdx": () => import("/home/myowngrave/WebstormProjects/components/src/pages/react/table.mdx" /* webpackChunkName: "component---src-pages-react-table-mdx" */)
}


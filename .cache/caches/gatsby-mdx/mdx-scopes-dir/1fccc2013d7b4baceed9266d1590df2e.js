import ReactPlayground from "../../../../src/pages-components/ReactPlayground/ReactPlayground";
import ComponentApi from "../../../../src/pages-components/ComponentAPI/ComponentAPI";
import Tabs from "../../../../src/components/Tabs/Tabs";
import Tab from "../../../../src/components/Tab/Tab";
import React from 'react';
import { MDXTag } from '@mdx-js/tag';
export default {
  ReactPlayground,
  ComponentApi,
  Tabs,
  Tab,
  React,
  MDXTag
};
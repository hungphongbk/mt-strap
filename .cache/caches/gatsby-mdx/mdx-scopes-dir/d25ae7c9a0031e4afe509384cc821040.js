import ReactPlayground from "../../../../src/pages-components/ReactPlayground/ReactPlayground";
import ComponentApi from "../../../../src/pages-components/ComponentAPI/ComponentAPI";
import Alert from "../../../../src/components/Alert/Alert";
import Button from "../../../../src/components/Button/Button";
import React from 'react';
import { MDXTag } from '@mdx-js/tag';
export default {
  ReactPlayground,
  ComponentApi,
  Alert,
  Button,
  React,
  MDXTag
};
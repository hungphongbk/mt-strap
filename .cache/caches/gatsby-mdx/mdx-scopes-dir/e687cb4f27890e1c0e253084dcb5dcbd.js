import ReactPlayground from "../../../../src/pages-components/ReactPlayground/ReactPlayground";
import ComponentApi from "../../../../src/pages-components/ComponentAPI/ComponentAPI";
import React from 'react';
import { MDXTag } from '@mdx-js/tag';
export default {
  ReactPlayground,
  ComponentApi,
  React,
  MDXTag
};
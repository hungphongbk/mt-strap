import ColorPalette from "../../../../src/pages-components/ColorPalette/ColorPalette";
import React from 'react';
import { MDXTag } from '@mdx-js/tag';
export default {
  ColorPalette,
  React,
  MDXTag
};
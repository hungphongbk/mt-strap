import { Link } from "gatsby";
import React from 'react';
import { MDXTag } from '@mdx-js/tag';
export default {
  Link,
  React,
  MDXTag
};
const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => m && m.default || m


exports.components = {
  "component---cache-dev-404-page-js": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/.cache/dev-404-page.js"))),
  "component---src-pages-index-js": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/index.js"))),
  "component---src-pages-react-alert-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/alert.mdx"))),
  "component---src-pages-react-cards-accordions-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/cards-accordions.mdx"))),
  "component---src-pages-react-colors-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/colors.mdx"))),
  "component---src-pages-react-buttons-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/buttons.mdx"))),
  "component---src-pages-react-faq-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/faq.mdx"))),
  "component---src-pages-react-dev-docs-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/dev-docs.mdx"))),
  "component---src-pages-react-grid-system-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/grid-system.mdx"))),
  "component---src-pages-react-custom-input-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/custom-input.mdx"))),
  "component---src-pages-react-install-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/install.mdx"))),
  "component---src-pages-react-breadcrumb-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/breadcrumb.mdx"))),
  "component---src-pages-react-images-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/images.mdx"))),
  "component---src-pages-react-radio-checkboxes-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/radio-checkboxes.mdx"))),
  "component---src-pages-react-references-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/references.mdx"))),
  "component---src-pages-react-getting-started-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/getting-started.mdx"))),
  "component---src-pages-react-paginations-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/paginations.mdx"))),
  "component---src-pages-react-tabs-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/tabs.mdx"))),
  "component---src-pages-react-modal-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/modal.mdx"))),
  "component---src-pages-react-tooltip-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/tooltip.mdx"))),
  "component---src-pages-react-value-slider-progress-bar-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/value-slider-progress-bar.mdx"))),
  "component---src-pages-react-coding-styles-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/coding-styles.mdx"))),
  "component---src-pages-react-selection-controls-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/selection-controls.mdx"))),
  "component---src-pages-react-text-input-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/text-input.mdx"))),
  "component---src-pages-react-table-mdx": hot(preferDefault(require("/home/myowngrave/WebstormProjects/components/src/pages/react/table.mdx")))
}


var gulp = require('gulp');
var sass = require('gulp-sass');

var include = require('gulp-file-include');
var browser_sync = require('browser-sync').create();
var htmlbeautify = require('gulp-html-beautify');

// html
gulp.task('sync-html', function() {
    gulp.src([
        './html/**/*.html',
        '!./html/html-global/*.html',
    ])
    .pipe(include())
    .pipe(gulp.dest('./build/static'))
    .pipe(browser_sync.stream());
});

// sass
gulp.task('sync-sass', function() {
    gulp.src([
        './src/sass/**/*.scss',
    ])
    .pipe(sass())
    .pipe(gulp.dest('./build/static/theme/css'))
    .pipe(browser_sync.stream());
});

// COPY library css
gulp.task('copy-css', function(){
    gulp.src([
        // './node_modules/converthtm./build/static/theme/css/convert-theme-default.css',
        './node_modules/bootstrap-touchspin/src/jquery.bootstrap-touchspin.css',
        './node_modules/ion-rangeslider/css/ion.rangeSlider.css',
        './node_modules/ion-rangeslider/css/ion.rangeSlider.skinHTML5.css',
        './node_modules/select2/dist/css/select2.min.css',
        './node_modules/perfect-scrollbar/css/perfect-scrollbar.css'

    ])
    .pipe(gulp.dest('./build/static/theme/css'));
});

// COPY library js
gulp.task('copy-js', function(){
    gulp.src([
        './node_modules/bootstrap-touchspin/src/jquery.bootstrap-touchspin.js',
        './node_modules/ion-rangeslider/js/ion.rangeSlider.min.js',
        './node_modules/select2/dist/js/select2.full.min.js',
        './node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js'
    ])
    // .pipe(concat('all.min.js'))
    // khi cần min thì bỏ note và chạy gulp
    //.pipe(concat('all.min.js'))
    // .pipe(jsmin())
    .pipe(gulp.dest('./build/static/theme/js'));
});

//copy images
gulp.task('copy-images', function(){
    gulp.src([
        './src/theme/images/*.jpg',
        './src/theme/images/*.png',
        './src/theme/images/**/*.jpg',
        './src/theme/images/**/*.png'
    ])
    .pipe(gulp.dest('./build/static/theme/images'))
    .pipe(browser_sync.stream());
});

// COPY fonts
gulp.task('copy-fonts', function(){
    gulp.src([
        './src/theme/fonts/**'
    ])
    .pipe(gulp.dest('./build/static/theme/fonts'));
});

// browser sync
gulp.task('browser-sync', [ 'sync-html', 'sync-sass','copy-images' ], function() {
    browser_sync.init({
        server: {
            baseDir: "./build/static"
        }
    });

    gulp.watch('./src/sass/**/*.scss', ['sync-sass']);
    gulp.watch('./html/**/*.html', ['sync-html']);
    gulp.watch(['./src/theme/images/*.jpg','./src/theme/images/*.png'], ['copy-images']);
});

gulp.task('beautify-html', function(){
    var options = {
        "indent_size": 4
    };
    gulp.src([
        './build/static/*.html'
    ])
    .pipe(htmlbeautify(options))
    .pipe(gulp.dest('./build/static'))
});

// run all task
gulp.task('default', ['browser-sync','copy-css','copy-js','copy-fonts','beautify-html']);


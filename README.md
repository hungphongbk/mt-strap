# mt-strap

## Yêu cầu hệ thống

-   NodeJS >= 11.10.6

## Hướng dẫn cài đặt và build tài liệu

1. Clone project.
2. Chạy lệnh `npm install`, có thể chờ hơi lâu vì số lượng thư viện khá lớn.
3. Chạy lệnh `npm run build`
    - Lệnh này sẽ đồng thời build hai bộ tài liệu: _**storybook**_ _(đóng vai trò demo)_ và _**ReactJS**_ _(bộ tài liệu chính của các component mt-strap v4)_
    - output của tài liệu storybook là `public/storybook`
    - output của tài liệu react là `public/react`

const { createFilePath } = require(`gatsby-source-filesystem`),
    jsDoc = require("jsdoc-api"),
    _compile = require("webpack-inline-compiler"),
    reactConfig = require("./config/webpack.config")("production-gatsby"),
    path = require("path");

const __GATSBY_DEMO_RENDERED__ = `__GATSBY_DEMO_RENDERED__`;

let i = 10;

const compile = (code, entry = null) =>
    _compile(code, webpackConfig => {
        function last(array, n) {
            if (array == null) return void 0;
            if (n == null) return array[array.length - 1];
            return array.slice(Math.max(array.length - n, 0));
        }

        webpackConfig.entry = entry;
        Object.assign(webpackConfig.output, {
            publicPath: "/",
            library: __GATSBY_DEMO_RENDERED__,
            libraryTarget: "var"
        });
        ["mode", "module", "resolve", "optimization"].forEach(key => {
            webpackConfig[key] = reactConfig[key];
        });
        webpackConfig.optimization.runtimeChunk = false;
        webpackConfig.externals.push(
            ...[
                {
                    "react-jss": "injectSheet"
                },
                function(context, request, callback) {
                    if (/\.\.\/components/.test(request)) {
                        return callback(
                            null,
                            "root " + last(request.split("/"))
                        );
                    }
                    callback();
                }
            ]
        );
        // console.log(Object.getOwnPropertyNames(webpackConfig));
        // process.exit(0);
        return webpackConfig;
    });

const parse = (name, source) =>
        new Promise(resolve => {
            jsDoc
                .explain({
                    source
                })
                .catch(err => console.error(err, name))
                .then(explained => {
                    const rs = { name };
                    if (typeof explained !== "undefined" && explained !== null)
                        rs.specs = explained
                            .filter(i => i.memberof === `${name}.propTypes`)
                            .map(i => {
                                // console.log(
                                //     i.type,
                                //     Object.getOwnPropertyNames(i.type)
                                // );
                                i.attrs = {};
                                ["required"].forEach(attr => {
                                    i.attrs[attr] = false;
                                    if (
                                        i.tags &&
                                        i.tags.findIndex(
                                            t => t.title === attr
                                        ) >= 0
                                    )
                                        i.attrs[attr] = true;
                                });
                                return {
                                    name: i.name,
                                    description: i.description,
                                    type:
                                        i.type && i.type.hasOwnProperty("names")
                                            ? i.type.names.join(" | ")
                                            : i.type || "string",
                                    default: i.defaultvalue || "",
                                    ...i.attrs
                                };
                            })
                            .sort((p1, p2) => {
                                if (p1.required === p2.required) return 0;
                                return p1.required ? -1 : 1;
                            });
                    resolve(rs);
                });
        }),
    parseDemo = async (name, entry, source) => {
        if (!source) return { name, source, parsed: "" };
        const sourceWillPrintedOut = source.replace("export default ", "");
        const parsed = await compile(source, entry);
        // console.log(`[${name}] parsed length: ${parsed.length}`);
        // i-- === 10 && console.log(parsed);
        return { name, source: sourceWillPrintedOut, parsed };
    };

exports.onCreateNode = async ({
    node,
    actions,
    getNode,
    loadNodeContent,
    createNodeId,
    createContentDigest
}) => {
    const { createNode, createParentChildLink, createNodeField } = actions;
    if (
        node.sourceInstanceName === "components" &&
        node.internal.mediaType === "application/javascript" &&
        !/\.stories\.js$/.test(node.relativePath)
    ) {
        const transform = component => {
            const componentNode = {
                ...component,
                id: createNodeId(`${node.id} >>> Component`),
                children: [],
                parent: node.id,
                internal: {
                    contentDigest: createContentDigest(component),
                    type: "ComponentApi"
                }
            };
            createNode(componentNode);
            createParentChildLink({ parent: node, child: componentNode });
        };

        const obj = await parse(node.name, await loadNodeContent(node));
        transform(obj);
    }
    if (node.internal.type === "Mdx") {
        const value = createFilePath({
            node,
            getNode: getNode,
            basePath: `pages`
        });
        createNodeField({
            name: `slug`,
            node,
            value
        });
    }
    if (
        node.sourceInstanceName === "demo" &&
        node.internal.type !== "Directory"
    ) {
        const transformDemo = demo => {
            const demoNode = {
                ...demo,
                id: createNodeId(`${node.id} >>> ComponentDemo`),
                children: [],
                parent: node.id,
                internal: {
                    contentDigest: createContentDigest(demo),
                    type: "ComponentDemo"
                }
            };
            createNode(demoNode);
            createParentChildLink({ parent: node, child: demoNode });
        };
        const obj = await parseDemo(
            node.name,
            node.absolutePath,
            await loadNodeContent(node)
        );
        transformDemo(obj);
    }
};

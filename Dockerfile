FROM node:11.11.0-stretch-slim as builder
WORKDIR /mytour-component
COPY . /mytour-component
RUN npm install
RUN npm run build

FROM nginx:1.14
ADD docker/default.conf /etc/nginx/conf.d/default.conf
WORKDIR /usr/share/nginx/html
COPY --from=builder /mytour-component/public/ .
EXPOSE 80
